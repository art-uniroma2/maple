MAPLE: Mapping Architecture based on Linguistic Evidences

An architecture and a software platform for the semi-automatic configuration of robust matching systems based on linguistic evidences and exploiting web published (linguistic) resources.