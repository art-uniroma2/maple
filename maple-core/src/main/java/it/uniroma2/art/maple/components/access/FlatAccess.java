package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.QueryResult;
import org.pf4j.ExtensionPoint;

/**
 * Flat access to the named concepts (identified by a URI) within a dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface FlatAccess extends ExtensionPoint {
	/**
	 * Lists the named concepts (identified by a URI) within a dataset.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<IRI> listNamedConcepts() throws RDF4JException;
}
