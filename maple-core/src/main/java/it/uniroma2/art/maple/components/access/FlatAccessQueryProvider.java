package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.maple.sparql.GraphPattern;
import org.pf4j.ExtensionPoint;

/**
 * Flat access to the named concepts (identified by a URI) within a dataset as a query.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public interface FlatAccessQueryProvider extends ExtensionPoint {
	/**
	 * Returns a graph pattern suitable for implementing {@link FlatAccess#listNamedConcepts()}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<IRI> listNamedConcepts() throws RDF4JException;
}
