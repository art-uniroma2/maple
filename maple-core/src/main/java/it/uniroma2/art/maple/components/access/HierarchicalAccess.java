package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.QueryResult;
import org.pf4j.ExtensionPoint;

/**
 * Hierarchical access to the named concepts (identified by a URI) within a dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface HierarchicalAccess extends ExtensionPoint, FlatAccess {

	/**
	 * Lists the uppermost named concepts in the hierarchy.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<IRI> listUppermostNamedConcepts() throws RDF4JException;

	/**
	 * Lists the named concepts directly below <code>concept</code> in the hierarchy.
	 * 
	 * @param concept
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<IRI> listChildNamedConcepts(IRI concept) throws RDF4JException;

	/**
	 * Checks whether <code>childConcept</code> is immediately below <code>concept</code> in the hierarchy.
	 * 
	 * @param concept
	 * @param childConcept
	 * @return
	 * @throws RDF4JException
	 */
	boolean hasChildNamedConcept(IRI concept, IRI childConcept) throws RDF4JException;

	/**
	 * Lists the named concepts above a <code>concept</code> in the hierarchy.
	 * 
	 * @param concept
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<IRI> listParentNamedConcepts(IRI concept) throws RDF4JException;

	/**
	 * Checks whether <code>concept</code> is immediately below <code>parentConcept</code> in the hierarchy.
	 * 
	 * @param concept
	 * @param parentConcept
	 * @return
	 * @throws RDF4JException
	 */
	boolean hasParentNamedConcept(IRI concept, IRI parentConcept) throws RDF4JException;
}
