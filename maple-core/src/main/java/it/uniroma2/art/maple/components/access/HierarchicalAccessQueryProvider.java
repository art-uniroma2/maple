package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.maple.sparql.GraphPattern;
import org.pf4j.ExtensionPoint;

/**
 * Hierarchical access to the named concepts (identified by a URI) within a dataset as a query.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface HierarchicalAccessQueryProvider extends ExtensionPoint, FlatAccessQueryProvider {

	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link HierarchicalAccess#listUppermostNamedConcepts()}.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<IRI> listUppermostNamedConcepts() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link HierarchicalAccess#listChildNamedConcepts(IRI)}.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<IRI> listChildNamedConcepts() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link HierarchicalAccess#hasChildNamedConcept(IRI, IRI)}.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Void> hasChildNamedConcept() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link HierarchicalAccess#listParentNamedConcepts(IRI)}.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<IRI> listParentNamedConcepts() throws RDF4JException;

	/**
	 * *Returns a graph pattern suitable for implementing
	 * {@link HierarchicalAccess#hasParentNamedConcept(IRI, IRI)}.
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Void> hasParentNamedConcept() throws RDF4JException;
}
