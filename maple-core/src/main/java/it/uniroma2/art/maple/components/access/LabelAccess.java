package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.QueryResult;
import org.pf4j.ExtensionPoint;

/**
 * Access to the labels of a resource.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public interface LabelAccess extends ExtensionPoint {
	/**
	 * Lists the labels associated with a resource in all natural languages.
	 * 
	 * @param resource
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listLabels(IRI resource) throws RDF4JException;

	/**
	 * Lists the labels associated with a resource in a given natural language.
	 * 
	 * @param resource
	 * @param lang
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listLabels(IRI resource, String lang) throws RDF4JException;

}
