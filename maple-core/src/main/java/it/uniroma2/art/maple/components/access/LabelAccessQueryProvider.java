package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.maple.sparql.GraphPattern;
import org.pf4j.ExtensionPoint;

/**
 * Access to the labels of a resource as a query.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public interface LabelAccessQueryProvider extends ExtensionPoint {
	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link LabelAccess#listLabels(org.eclipse.rdf4j.model.IRI)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listLabels() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link LabelAccess#listLabels(org.eclipse.rdf4j.model.IRI, String)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listLabelsForLanguage() throws RDF4JException;

}
