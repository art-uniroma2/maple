package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.QueryResult;
import org.pf4j.ExtensionPoint;

/**
 * Access to the terminology associated with a resource. The status of a label may be either:
 * <ul>
 * <li><em>preferred label:</em> the preferred way for mentioning a concept in a given natural language. It
 * should be unique.</li>
 * <li><em>alternative label:</em> alternative ways for mentioning a concept, beyond its preferred label</li>
 * <li><em>hidden label:</em> misspellings and similar strings that are stored for convenience, even though
 * they do not officially characterize a resource</li>
 * </ul>
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface TerminologyAccess extends ExtensionPoint, LabelAccess {

	/**
	 * Lists the preferred labels associated with a resource in all natural languages.
	 * 
	 * @param resource
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listPrefLabels(IRI resource) throws RDF4JException;

	/**
	 * Returns the preferred label associated with a resource in a given natural language.
	 * 
	 * @param resource
	 * @param lang
	 * @return
	 * @throws RDF4JException
	 */
	Literal getPrefLabel(IRI resource, String lang) throws RDF4JException;

	/**
	 * Lists the alternative labels associated with a resource in all natural languages.
	 * 
	 * @param resource
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listAltLabels(IRI resource) throws RDF4JException;

	/**
	 * Lists the alternative labels associated with a resource in a given natural language.
	 * 
	 * @param resource
	 * @param lang
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listAltLabels(IRI resource, String lang) throws RDF4JException;

	/**
	 * Lists the hidden labels associated with a resource in all natural languages.
	 * 
	 * @param resource
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listHiddenLabels(IRI resource) throws RDF4JException;

	/**
	 * Lists the hidden labels associated with a resource in a given natural language.
	 * 
	 * @param resource
	 * @param lang
	 * @return
	 * @throws RDF4JException
	 */
	QueryResult<Literal> listHiddenLabels(IRI resource, String lang) throws RDF4JException;

}
