package it.uniroma2.art.maple.components.access;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import it.uniroma2.art.maple.sparql.GraphPattern;
import org.pf4j.ExtensionPoint;

/**
 * Access to the terminology associated with a resource as a query. The status of a label may be either:
 * <ul>
 * <li><em>preferred label:</em> the preferred way for mentioning a concept in a given natural language. It
 * should be unique.</li>
 * <li><em>alternative label:</em> alternative ways for mentioning a concept, beyond its preferred label</li>
 * <li><em>hidden label:</em> misspellings and similar strings that are stored for convenience, even though
 * they do not officially characterize a resource</li>
 * </ul>
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface TerminologyAccessQueryProvider extends ExtensionPoint, LabelAccessQueryProvider {

	/**
	 * Returns a graph pattern suitable for implementing {@link TerminologyAccess#listPrefLabels(IRI)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listPrefLabels() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing {@link TerminologyAccess#getPrefLabel(IRI, String)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> getPrefLabelForLanguage() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing {@link TerminologyAccess#listAltLabels(IRI)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listAltLabels() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing {@link TerminologyAccess#listAltLabels(IRI, String)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listAltLabelsForLanguage() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing {@link TerminologyAccess#listHiddenLabels(IRI)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listHiddenLabels() throws RDF4JException;

	/**
	 * Returns a graph pattern suitable for implementing
	 * {@link TerminologyAccess#listHiddenLabels(IRI, String)}
	 * 
	 * @return
	 * @throws RDF4JException
	 */
	GraphPattern<Literal> listHiddenLabelsForLanguage() throws RDF4JException;

}
