package it.uniroma2.art.maple.components.access.facets;

/**
 * A facet indicating the awareness of the conceptual access over the input datasets.
 * 
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 * @param <T>
 */
public interface ConceptualAccessQueryProviderAware<T> {
	/**
	 * Sets the conceptual access query provider
	 * 
	 * @param conceptualAccess
	 */
	void setConceptualAccessQueryProvider(T conceptualAccess);

	/**
	 * Returns the conceptual access query provider
	 * 
	 * @return
	 */
	T getConceptualAccessQueryProvider();

}
