package it.uniroma2.art.maple.components.access.facets;

/**
 * A facet indicating the awareness of the linguistic access over the input datasets.
 * 
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 * @param <T>
 */
public interface LinguisticAccessQueryProviderAware<T> {
	/**
	 * Sets the linguistic access query provider
	 * 
	 * @param linguisticAccess
	 */
	void setLinguisticAccessQueryProvider(T linguisticAccess);

	/**
	 * Returns the linguistic access query provider
	 * 
	 * @return
	 */
	T getLinguisticAccessQueryProvider();

}
