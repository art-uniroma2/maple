package it.uniroma2.art.maple.components.access.facets;

import org.eclipse.rdf4j.repository.RepositoryConnection;

/**
 * A facet indicating the awareness of the connection to the target dataset.
 * 
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface RepositoryConnectionAware {
	/**
	 * Sets the repository connection
	 * 
	 * @param conn
	 */
	void setRepositoryConnection(RepositoryConnection conn);

	/**
	 * Returns the repository connection
	 * 
	 * @return
	 */
	RepositoryConnection getRepositoryConnection();

}
