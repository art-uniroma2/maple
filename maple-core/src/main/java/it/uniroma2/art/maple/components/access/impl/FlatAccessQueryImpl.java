package it.uniroma2.art.maple.components.access.impl;

import javax.annotation.PostConstruct;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.QueryResult;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.maple.components.access.FlatAccess;
import it.uniroma2.art.maple.components.access.FlatAccessQueryProvider;
import it.uniroma2.art.maple.components.access.facets.ConceptualAccessQueryProviderAware;
import it.uniroma2.art.maple.components.access.facets.RepositoryConnectionAware;
import it.uniroma2.art.maple.impl.misc.SingleBindingUnwrappingIterator;
import org.pf4j.Extension;

/**
 * An implementation of {@link FlatAccess} using a {@link FlatAccessQueryProvider}.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class FlatAccessQueryImpl implements FlatAccess,
		ConceptualAccessQueryProviderAware<FlatAccessQueryProvider>, RepositoryConnectionAware {

	private RepositoryConnection conn;
	private FlatAccessQueryProvider queryProvider;
	private TupleQuery listNamedConceptsQuery;

	@Override
	public void setRepositoryConnection(RepositoryConnection conn) throws RDF4JException {
		this.conn = conn;
	}

	@Override
	public RepositoryConnection getRepositoryConnection() {
		return conn;
	}

	@Override
	public void setConceptualAccessQueryProvider(FlatAccessQueryProvider queryProvider) {
		this.queryProvider = queryProvider;
	}

	@Override
	public FlatAccessQueryProvider getConceptualAccessQueryProvider() {
		return this.queryProvider;
	}

	/**
	 * Initializes this object
	 */
	@PostConstruct
	public void initialize() {
		if (queryProvider == null) {
			throw new IllegalStateException("Query provider not set");
		}

		listNamedConceptsQuery = queryProvider.listNamedConcepts().prepareTupleQuery(conn);
	}

	@Override
	public QueryResult<IRI> listNamedConcepts() throws RDF4JException {
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listNamedConceptsQuery.evaluate(), "concept", IRI.class));
	}

}
