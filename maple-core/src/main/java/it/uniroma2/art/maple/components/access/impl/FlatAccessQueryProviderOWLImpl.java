package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import it.uniroma2.art.maple.components.access.FlatAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link FlatAccessQueryProvider} suitable for OWL datasets. The resources of interest
 * are {@code owl:Class}es and {@code rdfs:Class}es.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class FlatAccessQueryProviderOWLImpl implements FlatAccessQueryProvider {

	private GraphPattern<IRI> listNamedConceptsQuery;

	/**
	 * Default constructor
	 */
	public FlatAccessQueryProviderOWLImpl() {
		listNamedConceptsQuery = new GraphPatternBuilder().outputVariable("concept").ns(RDFS.NS).ns(OWL.NS)
				.graphPattern(
				// @formatter:off
				" {                                                                                     \n" +
				"   ?concept a owl:Class                                                                \n" +
				" } UNION {                                                                             \n" +
				"   ?concept a rdfs:Class                                                               \n" +
				" }                                                                                     \n" +
				" FILTER(isIRI(?concept))                                                               \n" +
				" BIND(STR(?concept) as ?conceptIRI)                                                    \n" +
				" FILTER (!STRSTARTS(?conceptIRI, \"http://www.w3.org/2000/01/rdf-schema#\")            \n" +
				"    && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2002/07/owl#\")                     \n" +
				"    && !STRSTARTS(?conceptIRI, \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\")        \n" +
				"    && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2001/XMLSchema#\")                  \n" +
				" )                                                                                     \n"
				// @formatter:on
				).build();
	}

	@Override
	public GraphPattern<IRI> listNamedConcepts() throws RDF4JException {
		return listNamedConceptsQuery;
	}
}
