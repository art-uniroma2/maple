package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.SKOS;

import it.uniroma2.art.maple.components.access.FlatAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link FlatAccessQueryProvider} suitable for SKOS concept schemes. The resources of
 * interest are the individuals of the class skos:Concept.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class FlatAccessQueryProviderSKOSImpl implements FlatAccessQueryProvider {

	private GraphPattern<IRI> listNamedConceptsQuery;

	/**
	 * Default constructor
	 */
	public FlatAccessQueryProviderSKOSImpl() {
		listNamedConceptsQuery = new GraphPatternBuilder().outputVariable("concept").ns(SKOS.NS).graphPattern(
		// @formatter:off
				" ?concept a skos:Concept . \n" +
				" FILTER(isIRI(?concept))   \n"
				// @formatter:on
		).build();
	}

	@Override
	public GraphPattern<IRI> listNamedConcepts() throws RDF4JException {
		return listNamedConceptsQuery;
	}

}
