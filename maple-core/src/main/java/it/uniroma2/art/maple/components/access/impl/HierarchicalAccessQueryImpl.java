package it.uniroma2.art.maple.components.access.impl;

import javax.annotation.PostConstruct;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.QueryResult;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.maple.components.access.FlatAccess;
import it.uniroma2.art.maple.components.access.HierarchicalAccess;
import it.uniroma2.art.maple.components.access.HierarchicalAccessQueryProvider;
import it.uniroma2.art.maple.components.access.facets.ConceptualAccessQueryProviderAware;
import it.uniroma2.art.maple.components.access.facets.RepositoryConnectionAware;
import it.uniroma2.art.maple.impl.misc.SingleBindingUnwrappingIterator;
import org.pf4j.Extension;

/**
 * An implementation of {@link FlatAccess} using a {@link HierarchicalAccessQueryImpl}.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class HierarchicalAccessQueryImpl implements HierarchicalAccess,
		ConceptualAccessQueryProviderAware<HierarchicalAccessQueryProvider>, RepositoryConnectionAware {

	protected RepositoryConnection conn;
	protected HierarchicalAccessQueryProvider queryProvider;

	private TupleQuery listNamedConceptsQuery;
	private TupleQuery listUppermostNamedConceptsQuery;
	private TupleQuery listChildNamedConceptsQuery;
	private BooleanQuery hasChildNamedConceptQuery;
	private TupleQuery listParentNamedConceptsQuery;
	private BooleanQuery hasParentNamedConceptQuery;

	@Override
	public void setRepositoryConnection(RepositoryConnection conn) {
		this.conn = conn;
	}

	@Override
	public RepositoryConnection getRepositoryConnection() {
		return conn;
	}

	@Override
	public void setConceptualAccessQueryProvider(HierarchicalAccessQueryProvider queryProvider) {
		this.queryProvider = queryProvider;
	}

	@Override
	public HierarchicalAccessQueryProvider getConceptualAccessQueryProvider() {
		return queryProvider;
	}

	/**
	 * Initializes this object
	 */
	@PostConstruct
	public void initialize() {
		if (queryProvider == null) {
			throw new IllegalStateException("Query provider not set");
		}

		listNamedConceptsQuery = queryProvider.listNamedConcepts().prepareTupleQuery(conn);
		listUppermostNamedConceptsQuery = queryProvider.listUppermostNamedConcepts().prepareTupleQuery(conn);
		listChildNamedConceptsQuery = queryProvider.listChildNamedConcepts().prepareTupleQuery(conn);
		hasChildNamedConceptQuery = queryProvider.hasChildNamedConcept().prepareBooleanQuery(conn);
		listParentNamedConceptsQuery = queryProvider.listParentNamedConcepts().prepareTupleQuery(conn);
		hasParentNamedConceptQuery = queryProvider.hasParentNamedConcept().prepareBooleanQuery(conn);
	}

	@Override
	public QueryResult<IRI> listNamedConcepts() throws RDF4JException {
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listNamedConceptsQuery.evaluate(), "concept", IRI.class));
	}

	@Override
	public QueryResult<IRI> listUppermostNamedConcepts() throws RDF4JException {
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listUppermostNamedConceptsQuery.evaluate(), "concept", IRI.class));
	}

	@Override
	public QueryResult<IRI> listChildNamedConcepts(IRI resource) throws RDF4JException {
		listChildNamedConceptsQuery.setBinding("resource", resource);
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listChildNamedConceptsQuery.evaluate(), "concept", IRI.class));
	}

	@Override
	public boolean hasChildNamedConcept(IRI concept, IRI childConcept) throws RDF4JException {
		hasChildNamedConceptQuery.setBinding("concept", concept);
		hasChildNamedConceptQuery.setBinding("childConcept", childConcept);

		return hasChildNamedConceptQuery.evaluate();
	}

	@Override
	public QueryResult<IRI> listParentNamedConcepts(IRI resource) throws RDF4JException {
		listParentNamedConceptsQuery.setBinding("resource", resource);

		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listParentNamedConceptsQuery.evaluate(), "concept", IRI.class));
	}

	@Override
	public boolean hasParentNamedConcept(IRI concept, IRI parentConcept) throws RDF4JException {
		hasParentNamedConceptQuery.setBinding("concept", concept);
		hasParentNamedConceptQuery.setBinding("parentConcept", parentConcept);
		return hasParentNamedConceptQuery.evaluate();
	}

}
