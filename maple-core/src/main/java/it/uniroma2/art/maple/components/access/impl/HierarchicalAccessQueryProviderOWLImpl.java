/* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Portions created by ART Group, Tor Vergata University of Rome are Copyright (C) 2013 */

package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SESAME;

import it.uniroma2.art.maple.components.access.HierarchicalAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link HierarchicalAccessQueryProvider} suitable for OWL datasets. The resources of
 * interest are the same of {@link FlatAccessQueryProviderOWLImpl}.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class HierarchicalAccessQueryProviderOWLImpl extends FlatAccessQueryProviderOWLImpl
		implements HierarchicalAccessQueryProvider {

	private GraphPattern<IRI> listUppermostNamedConceptsQuery;
	private GraphPattern<IRI> listChildNamedConceptsQuery;
	private GraphPattern<Void> hasChildNamedConceptQuery;
	private GraphPattern<IRI> listParentNamedConceptsQuery;
	private GraphPattern<Void> hasParentNamedConceptQuery;

	/**
	 * Default constructor
	 */
	public HierarchicalAccessQueryProviderOWLImpl() {

		listUppermostNamedConceptsQuery = new GraphPatternBuilder().ns(OWL.NS).ns(RDFS.NS).ns(RDF.NS)
				.ns(SESAME.NS).outputVariable("concept").graphPattern(
				// @formatter:off
			"{                                                                                     \n" +
			"    ?concept sesame:directSubClassOf owl:Thing .                                      \n" +
			"} UNION {                                                                             \n" +
			"    ?concept sesame:directSubClassOf rdfs:Resource .                                  \n" +
			"}                                                                                     \n" +
			"{                                                                                     \n" +
			"    ?concept a owl:Class .                                                            \n" +
			"} UNION {                                                                             \n" +
			"    ?concept a rdfs:Class .                                                           \n" +
			"}                                                                                     \n" +
			"FILTER(isIRI(?concept))                                                               \n" +
			"BIND(STR(?concept) as ?conceptIRI)                                                    \n" +
			"FILTER (!STRSTARTS(?conceptIRI, \"http://www.w3.org/2000/01/rdf-schema#\")            \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2002/07/owl#\")                     \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\")        \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2001/XMLSchema#\")                  \n" +
			")                                                                                     \n"
			// @formatter:on
				).build();

		listChildNamedConceptsQuery = new GraphPatternBuilder().ns(OWL.NS).ns(RDFS.NS).ns(RDF.NS)
				.ns(SESAME.NS).outputVariable("concept").inputVariable("resource").graphPattern(
				// @formatter:off
			"?concept sesame:directSubClassOf ?resource .                                          \n" +
			"{                                                                                     \n" +
			"    ?concept a owl:Class .                                                            \n" +
			"} UNION {                                                                             \n" +
			"    ?concept a rdfs:Class .                                                           \n" +
			"}                                                                                     \n" +
			"FILTER(isIRI(?concept))                                                               \n" +
			"BIND(STR(?concept) as ?conceptIRI)                                                    \n" +
			"FILTER (!STRSTARTS(?conceptIRI, \"http://www.w3.org/2000/01/rdf-schema#\")            \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2002/07/owl#\")                     \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\")        \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2001/XMLSchema#\")                  \n" +
			")                                                                                     \n"
			// @formatter:on
				).build();

		hasChildNamedConceptQuery = new GraphPatternBuilder().ns(OWL.NS).ns(RDFS.NS).ns(RDF.NS).ns(SESAME.NS)
				.inputVariable("concept").inputVariable("childConcept").graphPattern(
				// @formatter:off
			"?childConcept sesame:directSubClassOf ?concept .                                  \n" +
			"{                                                                                 \n" +
			"    ?childConcept a owl:Class .                                                   \n" +
			"} UNION {                                                                         \n" +
			"    ?childConcept a rdfs:Class .                                                  \n" +
			"}                                                                                 \n" +
			"FILTER(isIRI(?childConcept))                                                      \n" +
			"BIND(STR(?childConcept) as ?conceptIRI)                                           \n" +
			"FILTER (!STRSTARTS(?conceptIRI, \"http://www.w3.org/2000/01/rdf-schema#\")        \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2002/07/owl#\")                 \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\")    \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2001/XMLSchema#\")              \n" +
			")                                                                                 \n"
			// @formatter:on
				).build();

		listParentNamedConceptsQuery = new GraphPatternBuilder().ns(OWL.NS).ns(RDFS.NS).ns(RDF.NS)
				.ns(SESAME.NS).outputVariable("concept").inputVariable("resource").graphPattern(
				// @formatter:off
			"?resource sesame:directSubClassOf ?concept .                                          \n" +
			"{                                                                                     \n" +
			"    ?concept a owl:Class .                                                            \n" +
			"} UNION {                                                                             \n" +
			"    ?concept a rdfs:Class .                                                           \n" +
			"}                                                                                     \n" +
			"FILTER(isIRI(?concept))                                                               \n" +
			"BIND(STR(?concept) as ?conceptIRI)                                                    \n" +
			"FILTER (!STRSTARTS(?conceptIRI, \"http://www.w3.org/2000/01/rdf-schema#\")            \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2002/07/owl#\")                     \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\")        \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2001/XMLSchema#\")                  \n" +
			")                                                                                     \n"
			// @formatter:on
				).build();

		hasParentNamedConceptQuery = new GraphPatternBuilder().ns(OWL.NS).ns(RDFS.NS).ns(RDF.NS).ns(SESAME.NS)
				.inputVariable("concept").inputVariable("parentConcept").graphPattern(
				// @formatter:off
			"?concept sesame:directSubClassOf ?parentConcept .                                     \n" +
			"{                                                                                     \n" +
			"    ?parentConcept a owl:Class .                                                      \n" +
			"} UNION {                                                                             \n" +
			"    ?parentConcept a rdfs:Class .                                                     \n" +
			"}                                                                                     \n" +
			"FILTER(isIRI(?parentConcept))                                                         \n" +
			"BIND(STR(?parentConcept) as ?conceptIRI)                                              \n" +
			"FILTER (!STRSTARTS(?conceptIRI, \"http://www.w3.org/2000/01/rdf-schema#\")            \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2002/07/owl#\")                     \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\")        \n" +
			"   && !STRSTARTS(?conceptIRI, \"http://www.w3.org/2001/XMLSchema#\")                  \n" +
			")                                                                                     \n"
			// @formatter:on	
				).build();
	}

	@Override
	public GraphPattern<IRI> listUppermostNamedConcepts() throws RDF4JException {
		return listUppermostNamedConceptsQuery;
	}

	@Override
	public GraphPattern<IRI> listChildNamedConcepts() throws RDF4JException {
		return listChildNamedConceptsQuery;
	}

	@Override
	public GraphPattern<Void> hasChildNamedConcept() throws RDF4JException {
		return hasChildNamedConceptQuery;
	}

	@Override
	public GraphPattern<IRI> listParentNamedConcepts() throws RDF4JException {
		return listParentNamedConceptsQuery;
	}

	@Override
	public GraphPattern<Void> hasParentNamedConcept() throws RDF4JException {
		return hasParentNamedConceptQuery;
	}
}
