package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.SKOS;

import it.uniroma2.art.maple.components.access.HierarchicalAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link HierarchicalAccessQueryProvider} for SKOS concept schemes. The resources of
 * interest are the same of {@link FlatAccessQueryProviderSKOSImpl}.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class HierarchicalAccessQueryProviderSKOSImpl extends FlatAccessQueryProviderSKOSImpl
		implements HierarchicalAccessQueryProvider {
	private GraphPattern<IRI> listUppermostNamedConceptsQuery;
	private GraphPattern<IRI> listChildNamedConceptsQuery;
	private GraphPattern<Void> hasChildNamedConceptQuery;
	private GraphPattern<IRI> listParentNamedConceptsQuery;
	private GraphPattern<Void> hasParentNamedConceptQuery;

	/**
	 * Default construtor
	 */
	public HierarchicalAccessQueryProviderSKOSImpl() {

		listUppermostNamedConceptsQuery = new GraphPatternBuilder().ns(SKOS.NS).outputVariable("concept")
				.graphPattern(
				// @formatter:off
					"?concept a skos:Concept .                                                        \n" +
					"FILTER(isIRI(?concept))                                                          \n" +
					"FILTER NOT EXISTS {                                                              \n" +
					"    ?concept skos:broader []                                                     \n" +
					"}                                                                                \n"
					// @formatter:on
				).build();

		listChildNamedConceptsQuery = new GraphPatternBuilder().ns(SKOS.NS).outputVariable("concept")
				.inputVariable("resource").graphPattern(
				// @formatter:off
					"?concept skos:broader|^skos:narrower ?resource                                   \n" +
					"FILTER(isIRI(?concept))                                                          \n"
					// @formatter:on
				).build();

		hasChildNamedConceptQuery = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("concept")
				.inputVariable("childConcept").graphPattern(
				// @formatter:off
					"?childConcept skos:broader|^skos:narrower ?concept                                \n"
					// @formatter:on
				).build();

		listParentNamedConceptsQuery = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.outputVariable("concept").graphPattern(
				// @formatter:off
					"?resource skos:broader|^skos:narrower ?concept                                    \n" +
					"FILTER(isIRI(?concept))                                                           \n"
					// @formatter:on
				).build();

		hasParentNamedConceptQuery = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("concept")
				.inputVariable("parentConcept").graphPattern(
				// @formatter:off
				"     ?concept skos:broader|^skos:narrower ?parentConcept                               \n"
				// @formatter:on	
				).build();
	}

	@Override
	public GraphPattern<IRI> listUppermostNamedConcepts() throws RDF4JException {
		return listUppermostNamedConceptsQuery;
	}

	@Override
	public GraphPattern<IRI> listChildNamedConcepts() throws RDF4JException {
		return listChildNamedConceptsQuery;
	}

	@Override
	public GraphPattern<Void> hasChildNamedConcept() throws RDF4JException {
		return hasChildNamedConceptQuery;
	}

	@Override
	public GraphPattern<IRI> listParentNamedConcepts() throws RDF4JException {
		return listParentNamedConceptsQuery;
	}

	@Override
	public GraphPattern<Void> hasParentNamedConcept() throws RDF4JException {
		return hasParentNamedConceptQuery;
	}
}
