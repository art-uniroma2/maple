package it.uniroma2.art.maple.components.access.impl;

import javax.annotation.PostConstruct;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.QueryResult;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.maple.components.access.LabelAccess;
import it.uniroma2.art.maple.components.access.LabelAccessQueryProvider;
import it.uniroma2.art.maple.components.access.facets.LinguisticAccessQueryProviderAware;
import it.uniroma2.art.maple.components.access.facets.RepositoryConnectionAware;
import it.uniroma2.art.maple.impl.misc.SingleBindingUnwrappingIterator;
import org.pf4j.Extension;

/**
 * An implementation of {@link LabelAccess} using a {@link LabelAccessQueryProvider}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class LabelAccessQueryImpl implements LabelAccess,
		LinguisticAccessQueryProviderAware<LabelAccessQueryProvider>, RepositoryConnectionAware {

	private RepositoryConnection conn;
	private LabelAccessQueryProvider queryProvider;

	private TupleQuery listLabels1Query;
	private TupleQuery listLabels2Query;

	@Override
	public void setRepositoryConnection(RepositoryConnection conn) {
		this.conn = conn;
	}

	@Override
	public RepositoryConnection getRepositoryConnection() {
		return conn;
	}

	@Override
	public void setLinguisticAccessQueryProvider(LabelAccessQueryProvider queryProvider) {
		this.queryProvider = queryProvider;
	}

	@Override
	public LabelAccessQueryProvider getLinguisticAccessQueryProvider() {
		return queryProvider;
	}

	/**
	 * Initializes this object
	 */
	@PostConstruct
	public void initialize() {
		if (queryProvider == null) {
			throw new IllegalStateException("Query provider not set");
		}

		listLabels1Query = queryProvider.listLabels().prepareTupleQuery(conn);
		listLabels2Query = queryProvider.listLabelsForLanguage().prepareTupleQuery(conn);
	}

	@Override
	public QueryResult<Literal> listLabels(IRI resource) throws RDF4JException {
		listLabels1Query.setBinding("resource", resource);
		return new SingleBindingQueryResult<>(
				new SingleBindingUnwrappingIterator<>(listLabels1Query.evaluate(), "label", Literal.class)); // return
	}

	@Override
	public QueryResult<Literal> listLabels(IRI resource, String lang) throws RDF4JException {
		listLabels2Query.setBinding("resource", resource);
		listLabels2Query.setBinding("lang", conn.getValueFactory().createLiteral(lang));
		return new SingleBindingQueryResult<>(
				new SingleBindingUnwrappingIterator<>(listLabels2Query.evaluate(), "label", Literal.class)); // return
	}

}
