package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;
import it.uniroma2.art.maple.components.access.LabelAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link LabelAccessQueryProvider} for OntoLex-Lemon lexicalizations.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class LabelAccessQueryProviderOntoLexLemonImpl implements LabelAccessQueryProvider {

	private GraphPattern<Literal> listLabels1Query;
	private GraphPattern<Literal> listLabels2Query;

	/**
	 * Default constructor
	 */
	public LabelAccessQueryProviderOntoLexLemonImpl() throws RDF4JException {
		listLabels1Query = new GraphPatternBuilder().outputVariable("label").inputVariable("resource")
				.ns(ONTOLEX.NS).graphPattern(
				// @formatter:off
				"\n{?resource <"+ONTOLEX.IS_DENOTED_BY+"> ?entry . } " +
				"\nUNION " +
				"\n{?resource ^<"+ONTOLEX.DENOTES+"> ?entry . } " +
				"\nUNION " +
				"\n{?resource <" + ONTOLEX.IS_EVOKED_BY + "> ?entry . } " +
				"\nUNION " +
				"\n{?resource ^<" + ONTOLEX.EVOKES + "> ?entry.} "+
				"\nUNION " +
				"\n{{?sense <"+ONTOLEX.REFERENCE+"> ?resource}" +
				"\nUNION " +
				"\n{?sense ^<"+ONTOLEX.IS_REFERENCE_OF+"> ?resource}" +
				"\nUNION " +
			    "\n{?sense <" + ONTOLEX.IS_LEXICALIZED_SENSE_OF + "> ?resource}" +
				"\nUNION " +
			    "\n{?sense ^<" + ONTOLEX.LEXICALIZED_SENSE +"> ?resource . }" +
				"\n{ ?sense <"+ONTOLEX.IS_SENSE_OF+"> ?entry } UNION {?sense ^<"+ONTOLEX.SENSE+"> ?entry . }} " +
				"\n?entry <"+ONTOLEX.CANONICAL_FORM+"> [ <"+ONTOLEX.WRITTEN_REP+"> ?label ] .\n"
				// @formatter:on
				).build();

		listLabels2Query = new GraphPatternBuilder().outputVariable("label").inputVariable("resource")
				.inputVariable("lang").ns(RDFS.NS).graphPattern(
				// @formatter:off
				"\n{?resource <"+ONTOLEX.IS_DENOTED_BY+"> ?entry . } " +
				"\nUNION " +
				"\n{?resource ^<"+ONTOLEX.DENOTES+"> ?entry . } " +
				"\nUNION " +
				"\n{?resource <" + ONTOLEX.IS_EVOKED_BY + "> ?entry . } " +
				"\nUNION " +
				"\n{?resource ^<" + ONTOLEX.EVOKES + "> ?entry.} "+
				"\nUNION " +
				"\n{{?sense <"+ONTOLEX.REFERENCE+"> ?resource}" +
				"\nUNION " +
				"\n{?sense ^<"+ONTOLEX.IS_REFERENCE_OF+"> ?resource}" +
				"\nUNION " +
			    "\n{?sense <" + ONTOLEX.IS_LEXICALIZED_SENSE_OF + "> ?resource}" +
				"\nUNION " +
			    "\n{?sense ^<" + ONTOLEX.LEXICALIZED_SENSE +"> ?resource . }" +
				"\n{ ?sense <"+ONTOLEX.IS_SENSE_OF+"> ?entry } UNION {?sense ^<"+ONTOLEX.SENSE+"> ?entry . }} " +
				"\n?entry <"+ONTOLEX.CANONICAL_FORM+"> [ <"+ONTOLEX.WRITTEN_REP+"> ?label ] .\n" +
				"\nFILTER(LANG(?label) = ?lang)"
				// @formatter:on
				).build();
	}

	@Override
	public GraphPattern<Literal> listLabels() throws RDF4JException {
		return listLabels1Query;
	}

	@Override
	public GraphPattern<Literal> listLabelsForLanguage() throws RDF4JException {
		return listLabels2Query;
	}
}
