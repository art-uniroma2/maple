package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import it.uniroma2.art.maple.components.access.LabelAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link LabelAccessQueryProvider} for rdfs:label.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class LabelAccessQueryProviderRDFSImpl implements LabelAccessQueryProvider {

	private GraphPattern<Literal> listLabels1Query;
	private GraphPattern<Literal> listLabels2Query;

	/**
	 * Default constructor
	 */
	public LabelAccessQueryProviderRDFSImpl() throws RDF4JException {
		listLabels1Query = new GraphPatternBuilder().outputVariable("label").inputVariable("resource")
				.ns(RDFS.NS).graphPattern(
				// @formatter:off
				"?resource rdfs:label ?label . \n"
				// @formatter:on
				).build();

		listLabels2Query = new GraphPatternBuilder().outputVariable("label").inputVariable("resource")
				.inputVariable("lang").ns(RDFS.NS).graphPattern(
				// @formatter:off
				"?resource rdfs:label ?label .                                                   \n" +
				"FILTER(LANG(?label) = ?lang)                                                    \n"
				// @formatter:on
				).build();
	}

	@Override
	public GraphPattern<Literal> listLabels() throws RDF4JException {
		return listLabels1Query;
	}

	@Override
	public GraphPattern<Literal> listLabelsForLanguage() throws RDF4JException {
		return listLabels2Query;
	}
}
