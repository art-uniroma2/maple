package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryResult;
import org.pf4j.Extension;

/**
 * A {@link QueryResult} returing the values produces by a {@link CloseableIteration}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 * @param <T>
 */
public class SingleBindingQueryResult<T> implements QueryResult<T> {

	private CloseableIteration<? extends T, ? extends QueryEvaluationException> iter;

	public SingleBindingQueryResult(
			CloseableIteration<? extends T, ? extends QueryEvaluationException> iter) {
		this.iter = iter;
	}

	@Override
	public void close() throws QueryEvaluationException {
		iter.close();
		;
	}

	@Override
	public boolean hasNext() throws QueryEvaluationException {
		return iter.hasNext();
	}

	@Override
	public T next() throws QueryEvaluationException {
		return iter.next();
	}

	@Override
	public void remove() throws QueryEvaluationException {
		iter.remove();
	}

}
