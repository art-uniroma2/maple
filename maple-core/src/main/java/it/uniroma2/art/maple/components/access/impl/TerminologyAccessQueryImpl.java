package it.uniroma2.art.maple.components.access.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.QueryResult;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.maple.components.access.TerminologyAccess;
import it.uniroma2.art.maple.components.access.TerminologyAccessQueryProvider;
import it.uniroma2.art.maple.components.access.facets.LinguisticAccessQueryProviderAware;
import it.uniroma2.art.maple.components.access.facets.RepositoryConnectionAware;
import it.uniroma2.art.maple.impl.misc.SingleBindingUnwrappingIterator;
import org.pf4j.Extension;

/**
 * An implementation of {@link TerminologyAccess} using a {@link TerminologyAccessQueryProvider}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
@Extension
public class TerminologyAccessQueryImpl implements TerminologyAccess,
		LinguisticAccessQueryProviderAware<TerminologyAccessQueryProvider>, RepositoryConnectionAware {

	private RepositoryConnection conn;
	private TerminologyAccessQueryProvider queryProvider;

	private TupleQuery listLabels1Query;
	private TupleQuery listLabels2Query;
	private TupleQuery getPrefLabelQuery;
	private TupleQuery listAltLabels1Query;
	private TupleQuery listAltLabels2Query;
	private TupleQuery listHiddenLabels1Query;
	private TupleQuery listHiddenLabels2Query;
	private TupleQuery listPrefLabelsQuery;

	@Override
	public void setRepositoryConnection(RepositoryConnection conn) {
		this.conn = conn;
	}

	@Override
	public RepositoryConnection getRepositoryConnection() {
		return conn;
	}

	@Override
	public void setLinguisticAccessQueryProvider(TerminologyAccessQueryProvider queryProvider) {
		this.queryProvider = queryProvider;
	}

	@Override
	public TerminologyAccessQueryProvider getLinguisticAccessQueryProvider() {
		return queryProvider;
	}

	/**
	 * Initializes this object
	 */
	@PostConstruct
	public void initialize() {
		if (queryProvider == null) {
			throw new IllegalStateException("Query provider not set");
		}

		listLabels1Query = queryProvider.listLabels().prepareTupleQuery(conn);
		listLabels2Query = queryProvider.listLabelsForLanguage().prepareTupleQuery(conn);
		listPrefLabelsQuery = queryProvider.listPrefLabels().prepareTupleQuery(conn);
		getPrefLabelQuery = queryProvider.getPrefLabelForLanguage().prepareTupleQuery(conn);
		listAltLabels1Query = queryProvider.listAltLabels().prepareTupleQuery(conn);
		listAltLabels2Query = queryProvider.listAltLabelsForLanguage().prepareTupleQuery(conn);
		listHiddenLabels1Query = queryProvider.listHiddenLabels().prepareTupleQuery(conn);
		listHiddenLabels2Query = queryProvider.listHiddenLabelsForLanguage().prepareTupleQuery(conn);
	}

	@Override
	public QueryResult<Literal> listPrefLabels(IRI resource) throws RDF4JException {
		listPrefLabelsQuery.setBinding("resource", resource);
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listPrefLabelsQuery.evaluate(), "label", Literal.class));
	}

	@Override
	public Literal getPrefLabel(IRI resource, String lang) throws RDF4JException {
		getPrefLabelQuery.setBinding("resource", resource);
		getPrefLabelQuery.setBinding("lang", conn.getValueFactory().createLiteral(lang));
		List<Literal> labels = Iterations.asList(new SingleBindingQueryResult<>(
				new SingleBindingUnwrappingIterator<>(getPrefLabelQuery.evaluate(), "label", Literal.class)));

		if (labels.isEmpty()) {
			return null;
		} else {
			return labels.iterator().next();
		}
	}

	@Override
	public QueryResult<Literal> listAltLabels(IRI resource) throws RDF4JException {
		listAltLabels1Query.setBinding("resource", resource);
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listAltLabels1Query.evaluate(), "label", Literal.class));
	}

	@Override
	public QueryResult<Literal> listAltLabels(IRI resource, String lang) throws RDF4JException {
		listAltLabels2Query.setBinding("resource", resource);
		listAltLabels2Query.setBinding("lang", conn.getValueFactory().createLiteral(lang));
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listAltLabels2Query.evaluate(), "label", Literal.class));
	}

	@Override
	public QueryResult<Literal> listHiddenLabels(IRI resource) throws RDF4JException {
		listHiddenLabels1Query.setBinding("resource", resource);
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listHiddenLabels1Query.evaluate(), "label", Literal.class));
	}

	@Override
	public QueryResult<Literal> listHiddenLabels(IRI resource, String lang) throws RDF4JException {
		listHiddenLabels2Query.setBinding("resource", resource);
		listHiddenLabels2Query.setBinding("lang", conn.getValueFactory().createLiteral(lang));
		return new SingleBindingQueryResult<>(new SingleBindingUnwrappingIterator<>(
				listHiddenLabels2Query.evaluate(), "label", Literal.class));
	}

	@Override
	public QueryResult<Literal> listLabels(IRI resource) throws RDF4JException {
		listLabels1Query.setBinding("resource", resource);
		return new SingleBindingQueryResult<>(
				new SingleBindingUnwrappingIterator<>(listLabels1Query.evaluate(), "label", Literal.class));
	}

	@Override
	public QueryResult<Literal> listLabels(IRI resource, String lang) throws RDF4JException {
		listLabels2Query.setBinding("resource", resource);
		listLabels2Query.setBinding("lang", conn.getValueFactory().createLiteral(lang));
		return new SingleBindingQueryResult<>(
				new SingleBindingUnwrappingIterator<>(listLabels2Query.evaluate(), "label", Literal.class));
	}
}
