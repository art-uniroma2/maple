package it.uniroma2.art.maple.components.access.impl;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.vocabulary.SKOS;

import it.uniroma2.art.maple.components.access.TerminologyAccessQueryProvider;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.sparql.GraphPatternBuilder;
import org.pf4j.Extension;

/**
 * An implementation of {@link TerminologyAccessQueryProvider} for skos:{pref,alt,hidden}Label.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Extension
public class TerminologyAccessQueryProviderSKOSImpl implements TerminologyAccessQueryProvider {

	private GraphPattern<Literal> listPrefLabelsQuery;
	private GraphPattern<Literal> getPrefLabelQuery;
	private GraphPattern<Literal> listAltLabels1Query;
	private GraphPattern<Literal> listAltLabels2Query;
	private GraphPattern<Literal> listHiddenLabels1Query;
	private GraphPattern<Literal> listHiddenLabels2Query;
	private GraphPattern<Literal> listLabels1Query;
	private GraphPattern<Literal> listLabels2Query;

	/**
	 * Default constructor
	 * 
	 * @throws RDF4JException
	 */
	public TerminologyAccessQueryProviderSKOSImpl() throws RDF4JException {
		listLabels1Query = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.outputVariable("label").graphPattern(
				// @formatter:off
				"{                                                                               \n" +
				"  ?resource skos:prefLabel ?label .                                             \n" +
				"} UNION {                                                                       \n" +
				"  ?resource skos:altLabel ?label .                                              \n" +
				"} UNION {                                                                       \n" +
				"  ?resource skos:hiddenLabel ?label .                                           \n" +
				"}                                                                               \n"
				// @formatter:on
				).build();

		listLabels2Query = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.inputVariable("lang").outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:prefLabel|skos:altLabel|skos:hiddenLabel ?label .                \n" +
				"FILTER(LANG(?label) = ?lang)                                                    \n"
				// @formatter:on
				).build();

		listPrefLabelsQuery = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:prefLabel ?label .                                               \n"
				// @formatter:on
				).build();

		getPrefLabelQuery = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.inputVariable("lang").outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:prefLabel ?label .                                               \n" +
				"FILTER(LANG(?label) = ?lang)                                                    \n"
				// @formatter:on
				).build();

		listAltLabels1Query = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:altLabel ?label .                                                \n"
				// @formatter:on
				).build();

		listAltLabels2Query = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.inputVariable("lang").outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:altLabel ?label .                                                \n" +
				"FILTER(LANG(?label) = ?lang)                                                    \n"
				// @formatter:on
				).build();

		listHiddenLabels1Query = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:hiddenLabel ?label .                                             \n"
				// @formatter:on
				).build();

		listHiddenLabels2Query = new GraphPatternBuilder().ns(SKOS.NS).inputVariable("resource")
				.inputVariable("lang").outputVariable("label").graphPattern(
				// @formatter:off
				"?resource skos:hiddenLabel ?label .                                             \n" +
				"FILTER(LANG(?label) = ?lang)                                                    \n"
				// @formatter:on
				).build();
	}

	@Override
	public GraphPattern<Literal> listLabels() throws RDF4JException {
		return listLabels1Query;
	}

	@Override
	public GraphPattern<Literal> listLabelsForLanguage() throws RDF4JException {
		return listLabels2Query;
	}

	@Override
	public GraphPattern<Literal> listPrefLabels() throws RDF4JException {
		return listPrefLabelsQuery;
	}

	@Override
	public GraphPattern<Literal> getPrefLabelForLanguage() throws RDF4JException {
		return getPrefLabelQuery;
	}

	@Override
	public GraphPattern<Literal> listAltLabels() throws RDF4JException {
		return listAltLabels1Query;
	}

	@Override
	public GraphPattern<Literal> listAltLabelsForLanguage() throws RDF4JException {
		return listAltLabels2Query;
	}

	@Override
	public GraphPattern<Literal> listHiddenLabels() throws RDF4JException {
		return listHiddenLabels1Query;
	}

	@Override
	public GraphPattern<Literal> listHiddenLabelsForLanguage() throws RDF4JException {
		return listHiddenLabels2Query;
	}
}
