package it.uniroma2.art.maple.components.alignment;

import java.io.File;

/**
 * An alignment between two input datasets. Implementations might handle different alignment representation
 * formats.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Alignment {
	/**
	 * Writes this alignment to a file.
	 * 
	 * @param outputfile
	 */
	void write(File outputfile);
}
