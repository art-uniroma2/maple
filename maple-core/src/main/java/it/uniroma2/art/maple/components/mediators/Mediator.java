package it.uniroma2.art.maple.components.mediators;

import java.net.URI;

import it.uniroma2.art.maple.components.alignment.Alignment;
import org.pf4j.ExtensionPoint;

/**
 * The interface of a mediator. A mediator willing to receive further inputs needs to implement the associated
 * facet in the package <code>it.uniroma2.art.maple.mediators.facets</code>.
 *
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Mediator extends ExtensionPoint {
	/**
	 * Aligns the given datasets.
	 * 
	 * @param uri1
	 * @param uri2
	 * @return
	 */
	Alignment align(URI uri1, URI uri2);
}
