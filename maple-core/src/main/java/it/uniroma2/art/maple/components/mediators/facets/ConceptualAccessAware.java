package it.uniroma2.art.maple.components.mediators.facets;

/**
 * A facet indicating the awareness of the conceptual access over the input datasets.
 * 
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 * @param <T>
 */
public interface ConceptualAccessAware<T> {
	/**
	 * Sets the conceptual access over the first dataset.
	 * 
	 * @param conceptualAccess1
	 */
	void setConceptualAccess1(T conceptualAccess1);

	/**
	 * Returns the conceptual access over the first dataset.
	 * 
	 * @return
	 */
	T getConceptualAccess1();

	/**
	 * Sets the conceptual access over the second dataset.
	 * 
	 * @param conceptualAccess2
	 */
	void setConceptualAccess2(T conceptualAccess2);

	/**
	 * Returns the conceptual access over the second dataset.
	 * 
	 * @return
	 */
	T getConceptualAccess2();
}
