package it.uniroma2.art.maple.components.mediators.facets;

/**
 * A facet indicating the awareness of the linguistic access over the input datasets.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 * @param <T>
 */
public interface LinguisticAccessAware<T> {
	/**
	 * Sets the linguistic access over the first dataset.
	 * 
	 * @param linguisticAccess1
	 */
	void setLinguisticAccess1(T linguisticAccess1);

	/**
	 * Returns the linguistic access over first dataset.
	 * 
	 * @return
	 */
	T getLinguisticAccess1();

	/**
	 * Sets the linguistic access over the second dataset.
	 * 
	 * @param linguisticAccess2
	 */
	void setLinguisticAccess2(T linguisticAccess2);

	/**
	 * Returns the linguistic access over the second dataset.
	 * 
	 * @return
	 */
	T getLinguisticAccess2();
}
