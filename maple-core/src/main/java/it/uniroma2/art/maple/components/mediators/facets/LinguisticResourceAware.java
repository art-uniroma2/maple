package it.uniroma2.art.maple.components.mediators.facets;

import java.util.Map;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.RepositoryConnection;

/**
 * A facet indicating the awareness of the linguistic resources.
 * 
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface LinguisticResourceAware {
	String[] getRequiredResources();

	void setLinguisticResource(String role, Map<String, Value> variables, RepositoryConnection conn);
}
