package it.uniroma2.art.maple.components.mediators.facets;

import it.uniroma2.art.maple.scenario.AlignmentScenario;

/**
 * A facet indicating the awareness of the mediation problem.
 * 
 * @param <T>
 */
public interface MediationProblemAware {
	/**
	 * Sets the mediation problem.
	 * @param mediationProblem
	 */
	void setMediationProblem(AlignmentScenario mediationProblem);
	
	/**
	 * Returns the mediation problem.
	 * @return
	 */
	AlignmentScenario getMediationProblem();
}
