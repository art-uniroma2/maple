package it.uniroma2.art.maple.impl.misc;

import org.eclipse.rdf4j.common.iteration.ConvertingIteration;
import org.eclipse.rdf4j.common.iteration.Iteration;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;

/**
 * Converts an iteration over {@link BindingSet} with a single binding to an iteration over the values bound
 * to that single binding.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 * @param <T>
 */
public class SingleBindingUnwrappingIterator<T extends Value>
		extends ConvertingIteration<BindingSet, T, QueryEvaluationException> {

	private Class<T> clazz;

	public SingleBindingUnwrappingIterator(
			Iteration<? extends BindingSet, ? extends QueryEvaluationException> iter, String bindingName,
			Class<T> clazz) {
		super(iter);
		this.bindingName = bindingName;
		this.clazz = clazz;
	}

	private String bindingName;

	@Override
	protected T convert(BindingSet sourceObject) throws QueryEvaluationException {
		return clazz.cast(sourceObject.getValue(bindingName));
	}

}
