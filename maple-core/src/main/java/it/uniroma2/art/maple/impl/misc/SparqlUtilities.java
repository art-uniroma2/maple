package it.uniroma2.art.maple.impl.misc;

import java.net.URL;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;

/**
 * Private utility class.
 * 
 * @author Manuel <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class SparqlUtilities {
	/**
	 * Simulates the clause <code>VALUES</code> as a <code>UNION</code> of <code>BIND</code>.
	 * 
	 * @param names
	 * @param values
	 * @return
	 */
	public static String tableToBindings(String[] names, Object[] values) {
		if (values == null)
			return "";

		if (values.length % names.length != 0)
			throw new IllegalArgumentException();

		StringBuilder sb = new StringBuilder();

		int rowCount = values.length / names.length;

		for (int i = 0; i < rowCount; i++) {
			sb.append("(");

			for (int j = 0; j < names.length; j++) {
				if (j != 0) {
					sb.append(" ");
				}
				sb.append(objectToNT(values[i * names.length + j]));
			}

			sb.append(")");
		}

		return sb.toString();
	}

	/**
	 * Produces the NT representation of an object.
	 * 
	 * @param object
	 * @return
	 */
	private static String objectToNT(Object object) {
		if (object instanceof Value) {
			return NTriplesUtil.toNTriplesString((Value) object);
		} else if (object instanceof URL) {
			return NTriplesUtil.toNTriplesString(
					SimpleValueFactory.getInstance().createIRI(((URL) object).toExternalForm()));
		} else {
			return object.toString();
		}
	}
}
