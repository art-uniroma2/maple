package it.uniroma2.art.maple.impl.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

/**
 * Private utility class.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class Utilities {

	/**
	 * Copies an input stream into a string, then closes the it.
	 * 
	 * @param in
	 *            the input stream to copy
	 * @param charset
	 *            the encoding used in the input stream
	 * @return
	 * @throws IOException
	 */
	public static String copyToStringThenClose(InputStream in, Charset charset) throws IOException {
		InputStreamReader r = new InputStreamReader(in, charset);
		StringWriter w = new StringWriter();

		CharStreams.copy(r, w);

		r.close();
		w.close();

		return w.toString();
	}

	private static final Pattern aggregationSplittingPattern = Pattern
			.compile("(?<!\\\\)(\\\\\\\\)*(?<splitter>,)");

	/**
	 * Split a comma-separated list of values
	 * 
	 * @param input
	 * @return
	 */
	public static List<String> splitCsvString(String input) {
		int index = 0;
		Matcher m = aggregationSplittingPattern.matcher(input);
		List<String> result = new ArrayList<>();

		if (input.isEmpty()) return result;
		
		while (m.find()) {
			int splitter = m.start("splitter");
			String componentString = input.substring(index, splitter).replace("\\,", ",").replace("\\\\",
					"\\");
			result.add(componentString);
			index = splitter + 1;
		}

		String componentString = input.substring(index).replace("\\,", ",").replace("\\\\", "\\");
		result.add(componentString);

		return result;
	}

	/**
	 * Copies a textual resource (encoded in UTF-8) from the classpath into a string. This method will use the
	 * classloader associated with the given class.
	 * 
	 * @param clazz
	 * @param resource
	 * @return
	 * @throws IOException
	 */
	public static String readTextualResoruceToString(Class<?> clazz, String resource) throws IOException {
		InputStream in = clazz.getResourceAsStream(resource);
		return copyToStringThenClose(in, Charsets.UTF_8);
	}

	/**
	 * Copies a textual resource (encoded in UTF-8) from the classpath into a string. This method will use the
	 * classloader associated with the class of the given object.
	 * 
	 * @param object
	 * @param resource
	 * @return
	 * @throws IOException
	 */
	public static String readResoruceToString(Object object, String resource) throws IOException {
		return readTextualResoruceToString(object.getClass(), resource);
	}

	/**
	 * Reduces the given set of model classes to the most expressive ones.
	 * 
	 * @param modelClasses
	 * @return
	 */
	// public static Set<Class<? extends RDFModel>> reduceModelClasses(
	// Set<Class<? extends RDFModel>> modelClasses) {
	// Iterator<Class<? extends RDFModel>> it = modelClasses.iterator();
	//
	// while (it.hasNext()) {
	// final Class<? extends RDFModel> curr = it.next();
	//
	// if (Iterators.any(modelClasses.iterator(), new Predicate<Class<? extends RDFModel>>() {
	//
	// @Override
	// public boolean apply(Class<? extends RDFModel> arg0) {
	// return !curr.equals(arg0) && canConvertModelTo(arg0, curr);
	// }
	//
	// })) {
	// it.remove();
	// }
	// }
	//
	// return modelClasses;
	// }
	//
	// /**
	// * Checks whether a given model might be converter into a another.
	// *
	// * @param fromModel
	// * @param toModel
	// * @return
	// */
	// public static boolean canConvertModelTo(Class<? extends RDFModel> fromModel,
	// Class<? extends RDFModel> toModel) {
	// if (fromModel.equals(toModel)) {
	// return true;
	// }
	//
	// if (toModel.isAssignableFrom(fromModel)) {
	// return true;
	// }
	//
	// if (SKOSModel.class.isAssignableFrom(fromModel) && toModel.isAssignableFrom(OWLModel.class)) {
	// return true;
	// }
	//
	// return false;
	// }

	/**
	 * Converts a model into another kind of model.
	 * 
	 * @param model
	 * @param toModelClass
	 * @return
	 */
	// public static <T extends RDFModel, Q extends RDFModel> Q convertModelTo(T model, Class<Q> toModelClass)
	// {
	// if (toModelClass.isAssignableFrom(OWLModel.class)
	// && (SKOSModel.class.isAssignableFrom(model.getClass()))) {
	// return toModelClass.cast(((SKOSModel) model).getOWLModel());
	// }
	//
	// return toModelClass.cast(model);
	// }
}