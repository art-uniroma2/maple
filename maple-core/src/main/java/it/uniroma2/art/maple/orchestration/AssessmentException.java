package it.uniroma2.art.maple.orchestration;

/**
 * Represents an exception occurred while performing an assessment.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli>
 *
 */
public class AssessmentException extends Exception {

	private static final long serialVersionUID = 388836185438908530L;

	public AssessmentException() {
		super();
	}

	public AssessmentException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AssessmentException(String message, Throwable cause) {
		super(message, cause);
	}

	public AssessmentException(String message) {
		super(message);
	}

	public AssessmentException(Throwable cause) {
		super(cause);
	}

	
}
