package it.uniroma2.art.maple.orchestration;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.maple.components.access.FlatAccess;
import it.uniroma2.art.maple.components.access.FlatAccessQueryProvider;
import it.uniroma2.art.maple.components.access.HierarchicalAccess;
import it.uniroma2.art.maple.components.access.HierarchicalAccessQueryProvider;
import it.uniroma2.art.maple.components.access.LabelAccess;
import it.uniroma2.art.maple.components.access.LabelAccessQueryProvider;
import it.uniroma2.art.maple.components.access.TerminologyAccess;
import it.uniroma2.art.maple.components.access.TerminologyAccessQueryProvider;
import it.uniroma2.art.maple.orchestration.model.Collaboration;
import it.uniroma2.art.maple.orchestration.model.CollaborationInstance;
import it.uniroma2.art.maple.scenario.AlignmentScenario;
import it.uniroma2.art.maple.scenario.ResourceLexicalizationSet;
import it.uniroma2.art.maple.scenario.SingleResourceMatchingProblem;

/**
 * Façade for accessing the main functionalities of MAPLE.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public interface MediationFramework {

	/**
	 * Returns {@link Collaboration}s including a {@link FlatAccess} over the provided {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeFlatAccesses(IRI model);

	/**
	 * Returns {@link Collaboration}s including a {@link HierarchicalAccess} over the provided {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeHierarchicalAccesses(IRI model);

	/**
	 * Returns {@link Collaboration}s including a {@link LabelAccess} over the provided {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeLabelAccesses(IRI model);

	/**
	 * Returns {@link Collaboration}s including a {@link TerminologyAccess} over the provided {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeTerminologyAccesses(IRI model);

	/**
	 * Instantiates a {@link Collaboration}. The returned {@link CollaborationInstance} contains the instances
	 * of the participants to the provided collaboration.
	 * 
	 * @param collaboration
	 * @param connections
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	CollaborationInstance instantiateCollaboration(Collaboration collaboration,
			Map<String, RepositoryConnection> connections)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException;

	/**
	 * Returns {@link Collaboration}s including a {@link FlatAccessQueryProvider} over the provided
	 * {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeFlatAccessQueryProviders(IRI model);

	/**
	 * Returns {@link Collaboration}s including a {@link HierarchicalAccessQueryProvider} over the provided
	 * {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeHierarchicalAccessQueryProviders(IRI model);

	/**
	 * Returns {@link Collaboration}s including a {@link LabelAccessQueryProvider} over the provided
	 * {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeLabelAccessQueryProviders(IRI createIRI);

	/**
	 * Returns {@link Collaboration}s including a {@link TerminologyAccessQueryProvider} over the provided
	 * {@code model}
	 * 
	 * @param model
	 * @return
	 */
	List<Collaboration> computeTerminologyAccessQueryProviders(IRI createIRI);

	/**
	 * Discovers lexicalization sets for the provided dataset and record them in the provided metadata store.
	 * 
	 * @param metadataConn
	 * @param dataset
	 * @throws AssessmentException
	 */
	void discoverLexicalizationSets(RepositoryConnection metadataConn, IRI dataset)
			throws AssessmentException;

	/**
	 * Returns the lexicalizations available for the provided resource
	 * 
	 * @param dataConn
	 * @param resource
	 * @return
	 * @throws AssessmentException
	 */
	List<ResourceLexicalizationSet> discoverLexicalizationSetsForResource(RepositoryConnection dataConn,
			IRI resource) throws AssessmentException;

	/**
	 * Establish the lexicalization model of the provided dataset
	 * 
	 * @param dataset
	 * @param profile
	 * @return
	 * @throws AssessmentException
	 */
	Optional<IRI> assessLexicalizationModel(IRI dataset, Model profile) throws AssessmentException;

	AlignmentScenario profileAlignmentScenario(RepositoryConnection metadataConn, IRI sourceDataset, IRI targetDataset,
			ProfilerOptions options) throws ProfilingException;

	AlignmentScenario profileAlignmentScenario(RepositoryConnection metadataConn, IRI sourceDataset, IRI targetDataset)
			throws ProfilingException;

	/**
	 * Profiles the problem of matching a resource against the given dataset
	 * 
	 * @param resource
	 * @param resourceLexicalizationSets
	 * @param targetDataset
	 * @param targetDatasetProfile
	 * @return
	 * @throws ProfilingException
	 */
	SingleResourceMatchingProblem profileSingleResourceMatchingProblem(IRI resource,
			List<ResourceLexicalizationSet> resourceLexicalizationSets, IRI targetDataset,
			Model targetDatasetProfile) throws ProfilingException;

	/**
	 * Loads an OWL ART model. Refer to {@link ModelFactory}, for details about the parameters.
	 * 
	 * @param modelClass
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws ModelUpdateException
	 * @throws UnsupportedRDFFormatException
	 * @throws ClassNotFoundException
	 */
	// Repository loadDataset(Class<? extends RDFModel> modelClass, String baseuri, String
	// persistenceDirectory)
	// throws RDF4JException, ClassNotFoundException;

	/**
	 * Loads a LIME Model. Refer to {@link LIMEModelFactory}, for details about the parameters.
	 * 
	 * @param baseuri
	 * @param repositoryDirectory
	 * @return
	 * @throws ModelCreationException
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws ModelUpdateException
	 * @throws UnsupportedRDFFormatException
	 */
	// Repository loadLIMEModel(String baseuri, String repositoryDirectory) throws RDF4JException,
	// IOException;

	/**
	 * Populates a LIME Model with the profile of a given dataset. The dataset might be loaded through
	 * {@link #loadDataset(Class, String, String)}, or via {@link #createOntologyManager(Class)}. The empty
	 * LIME model should be created through {@link #loadLIMEModel(String, String)}.
	 * 
	 * @param model
	 * @param limeModel
	 * @throws ModelCreationException
	 * @throws ProfilerException
	 * @throws ModelUpdateException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException
	 */
	// void profileDataset(Repository model, Repository limeModel)
	// throws ProfilerException, FileNotFoundException, IOException, RDF4JException;

	/**
	 * Profiles the given mediation problem, as described by the profiles of the input datasets.
	 * 
	 * @param profile1
	 * @param profile2
	 * @return
	 * @throws Exception
	 */
	// MediationProblem profileProblem(Repository profile1, Repository profile2) throws Exception;

	/**
	 * Computes resolution strategies for a given mediation problem. Strategies should be ranked in order of
	 * decreasing score.
	 * 
	 * @param mediationProblem
	 * @return
	 * @throws IOException
	 * @throws ModelAccessException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 * @throws ClassNotFoundException
	 */
	// List<Strategy> computeStrategies(MediationProblem mediationProblem)
	// throws IOException, RDF4JException, ClassNotFoundException;

	/**
	 * Deploys the a strategy. The framework could ask to deploy into the local repository further OSGi
	 * bundles from configured repositories.
	 * 
	 * @param strategy
	 */
	// void deployStrategy(Strategy strategy);

	/**
	 * Instantiates a strategy. The datasets must be loaded using the right modelClass, as specified by
	 * {@link Strategy#getRequiredModelClass()}.
	 * 
	 * @param strategy
	 * @param dataset1
	 * @param dataset2
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws ModelCreationException
	 */
	// StrategyInstance instantiateStrategy(Strategy strategy, Repository dataset1, Repository dataset2)
	// throws ClassNotFoundException, InstantiationException, IllegalAccessException,
	// NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException,
	// RDF4JException;

}
