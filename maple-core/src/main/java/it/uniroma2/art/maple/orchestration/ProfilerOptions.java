package it.uniroma2.art.maple.orchestration;

/**
 * Options for
 * {@link MediationFramework#profileAlignmentScenario(org.eclipse.rdf4j.repository.RepositoryConnection, org.eclipse.rdf4j.model.IRI, org.eclipse.rdf4j.model.IRI, ProfilerOptions)}
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class ProfilerOptions {
	public static final int SYNONYMIZER_CANDIDATE_CAP_DEFAULT = 10;

	private int synonimizerCandidateCap;

	public ProfilerOptions() {
		synonimizerCandidateCap = SYNONYMIZER_CANDIDATE_CAP_DEFAULT;
	}

	/**
	 * Returns the maximum number of candidate synonymizers. A negative value means nop cap
	 * 
	 * @return
	 */
	public int getSynonymizerCandidateCap() {
		return synonimizerCandidateCap;
	}

	/**
	 * Sets the maximum number of candidate synonymizers. A negative value means nop cap
	 * 
	 * @return
	 */
	public void setSynonimizerCandidateCap(int synonimizerCandidateCap) {
		this.synonimizerCandidateCap = synonimizerCandidateCap;
	}
}
