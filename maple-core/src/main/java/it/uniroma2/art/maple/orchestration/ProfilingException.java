package it.uniroma2.art.maple.orchestration;

/**
 * Represents an exception occurred while profiling.
 * 
 * @author Manuel
 *
 */
public class ProfilingException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2396350848064718849L;

	/**
	 * 
	 */
	public ProfilingException() {
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ProfilingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ProfilingException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ProfilingException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ProfilingException(Throwable cause) {
		super(cause);
	}

}
