package it.uniroma2.art.maple.orchestration.impl;

import java.math.BigInteger;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.uniroma2.art.lime.profiler.ConceptSetStats;
import it.uniroma2.art.maple.scenario.DataService;
import it.uniroma2.art.maple.scenario.Dataset;

public class ConceptSet extends Dataset {

	private final ConceptSetStats stats;

	@JsonCreator
	public ConceptSet(@JsonProperty("@id") IRI id, @JsonProperty("@type") IRI type,
			@JsonProperty("uriSpace") String uriSpace, @JsonProperty("title") Set<Literal> title,
			@JsonProperty("sparqlEndpoint") DataService sparqlEndpoint,
			@JsonProperty("concepts") BigInteger concepts) {
		this(id, type, uriSpace, title, sparqlEndpoint, ConceptSetStats.of(concepts));
	}

	public ConceptSet(IRI id, IRI type, String uriSpace, Set<Literal> title, DataService sparqlEndpoint,
			ConceptSetStats stats) {
		super(id, type, uriSpace, title, sparqlEndpoint, null);
		this.stats = stats;
	}

	public long getConcepts() {
		return stats.getConcepts().longValueExact();
	}

	@Override
	protected ToStringHelper toStringHelper() {
		return super.toStringHelper().add("concepts", getConcepts());
	}
}
