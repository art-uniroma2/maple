package it.uniroma2.art.maple.orchestration.impl;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;
import it.uniroma2.art.lime.profiler.ConceptSetStats;
import it.uniroma2.art.lime.profiler.ConceptualizationSetStatistics;
import it.uniroma2.art.lime.profiler.LexicalizationSetStatistics;
import it.uniroma2.art.lime.profiler.LexiconStats;
import it.uniroma2.art.maple.components.access.LabelAccessQueryProvider;
import it.uniroma2.art.maple.components.access.facets.ConceptualAccessQueryProviderAware;
import it.uniroma2.art.maple.components.access.facets.LinguisticAccessQueryProviderAware;
import it.uniroma2.art.maple.components.access.facets.RepositoryConnectionAware;
import it.uniroma2.art.maple.impl.misc.Utilities;
import it.uniroma2.art.maple.orchestration.AssessmentException;
import it.uniroma2.art.maple.orchestration.MediationFramework;
import it.uniroma2.art.maple.orchestration.ProfilerOptions;
import it.uniroma2.art.maple.orchestration.ProfilingException;
import it.uniroma2.art.maple.orchestration.model.PluginCoordinates;
import it.uniroma2.art.maple.orchestration.model.Collaboration;
import it.uniroma2.art.maple.orchestration.model.CollaborationInstance;
import it.uniroma2.art.maple.orchestration.model.Component;
import it.uniroma2.art.maple.orchestration.model.Manifestation;
import it.uniroma2.art.maple.orchestration.model.Participant;
import it.uniroma2.art.maple.orchestration.model.ParticipantComponent;
import it.uniroma2.art.maple.orchestration.model.QueryProviderComponent;
import it.uniroma2.art.maple.orchestration.model.impl.PluginCoordinatesImpl;
import it.uniroma2.art.maple.orchestration.model.impl.CollaborationImpl;
import it.uniroma2.art.maple.orchestration.model.impl.CollaborationInstanceImpl;
import it.uniroma2.art.maple.orchestration.model.impl.ConceptualAccessComponentImpl;
import it.uniroma2.art.maple.orchestration.model.impl.ConceptualAccessQueryProviderComponentImpl;
import it.uniroma2.art.maple.orchestration.model.impl.LinguisticAccessComponentImpl;
import it.uniroma2.art.maple.orchestration.model.impl.LinguisticAccessQueryProviderComponentImpl;
import it.uniroma2.art.maple.orchestration.model.impl.ManifestationImpl;
import it.uniroma2.art.maple.orchestration.model.impl.MediatorComponentImpl;
import it.uniroma2.art.maple.orchestration.model.impl.ParticipantingComponentImpl;
import it.uniroma2.art.maple.provisioning.ComponentRepository;
import it.uniroma2.art.maple.scenario.Alignment;
import it.uniroma2.art.maple.scenario.AlignmentChain;
import it.uniroma2.art.maple.scenario.AlignmentScenario;
import it.uniroma2.art.maple.scenario.ConceptualizationSet;
import it.uniroma2.art.maple.scenario.DataService;
import it.uniroma2.art.maple.scenario.Dataset;
import it.uniroma2.art.maple.scenario.LexicalizationSet;
import it.uniroma2.art.maple.scenario.Lexicon;
import it.uniroma2.art.maple.scenario.PairingHand;
import it.uniroma2.art.maple.scenario.RefinablePairing;
import it.uniroma2.art.maple.scenario.ResourceLexicalizationSet;
import it.uniroma2.art.maple.scenario.SingleResourceMatchingProblem;
import it.uniroma2.art.maple.scenario.Synonymizer;
import it.uniroma2.art.maple.sparql.GraphPattern;
import it.uniroma2.art.maple.utilities.ErrorRecoveringValueFactory;
import it.uniroma2.art.maple.vocabulary.MAPLE;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.rdf4j.http.client.SPARQLProtocolSession;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.DCAT;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.VOID;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * Implementation of {@link MediationFramework}.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class MediationFrameworkImpl implements MediationFramework {

    private static final Logger logger = LoggerFactory.getLogger(MediationFrameworkImpl.class);

    private ComponentRepository componentRepository;

    public ComponentRepository getComponentRepository() {
        return componentRepository;
    }

    public void setComponentRepository(ComponentRepository componentRepository) {
        this.componentRepository = componentRepository;
    }

    @Override
    public List<Collaboration> computeFlatAccesses(IRI model) {
        String accessRole = Collaboration.CONCEPTUAL_ACCESS_ROLE;
        IRI accessClass = MAPLE.FLAT_ACCESS;
        IRI queryProviderRequirement = MAPLE.REQUIRES_FLAT_ACCESS_QUERY_PROVIDER;
        IRI accessQueryProviderClass = MAPLE.FLAT_ACCESS_QUERY_PROVIDER;

        return computeAccessesHelper(model, accessRole, accessClass, queryProviderRequirement,
                accessQueryProviderClass);
    }

    @Override
    public List<Collaboration> computeHierarchicalAccesses(IRI model) {
        String accessRole = Collaboration.CONCEPTUAL_ACCESS_ROLE;
        IRI accessClass = MAPLE.HIERARCHICAL_ACCESS;
        IRI queryProviderRequirement = MAPLE.REQUIRES_HIERARCHICAL_ACCESS_QUERY_PROVIDER;
        IRI accessQueryProviderClass = MAPLE.HIERARCHICAL_ACCESS_QUERY_PROVIDER;

        return computeAccessesHelper(model, accessRole, accessClass, queryProviderRequirement,
                accessQueryProviderClass);
    }

    @Override
    public List<Collaboration> computeLabelAccesses(IRI model) {
        String accessRole = Collaboration.LINGUISTIC_ACCESS_ROLE;
        IRI accessClass = MAPLE.LABEL_ACCESS;
        IRI queryProviderRequirement = MAPLE.REQUIRES_LABEL_ACCESS_QUERY_PROVIDER;
        IRI accessQueryProviderClass = MAPLE.LABEL_ACCESS_QUERY_PROVIDER;

        return computeAccessesHelper(model, accessRole, accessClass, queryProviderRequirement,
                accessQueryProviderClass);
    }

    @Override
    public List<Collaboration> computeTerminologyAccesses(IRI model) {
        String accessRole = Collaboration.LINGUISTIC_ACCESS_ROLE;
        IRI accessClass = MAPLE.TERMINOLOGY_ACCESS;
        IRI queryProviderRequirement = MAPLE.REQUIRES_TERMINOLOGY_ACCESS_QUERY_PROVIDER;
        IRI accessQueryProviderClass = MAPLE.TERMINOLOGY_ACCESS_QUERY_PROVIDER;

        return computeAccessesHelper(model, accessRole, accessClass, queryProviderRequirement,
                accessQueryProviderClass);
    }

    @Override
    public List<Collaboration> computeFlatAccessQueryProviders(IRI model) {
        String accessQueryProviderRole = Collaboration.CONCEPTUAL_ACCESS_QUERY_PROVIDER_ROLE;
        IRI accessClass = MAPLE.FLAT_ACCESS_QUERY_PROVIDER;

        return computeAccessQueryProvidersHelper(model, accessQueryProviderRole, accessClass);
    }

    @Override
    public List<Collaboration> computeHierarchicalAccessQueryProviders(IRI model) {
        String accessQueryProviderRole = Collaboration.CONCEPTUAL_ACCESS_QUERY_PROVIDER_ROLE;
        IRI accessQueryProviderClass = MAPLE.HIERARCHICAL_ACCESS_QUERY_PROVIDER;

        return computeAccessQueryProvidersHelper(model, accessQueryProviderRole, accessQueryProviderClass);
    }

    @Override
    public List<Collaboration> computeLabelAccessQueryProviders(/* @Nullable */ IRI model) {
        String accessQueryProviderRole = Collaboration.LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE;
        IRI accessQueryProviderClass = MAPLE.LABEL_ACCESS_QUERY_PROVIDER;

        return computeAccessQueryProvidersHelper(model, accessQueryProviderRole, accessQueryProviderClass);
    }

    @Override
    public List<Collaboration> computeTerminologyAccessQueryProviders(IRI model) {
        String accessQueryProviderRole = Collaboration.LINGUISTIC_ACCESS_ROLE;
        IRI accessQueryProviderClass = MAPLE.TERMINOLOGY_ACCESS;

        return computeAccessQueryProvidersHelper(model, accessQueryProviderRole, accessQueryProviderClass);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public CollaborationInstance instantiateCollaboration(Collaboration collaboration,
                                                          Map<String, RepositoryConnection> connections)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        Collection<Participant> participants = collaboration.getParticipants();

        Collection<ParticipantComponent> participantComponents = collaboration.getParticipants().stream()
                .filter(ParticipantComponent.class::isInstance).map(ParticipantComponent.class::cast)
                .toList();

        if (participants.size() != participantComponents.size()) {
            throw new IllegalArgumentException(
                    "The provided collaboration includes non-component participants");
        }

        Map<String, Object> roleObjectMapping = new LinkedHashMap<>();

        for (ParticipantComponent participantComponent : participantComponents) {
            String role = participantComponent.getRole();
            Component component = participantComponent.getComponent();
            Manifestation manifestation = component.getManifestation();
            Object instance = componentRepository.instantiateParticipant(manifestation.getJavaClassName(), manifestation.getPluginCoordinates());
            roleObjectMapping.put(role, instance);
        }

        RepositoryConnection conn = connections.values().stream().findAny().orElse(null);

        for (Object obj : roleObjectMapping.values()) {
            if (obj instanceof ConceptualAccessQueryProviderAware<?>) {
                ((ConceptualAccessQueryProviderAware) obj)
                        .setConceptualAccessQueryProvider(Objects.requireNonNull(
                                roleObjectMapping.get(Collaboration.CONCEPTUAL_ACCESS_QUERY_PROVIDER_ROLE)));
            }

            if (obj instanceof LinguisticAccessQueryProviderAware<?>) {
                ((LinguisticAccessQueryProviderAware) obj)
                        .setLinguisticAccessQueryProvider(Objects.requireNonNull(
                                roleObjectMapping.get(Collaboration.LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE)));
            }

            if (obj instanceof RepositoryConnectionAware) {
                ((RepositoryConnectionAware) obj).setRepositoryConnection(Objects.requireNonNull(conn));
            }
        }

        for (Object obj : roleObjectMapping.values()) {
            for (Method m : obj.getClass().getDeclaredMethods()) {
                if ((m.getModifiers() & Modifier.PUBLIC) == 0)
                    continue;
                if (m.isAnnotationPresent(PostConstruct.class)) {
                    m.invoke(obj);
                }
            }
        }

        return new CollaborationInstanceImpl(collaboration, roleObjectMapping);
    }

    protected List<Collaboration> computeAccessesHelper(IRI model, String accessRole, IRI accessClass,
                                                        IRI queryProviderRequirement, IRI accessQueryProviderClass)
            throws RepositoryException, MalformedQueryException, QueryEvaluationException {
        List<Collaboration> candidateCollaborations = new ArrayList<>();

        try (RepositoryConnection conn = componentRepository.getConnection()) {
            TupleQuery query = conn.prepareTupleQuery(
                    // @formatter:off
                    "prefix maple: <http://art.uniroma2.it/ontologies/maple/core#>                       \n" +
                            "                                                                                    \n" +
                            "select * {                                                                          \n" +
                            "  ?" + accessRole + " a " + RenderUtils.toSPARQL(accessClass) + " ;                \n" +
                            "    maple:supportedModel ?" + accessRole + "SupportedModelT ;                       \n" +
                            "    maple:manifestation [                                                           \n" +
                            "      maple:javaClassName ?" + accessRole + "JavaClassName ;                        \n" +
                            "      maple:plugin [                                                                \n" +
                            "        maple:pluginId ?" + accessRole + "BundleSymbolicName ;                  \n" +
                            "        maple:version ?" + accessRole + "BundleVersion                              \n" +
                            "      ]                                                                             \n" +
                            "    ].                                                                              \n" +
                            "    FILTER(isBlank(?" + accessRole + "SupportedModelT) || sameTerm(?" + accessRole + "SupportedModelT, ?model)) \n" +
                            "                                                                                    \n" +
                            "    OPTIONAL {                                                                      \n" +
                            "      ?" + accessRole + " " + RenderUtils.toSPARQL(queryProviderRequirement) + " [  \n" +
                            "        maple:supportedModel ?" + accessRole + "SupportedModelT2                    \n" +
                            "      ] .                                                                           \n" +
                            "      ?" + accessRole + "QueryProvider a " + RenderUtils.toSPARQL(accessQueryProviderClass) + "; \n" +
                            "        maple:supportedModel ?" + accessRole + "QueryProviderSupportedModelT3 ;     \n" +
                            "        maple:manifestation [                                                       \n" +
                            "          maple:javaClassName ?" + accessRole + "QueryProviderJavaClassName ;       \n" +
                            "          maple:plugin [                                                            \n" +
                            "            maple:pluginId ?" + accessRole + "QueryProviderBundleSymbolicName ; \n" +
                            "            maple:version ?" + accessRole + "QueryProviderBundleVersion             \n" +
                            "          ]                                                                         \n" +
                            "        ].                                                                          \n" +
                            "      FILTER(isBlank(?" + accessRole + "SupportedModelT2) || isBlank(?" + accessRole + "QueryProviderSupportedModelT3) || sameTerm(?" + accessRole + "SupportedModelT2, ?" + accessRole + "QueryProviderSupportedModelT3))\n" +
                            "      FILTER(isBlank(?" + accessRole + "QueryProviderSupportedModelT3) || sameTerm(?" + accessRole + "QueryProviderSupportedModelT3, ?model)) \n" +
                            "      BIND(?model as ?" + accessRole + "SupportedModel)                             \n" +
                            "      BIND(IF(sameTerm(?" + accessRole + "QueryProviderSupportedModelT3, ?" + accessRole + "SupportedModel), ?model, ?" + accessRole + "QueryProviderSupportedModelT3) as ?" + accessRole + "QueryProviderSupportedModel)\n" +
                            "    }                                                                               \n" +
                            "}                                                                                   \n"
                    // @formatter:on
            );
            query.setBinding("model", model);

            try (TupleQueryResult bindings = query.evaluate()) {
                while (bindings.hasNext()) {
                    BindingSet bindingSet = bindings.next();
                    ParticipantComponent accessParticipantComponent = buildParticipantFromBindings(bindingSet,
                            accessRole);
                    ParticipantComponent accessQueryProviderParticipantComponent = buildParticipantFromBindings(
                            bindingSet, accessRole + "QueryProvider");

                    Collection<Participant> participants = new ArrayList<>();
                    participants.add(accessParticipantComponent);
                    if (accessQueryProviderParticipantComponent != null) {
                        participants.add(accessQueryProviderParticipantComponent);
                    }

                    candidateCollaborations.add(new CollaborationImpl(participants));
                }
            }
        }

        return candidateCollaborations;
    }

    protected List<Collaboration> computeAccessQueryProvidersHelper(/* Nullable */IRI model,
                                                                                  String accessQueryProviderRole, IRI accessQueryProviderClass)
            throws RepositoryException, MalformedQueryException, QueryEvaluationException {
        List<Collaboration> candidateCollaborations = new ArrayList<>();

        try (RepositoryConnection conn = componentRepository.getConnection()) {
            TupleQuery query = conn.prepareTupleQuery(
                    // @formatter:off
                    "prefix maple: <http://art.uniroma2.it/ontologies/maple/core#>                       \n" +
                            "                                                                                    \n" +
                            "select * {                                                                          \n" +
                            "  ?" + accessQueryProviderRole + " a " + RenderUtils.toSPARQL(accessQueryProviderClass) + " ; \n" +
                            "    maple:supportedModel ?" + accessQueryProviderRole + "SupportedModelT ;          \n" +
                            "    maple:manifestation [                                                           \n" +
                            "      maple:javaClassName ?" + accessQueryProviderRole + "JavaClassName ;           \n" +
                            "      maple:plugin [                                                                \n" +
                            "        maple:pluginId ?" + accessQueryProviderRole + "BundleSymbolicName ;     \n" +
                            "        maple:version ?" + accessQueryProviderRole + "BundleVersion                 \n" +
                            "      ]                                                                             \n" +
                            "    ].                                                                              \n" +
                            (model != null ?
                                    "    FILTER(isBlank(?" + accessQueryProviderRole + "SupportedModelT) || sameTerm(?" + accessQueryProviderRole + "SupportedModelT, ?model)) \n" +
                                            "    BIND(?model as ?" + accessQueryProviderRole + "SupportedModel)                  \n"
                                    :
                                    "    BIND(?" + accessQueryProviderRole + "SupportedModelT as ?" + accessQueryProviderRole + "SupportedModel) \n") +
                            "}                                                                                   \n"
                    // @formatter:on
            );
            if (model != null) {
                query.setBinding("model", model);
            }

            try (TupleQueryResult bindings = query.evaluate()) {
                while (bindings.hasNext()) {
                    BindingSet bindingSet = bindings.next();
                    ParticipantComponent accessParticipantComponent = buildParticipantFromBindings(bindingSet,
                            accessQueryProviderRole);

                    Collection<Participant> participants = new ArrayList<>();
                    participants.add(accessParticipantComponent);

                    candidateCollaborations.add(new CollaborationImpl(participants));
                }
            }
        }

        return candidateCollaborations;
    }

    @Override
    public void discoverLexicalizationSets(RepositoryConnection metadataConn, IRI dataset)
            throws AssessmentException {
        logger.debug("Discover lexicalization sets of the dataset {}", dataset);
        TupleQuery metadataQuery = metadataConn.prepareTupleQuery(
                // @formatter:off
                "prefix void: <http://rdfs.org/ns/void#>                 \n" +
                        "SELECT ?datasetSPARQLEndpoint ?datasetUriSpace {        \n" +
                        "  ?dataset void:sparqlEndpoint ?datasetSPARQLEndpoint ; \n" +
                        "           void:uriSpace ?datasetUriSpace .             \n" +
                        "}                                                       \n" +
                        "LIMIT 1                                                 \n"
                // @formatter:on
        );
        metadataQuery.setBinding("dataset", dataset);

        List<BindingSet> metadataResults = QueryResults.asList(metadataQuery.evaluate());

        IRI sparqlEndpoint = metadataResults.stream().map(bs -> bs.getValue("datasetSPARQLEndpoint"))
                .filter(IRI.class::isInstance).map(IRI.class::cast).findAny()
                .orElseThrow(() -> new AssessmentException(
                        "Unable to assess the lexicalization sets of a dataset without its SPARQL endpoint"));

        Literal uriSpace = metadataResults.stream().map(bs -> bs.getValue("datasetUriSpace"))
                .filter(Literal.class::isInstance).map(Literal.class::cast).findAny()
                .orElseThrow(() -> new AssessmentException(
                        "Unable to assess the lexicalization set of a dataset without its URI space"));

        logger.debug("Assess lexicalizaiton sets using sparqlEndpoint = \"{}\" and uriSpace = \"{}\"",
                sparqlEndpoint, uriSpace.stringValue());

        List<Collaboration> labelAccessCollaborations = computeLabelAccessQueryProviders(null);
        if (labelAccessCollaborations.isEmpty()) {
            logger.debug("Abort assesment because " + LabelAccessQueryProvider.class.getSimpleName()
                    + " is available");
            return;
        }

        SPARQLRepository sparqlRepository = new SPARQLRepository(sparqlEndpoint.stringValue()) {
            @Override
            protected SPARQLProtocolSession createHTTPClient() {
                SPARQLProtocolSession rv = super.createHTTPClient();
                rv.setValueFactory(ErrorRecoveringValueFactory.getInstance());
                return rv;
            }
        };
        sparqlRepository.init();

        try (RepositoryConnection dataConnection = sparqlRepository.getConnection()) {

            int maxLang = 0;
            Model assessedMetadata = null;

            for (Collaboration labelAccessCollaboration : labelAccessCollaborations) {

                Map<String, RepositoryConnection> connections = new HashMap<>();
                connections.put("target", dataConnection);
                CollaborationInstance labelAccessCollaborationInstance = instantiateCollaboration(
                        labelAccessCollaboration, connections);
                LabelAccessQueryProvider labelAccessQueryProvider = (LabelAccessQueryProvider) labelAccessCollaborationInstance
                        .getRoleObjectMapping().get(Collaboration.LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE);
                ParticipantComponent labelAccessQueryProviderComponent = (ParticipantComponent) labelAccessCollaboration
                        .getParticipants().stream()
                        .filter(p -> Collaboration.LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE.equals(p.getRole()))
                        .findAny().orElseThrow(() -> new IllegalStateException());

                GraphPattern<Literal> labelsGraphPattern = labelAccessQueryProvider.listLabels();
                String labelGPString = labelsGraphPattern.getGraphPattern();

                String labelQueryString =
                        // @formatter:off
                        labelsGraphPattern.getNamespaces().stream()
                                .map(ns -> "PREFIX " + ns.getPrefix() + ": <" + ns.getName() + "> \n")
                                .collect(joining()) +
                                "SELECT DISTINCT ?lang {\n" +
                                "{SELECT DISTINCT ?resource {\n" +
                                labelGPString +
                                "FILTER(STRSTARTS(STR(?resource), ?datasetUriSpace))" +
                                "} LIMIT 100}\n" +
                                labelGPString +
                                "BIND(LANG(?label) as ?lang)\n" +
                                "}\n";
                // @formatter:on

                IRI model = ((QueryProviderComponent) labelAccessQueryProviderComponent.getComponent())
                        .getModel();

                logger.debug("Assess using the lexicalization model {}",
                        NTriplesUtil.toNTriplesString(model));
                logger.debug("Assessment query:\n{}", labelQueryString);
                logger.debug("Variable bindings:  datasetUriSpace = {}", uriSpace);
                TupleQuery labelQuery = dataConnection.prepareTupleQuery(labelQueryString);
                labelQuery.setBinding("datasetUriSpace", uriSpace);
                List<BindingSet> results = QueryResults.asList(labelQuery.evaluate());

                logger.debug("Assessment query terminated with {} result(s)", results.size());

                if (results.size() >= maxLang) {
                    String ns = metadataConn.getNamespace("");
                    ValueFactory vf = metadataConn.getValueFactory();
                    ModelBuilder metadataBuilder = new ModelBuilder();

                    for (BindingSet bs : results) {
                        IRI lexicalizationSet = vf.createIRI(ns, UUID.randomUUID().toString());

                        metadataBuilder
                                // @formatter:off
                                .subject(lexicalizationSet)
                                .add(RDF.TYPE, LIME.LEXICALIZATION_SET)
                                .add(LIME.REFERENCE_DATASET, dataset)
                                .add(LIME.LEXICALIZATION_MODEL, model)
                                .add(LIME.LANGUAGE, bs.getValue("lang"))
                                .subject(dataset)
                                .add(VOID.SUBSET, lexicalizationSet);
                        // @formatter:on
                    }

                    assessedMetadata = metadataBuilder.build();
                    maxLang = results.size();

                }
            }

            if (assessedMetadata != null) {
                metadataConn.add(assessedMetadata);
                Update timestampUpdate = metadataConn.prepareUpdate(
                        // @formatter:off
                        "DELETE {\n" +
                                "  ?catalogRecord " + RenderUtils.toSPARQL(DCTERMS.MODIFIED) + " ?oldModified . \n" +
                                "}\n" +
                                "INSERT {\n" +
                                "  ?catalogRecord " + RenderUtils.toSPARQL(DCTERMS.MODIFIED) + " ?now . \n" +
                                "}\n" +
                                "WHERE {" +
                                "  ?catalogRecord a " + RenderUtils.toSPARQL(DCAT.CATALOG_RECORD) + " ; \n" +
                                "    " + RenderUtils.toSPARQL(FOAF.PRIMARY_TOPIC) + "|" + RenderUtils.toSPARQL(FOAF.TOPIC) + " " + RenderUtils.toSPARQL(dataset) + " \n" +
                                "    . \n" +
                                "  BIND(NOW() as ?now)\n" +
                                "  OPTIONAL{ ?catalogRecord " + RenderUtils.toSPARQL(DCTERMS.MODIFIED) + " ?oldModified .} \n" +
                                "}\n"
                        // @formatter:on
                );
                timestampUpdate.execute();
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException e) {
            throw new AssessmentException(e);
        }

    }

    @Override
    public List<ResourceLexicalizationSet> discoverLexicalizationSetsForResource(
            RepositoryConnection dataConn, IRI resource) throws AssessmentException {
        try {
            logger.debug("Discover lexicalization sets for resource {}", resource);
            Map<String, ResourceLexicalizationSet> lang2ResourceLexicalizationSet = new HashMap<>();
            List<Collaboration> labelAccessCollaborations = computeLabelAccessQueryProviders(null);
            if (labelAccessCollaborations.isEmpty()) {
                logger.debug("Abort assesment because " + LabelAccessQueryProvider.class.getSimpleName()
                        + " is available");
                return Collections.emptyList();
            }

            for (Collaboration labelAccessCollaboration : labelAccessCollaborations) {

                Map<String, RepositoryConnection> connections = new HashMap<>();
                connections.put("target", dataConn);
                CollaborationInstance labelAccessCollaborationInstance = instantiateCollaboration(
                        labelAccessCollaboration, connections);
                LabelAccessQueryProvider labelAccessQueryProvider = (LabelAccessQueryProvider) labelAccessCollaborationInstance
                        .getRoleObjectMapping().get(Collaboration.LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE);
                ParticipantComponent labelAccessQueryProviderComponent = (ParticipantComponent) labelAccessCollaboration
                        .getParticipants().stream()
                        .filter(p -> Collaboration.LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE.equals(p.getRole()))
                        .findAny().orElseThrow(() -> new IllegalStateException());

                GraphPattern<Literal> labelsGraphPattern = labelAccessQueryProvider.listLabels();
                String labelGPString = labelsGraphPattern.getGraphPattern();

                String labelQueryString =
                        // @formatter:off
                        labelsGraphPattern.getNamespaces().stream()
                                .map(ns -> "PREFIX " + ns.getPrefix() + ": <" + ns.getName() + "> \n")
                                .collect(joining()) +
                                "SELECT DISTINCT ?label {\n" +
                                labelGPString +
                                "}";
                // @formatter:on

                IRI model = ((QueryProviderComponent) labelAccessQueryProviderComponent.getComponent())
                        .getModel();

                logger.debug("Assess using the lexicalization model {}",
                        NTriplesUtil.toNTriplesString(model));
                logger.debug("Assessment query:\n{}", labelQueryString);
                logger.debug("Variable bindings:  resource = {}", resource);
                TupleQuery labelQuery = dataConn.prepareTupleQuery(labelQueryString);
                labelQuery.setBinding("resource", resource);

                Map<String, List<Literal>> lang2labels;
                try (TupleQueryResult queryResult = labelQuery.evaluate();
                     Stream<BindingSet> stream = QueryResults.stream(queryResult)) {
                    lang2labels = stream
                            .map(bs -> (Literal) bs.getValue("label"))
                            .collect(Collectors.groupingBy(lit -> lit.getLanguage().orElse("und")));
                }
                for (Map.Entry<String, List<Literal>> entry : lang2labels.entrySet()) {
                    String lang = entry.getKey();
                    List<Literal> labels = entry.getValue();

                    ResourceLexicalizationSet alreadyAvailableLexSet = lang2ResourceLexicalizationSet
                            .get(lang);
                    boolean replace;
                    if (alreadyAvailableLexSet == null) {
                        replace = true;
                    } else {
                        replace = alreadyAvailableLexSet.getLabels().size() < labels.size();
                    }

                    if (replace) {
                        lang2ResourceLexicalizationSet.put(lang,
                                new ResourceLexicalizationSet(lang, model, labels));
                    }
                }
                logger.debug("Assessment query terminated with {} result(s)", lang2labels.size());
            }
            return new ArrayList<>(lang2ResourceLexicalizationSet.values());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException e) {
            throw new AssessmentException(e);
        }
    }

    protected List<LexicalizationSetStatistics> extractLexicalizationSets(IRI dataset, Model profile) {
        return Models.objectIRIs(profile.filter(dataset, VOID.SUBSET, null)).stream()
                .filter(o -> profile.contains(o, RDF.TYPE, LIME.LEXICALIZATION_SET)
                        && profile.contains(o, LIME.REFERENCE_DATASET, dataset))
                .map(o -> {
                    LexicalizationSetStatistics stats = new LexicalizationSetStatistics();
                    Models.getPropertyIRI(profile, o, LIME.LEXICALIZATION_MODEL)
                            .ifPresent(stats::setLexicalizationModel);
                    Models.getPropertyIRI(profile, o, LIME.REFERENCE_DATASET)
                            .ifPresent(stats::setReferenceDataset);
                    Models.getPropertyIRI(profile, o, LIME.LEXICON_DATASET)
                            .ifPresent(stats::setLexiconDataset);
                    String langTag = Models.getPropertyString(profile, o, LIME.LANGUAGE).orElse("und");
                    stats.setLanguageTag(langTag);
                    Models.getPropertyLiteral(profile, o, LIME.LEXICAL_ENTRIES)
                            .map(l -> Literals.getIntegerValue(l, BigInteger.ZERO))
                            .ifPresent(stats::setLexicalEntries);
                    Models.getPropertyLiteral(profile, o, LIME.REFERENCES)
                            .map(l -> Literals.getIntegerValue(l, BigInteger.ZERO))
                            .ifPresent(stats::setReferences);
                    Models.getPropertyLiteral(profile, o, LIME.LEXICALIZATIONS)
                            .map(l -> Literals.getIntegerValue(l, BigInteger.ZERO))
                            .ifPresent(stats::setLexicalizations);
                    Models.getPropertyLiteral(profile, o, LIME.PERCENTAGE)
                            .map(l -> Literals.getDecimalValue(l, BigDecimal.ZERO))
                            .ifPresent(stats::setPercentage);
                    Models.getPropertyLiteral(profile, o, LIME.AVG_NUM_OF_LEXICALIZATIONS)
                            .map(l -> Literals.getDecimalValue(l, BigDecimal.ZERO))
                            .ifPresent(stats::setAvgNumOfLexicalizations);
                    return stats;
                }).filter(stats -> stats.getLexicalizationModel() != null && stats.getLanguageTag() != null)
                .collect(toList());
    }

    @Override
    public Optional<IRI> assessLexicalizationModel(IRI dataset, Model profile) throws AssessmentException {
        return extractLexicalizationSets(dataset, profile).stream()
                .map(LexicalizationSetStatistics::getLexicalizationModel)
                .sorted(LexicalizationModelComparator.instance.reversed()).findFirst();
    }

    @Override
    public AlignmentScenario profileAlignmentScenario(RepositoryConnection metadataConn, IRI sourceDataset,
                                                      IRI targetDataset) throws ProfilingException {
        return profileAlignmentScenario(metadataConn, sourceDataset, targetDataset, new ProfilerOptions());
    }

    @Override
    public AlignmentScenario profileAlignmentScenario(RepositoryConnection metadataConn, IRI sourceDataset,
                                                      IRI targetDataset, ProfilerOptions options) throws ProfilingException {

        // Obtains the SPARQL endpoint of each dataset

        Map<IRI, Dataset> inputDataset2object = obtainDatasetMetadata(metadataConn, sourceDataset, targetDataset);

        Dataset sourceDatasetDescription = inputDataset2object.get(sourceDataset);
        if (sourceDatasetDescription == null) {
            throw new ProfilingException("Unable to obtain the description of the source dataset");
        }
        Dataset targetDatasetDescription = inputDataset2object.get(targetDataset);
        if (targetDatasetDescription == null) {
            throw new ProfilingException("Unable to determine the description of the target dataset");
        }
        Set<Dataset> supportDatasets = new TreeSet<>(Comparator.comparing(ds -> ds.getId().stringValue()));
        List<RefinablePairing> pairings = new ArrayList<>();

        Function<IRI, List<LexicalizationSet>> lexicalizationSetRetriever = referenceDataset -> {
            TupleQuery lexicalizationSetQuery = metadataConn.prepareTupleQuery(
                    // @formatter:off
                    "PREFIX lime: <http://www.w3.org/ns/lemon/lime#>\n" +
                            "PREFIX void: <http://rdfs.org/ns/void#>\n" +
                            "select ?lexicalizationSet ?sparqlEndpoint ?lexicalizationModel ?languageTag ?referenceDataset ?lexicalEntries ?references ?lexicalizations ?percentage ?avgNumOfLexicalizations ?lexiconDataset  {\n" +
                            "    {SELECT ?lexicalizationSet (MIN(?sparqlEndpointT) as ?sparqlEndpoint) {\n" +
                            "        ?lexicalizationSet a lime:LexicalizationSet ;\n" +
                            "    	     lime:referenceDataset " + RenderUtils.toSPARQL(referenceDataset) + " ;\n" +
                            "            ^void:subset*/void:sparqlEndpoint ?sparqlEndpointT .\n" +
                            "    }\n" +
                            "    GROUP BY ?lexicalizationSet\n" +
                            "    HAVING BOUND(?lexicalizationSet)}\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:lexicalizationModel ?lexicalizationModel .\n" +
                            "    }\n" +
                            "    ?lexicalizationSet lime:language ?languageTag .\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:lexicalEntries ?lexicalEntries .\n" +
                            "    }\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:references ?references .\n" +
                            "    }\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:lexicalizations ?lexicalizations .\n" +
                            "    }\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:percentage ?percentage .\n" +
                            "    }\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:avgNumOfLexicalizations ?avgNumOfLexicalizations .\n" +
                            "    }\n" +
                            "    OPTIONAL {\n" +
                            "        ?lexicalizationSet lime:lexiconDataset ?lexiconDataset\n" +
                            "    }\n" +
                            "    BIND(" + RenderUtils.toSPARQL(referenceDataset) + "AS ?referenceDataset)\n" +
                            "}"
                    // @formatter:on
            );
            lexicalizationSetQuery.setIncludeInferred(false);

            try (TupleQueryResult queryResult = lexicalizationSetQuery.evaluate();
                 Stream<BindingSet> stream = QueryResults.stream(queryResult)) {
                return stream.map(this::bindingSet2LexicalizationSet).collect(Collectors.toList());
            }
        };

        // additional datasets (e.g. conceptualization sets and lexicon) that are candidate to become support
        // datasets
        Map<IRI, ConceptualizationSet> candidateConceptualizationSets = new HashMap<>();
        Map<IRI, Lexicon> candidateLexicons = new HashMap<>();
        Map<IRI, ConceptSet> candidateConceptSets = new HashMap<>();

        // Functions to retrieve synonymizers for a given language. Returns the discovered conceptualization
        // sets, while expanding the candidate sets above
        Function<String, Set<IRI>> retrieveSynonymizers = languageTag -> {
            GraphQuery synonymyzerQuery = metadataConn.prepareGraphQuery(
                    //@formatter:off
                    "PREFIX lime: <http://www.w3.org/ns/lemon/lime#>\n" +
                            "PREFIX void: <http://rdfs.org/ns/void#>\n" +
                            "prefix ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n" +
                            "prefix dcterms: <http://purl.org/dc/terms/>\n" +
                            "construct {\n" +
                            "    ?lexicon a lime:Lexicon ;\n" +
                            "        lime:language ?language ;\n" +
                            "        void:sparqlEndpoint ?lexiconSparqlEndpoint ;\n" +
                            "        lime:lexicalEntries ?lexiconLexicalEntries ;\n " +
                            "        dcterms:title ?lexiconTitle .\n" +
                            "    ?conceptualizationSet a lime:ConceptualizationSet ;\n" +
                            "        void:sparqlEndpoint ?conceptualizationSetSparqlEndpoint ;\n" +
                            "        lime:lexiconDataset ?lexicon ;\n" +
                            "        lime:conceptualDataset ?conceptSet;\n" +
                            "        lime:conceptualizations ?conceptualizationSetConceptualizations ;\n" +
                            "        lime:lexicalEntries ?conceptualizationSetLexicalEntries ;\n" +
                            "        lime:concepts ?conceptualizationSetConcepts ;\n" +
                            "        lime:avgAmbiguity ?conceptualizationSetAvgAmbiguity ;\n" +
                            "        lime:avgSynonymy ?conceptualizationSetAvgSynonymy ;\n" +
                            "        dcterms:title ?conceptualizationSetTitle\n" +
                            "        .\n" +
                            "    ?conceptSet a ontolex:ConceptSet;\n" +
                            "        void:sparqlEndpoint ?conceptSetSparqlEndpoint ;\n" +
                            "        lime:concepts ?conceptSetConcepts ;\n" +
                            "        dcterms:title ?conceptSetTitle \n" +
                            "	.\n" +
                            "}WHERE {\n" +
                            "    ?lexicon a lime:Lexicon ;\n" +
                            "        lime:language ?language ;\n" +
                            "        ^void:subset*/void:sparqlEndpoint ?lexiconSparqlEndpoint ;\n" +
                            "        lime:lexicalEntries ?lexiconLexicalEntries .\n" +
                            "    OPTIONAL { ?lexicon dcterms:title ?lexiconTitle. } \n" +
                            "    ?conceptualizationSet a lime:ConceptualizationSet ;\n" +
                            "        ^void:subset*/void:sparqlEndpoint ?conceptualizationSetSparqlEndpoint ;\n" +
                            "        lime:lexiconDataset ?lexicon ;\n" +
                            "        lime:conceptualDataset ?conceptSet;\n" +
                            "        lime:conceptualizations ?conceptualizationSetConceptualizations ;\n" +
                            "        lime:lexicalEntries ?conceptualizationSetLexicalEntries ;\n" +
                            "        lime:concepts ?conceptualizationSetConcepts ;\n" +
                            "        lime:avgAmbiguity ?conceptualizationSetAvgAmbiguity ;\n" +
                            "        lime:avgSynonymy ?conceptualizationSetAvgSynonymy ;\n" +
                            "        .\n" +
                            "    OPTIONAL { ?conceptualizationSet dcterms:title ?conceptualizationSetTitle. } \n" +
                            "    ?conceptSet a ontolex:ConceptSet;\n" +
                            "        ^void:subset*/void:sparqlEndpoint ?conceptSetSparqlEndpoint ;\n" +
                            "        lime:concepts ?conceptSetConcepts ;\n" +
                            "	.\n" +
                            "    OPTIONAL { ?conceptSet dcterms:title ?conceptSetTitle. } \n" +
                            "}"
                    //@formatter:on
            );
            synonymyzerQuery.setBinding("language",
                    SimpleValueFactory.getInstance().createLiteral(languageTag));

            Model synonymizerModel = QueryResults.asModel(synonymyzerQuery.evaluate());
            Set<IRI> conceptualizationSets = Models
                    .subjectIRIs(synonymizerModel.filter(null, RDF.TYPE, LIME.CONCEPTUALIZATION_SET));

            for (IRI conceptualizationSet : conceptualizationSets) {
                ConceptualizationSetStatistics stats = new ConceptualizationSetStatistics();
                stats.parse(synonymizerModel, conceptualizationSet);

                ConceptualizationSet conceptualizationSetDescription = new ConceptualizationSet(
                        conceptualizationSet, LIME.CONCEPTUALIZATION_SET, null,
                        Models.getPropertyLiterals(synonymizerModel, conceptualizationSet, DCTERMS.TITLE),
                        new DataService(Models
                                .getProperty(synonymizerModel, conceptualizationSet, VOID.SPARQL_ENDPOINT)
                                .map(Value::stringValue).orElseThrow(() -> new NullPointerException())),
                        stats);

                candidateConceptualizationSets.put(conceptualizationSet, conceptualizationSetDescription);

                IRI lexicon = conceptualizationSetDescription.getLexiconDataset();
                LexiconStats lexiconStats = new LexiconStats();
                lexiconStats.parse(synonymizerModel, lexicon);

                Lexicon lexiconDescription = new Lexicon(lexicon, LIME.LEXICON, null,
                        Models.getPropertyLiterals(synonymizerModel, lexicon, DCTERMS.TITLE),
                        new DataService(Models.getProperty(synonymizerModel, lexicon, VOID.SPARQL_ENDPOINT)
                                .map(Value::stringValue).orElseThrow(() -> new NullPointerException())),
                        lexiconStats);
                candidateLexicons.put(lexicon, lexiconDescription);

                IRI conceptSet = conceptualizationSetDescription.getConceptualDataset();
                ConceptSetStats conceptSetStats = new ConceptSetStats();
                conceptSetStats.parse(synonymizerModel, conceptSet);

                ConceptSet conceptSetDescription = new ConceptSet(conceptSet, ONTOLEX.CONCEPT_SET, null,
                        Models.getPropertyLiterals(synonymizerModel, conceptSet, DCTERMS.TITLE),
                        new DataService(Models.getProperty(synonymizerModel, conceptSet, VOID.SPARQL_ENDPOINT)
                                .map(Value::stringValue).orElseThrow(() -> new NullPointerException())),
                        conceptSetStats);
                candidateConceptSets.put(conceptSet, conceptSetDescription);
            }

            return conceptualizationSets;
        };

        // lexicalization sets of the source dataset
        List<LexicalizationSet> sourceLexicalizationSets = lexicalizationSetRetriever.apply(sourceDataset);

        // lexicalization sets of the target dataset
        List<LexicalizationSet> targetLexicalizationSets = lexicalizationSetRetriever.apply(targetDataset);

        for (LexicalizationSet sourceLexSet : sourceLexicalizationSets) {
            for (LexicalizationSet targetLexSet : targetLexicalizationSets) {
                String sourceLanguage = sourceLexSet.getLanguageTag();
                String targetLanguage = targetLexSet.getLanguageTag();

                // same language except for variants
                if (sourceLanguage.startsWith(targetLanguage) || targetLanguage.startsWith(sourceLanguage)) {
                    Set<IRI> conceptualizationSets = retrieveSynonymizers.apply(sourceLanguage); // TODO
                    // better
                    // handle
                    // language
                    // variants

                    final double ALPHA = 0.5;
                    final double BETA = 0.1;

                    double sourcePercentage = sourceLexSet.getPercentage();
                    double targetPercentage = targetLexSet.getPercentage();

                    double sourceExpressivity = sourceLexSet.getAvgNumOfLexicalizations() / sourcePercentage;
                    double targetExpressivity = targetLexSet.getAvgNumOfLexicalizations() / targetPercentage;

                    PriorityQueue<Pair<ConceptualizationSet, Double>> lrByContribution = new PriorityQueue<>(
                            Comparator.<Pair<ConceptualizationSet, Double>, Double>comparing(Pair::getValue)
                                    .reversed());

                    List<Synonymizer> synonymizers = new ArrayList<>();

                    if (options.getSynonymizerCandidateCap() != 0) {
                        for (IRI conceptualizationSet : conceptualizationSets) {
                            ConceptualizationSet conceptualizationSetDescription = candidateConceptualizationSets
                                    .get(conceptualizationSet);
                            Lexicon lexiconDescription = candidateLexicons
                                    .get(conceptualizationSetDescription.getLexiconDataset());

                            double anchorProb = (double) conceptualizationSetDescription
                                    .getConceptualizations()
                                    / Math.max(sourceLexSet.getLexicalizations(),
                                    targetLexSet.getLexicalizations());

                            double lrContribution = anchorProb
                                    * conceptualizationSetDescription.getAvgAmbiguity()
                                    * conceptualizationSetDescription.getAvgSynonymy()
                                    * lexiconDescription.getLexicalEntries()
                                    / (double) conceptualizationSetDescription.getLexicalEntries();

                            lrByContribution.add(Pair.of(conceptualizationSetDescription, lrContribution));
                        }

                        for (Pair<ConceptualizationSet, Double> pair : options
                                .getSynonymizerCandidateCap() > 0
                                ? Iterables.limit(lrByContribution,
                                options.getSynonymizerCandidateCap())
                                : lrByContribution) {
                            ConceptualizationSet conceptualizationSetDescription = pair.getKey();
                            double lrContribution = pair.getValue();

                            double sourceExpressivity2 = sourceExpressivity * (1 + lrContribution);
                            double targetExpressivity2 = targetExpressivity * (1 + lrContribution);

                            double combinedScore = sourcePercentage * targetPercentage * (1 - ALPHA * Math
                                    .exp(-BETA * (Math.max(sourceExpressivity2, targetExpressivity2) - 1)));

                            Synonymizer synonymizer = new Synonymizer(combinedScore,
                                    conceptualizationSetDescription.getLexiconDataset(),
                                    conceptualizationSetDescription.getId());
                            supportDatasets.add(conceptualizationSetDescription);
                            supportDatasets.add(candidateLexicons
                                    .get(conceptualizationSetDescription.getLexiconDataset()));
                            supportDatasets.add(candidateConceptSets
                                    .get(conceptualizationSetDescription.getConceptualDataset()));

                            synonymizers.add(synonymizer);
                        }

                    }

                    double score = sourcePercentage * targetPercentage * (1 - ALPHA
                            * Math.exp(-BETA * (Math.max(sourceExpressivity, targetExpressivity) - 1)));

                    double bestCombinedScore = synonymizers.isEmpty() ? score
                            : synonymizers.iterator().next().getScore();

                    PairingHand source = new PairingHand(sourceLexSet.getId());
                    PairingHand target = new PairingHand(targetLexSet.getId());
                    RefinablePairing pairing = new RefinablePairing(score, bestCombinedScore, source, target,
                            synonymizers);
                    supportDatasets.add(sourceLexSet);
                    supportDatasets.add(targetLexSet);
                    pairings.add(pairing);
                }
            }
        }
        pairings.sort(Comparator.comparing(RefinablePairing::getScore));

        Function<Dataset, List<Alignment>> alignmentsRetriever = referenceDataset -> {
            org.eclipse.rdf4j.query.algebra.evaluation.function.rdfterm.UUID uuidIRIgen = new org.eclipse.rdf4j.query.algebra.evaluation.function.rdfterm.UUID();

            TupleQuery alignmentsQuery = metadataConn.prepareTupleQuery(
                    // @formatter:off
                    "PREFIX void: <http://rdfs.org/ns/void#>\n" +
                        "select DISTINCT ?sparqlEndpoint ?subjectsTarget ?subjectsTargetNS ?objectsTarget ?objectsTargetNS {\n" +
                        "    ?linkset a void:Linkset ;\n" +
                        "        void:subjectsTarget ?subjectsTargetT ;\n" +
                        "        void:objectsTarget ?objectsTargetT ;\n" +
                        "        ^void:subset*/void:sparqlEndpoint ?sparqlEndpoint .\n" +
                        "        \n" +
                        "    ?subjectsTargetT void:uriSpace ?subjectsTargetNS .\n" +
                        "    OPTIONAL {\n" +
                        "        ?subjectsTargetT ^void:subset*/void:sparqlEndpoint ?subjectsTargetSparqlEndpoint .\n" +
                        "        BIND(?subjectsTargetT as ?subjectsTarget)\n" +
                        "    }\n" +
                        "    \n" +
                        "    ?objectsTargetT void:uriSpace ?objectsTargetNS .\n" +
                        "OPTIONAL {\n" +
                        "        ?objectsTargetT ^void:subset*/void:sparqlEndpoint ?objectsTargetSparqlEndpoint .\n" +
                        "        BIND(?objectsTargetT as ?objects)" +
                        "    }\n" +
                        "    FILTER(?subjectsTargetNS = ?targetNS || ?objectsTargetNS = ?targetNS)\n" +
                        "    \n" +
                        "}\n" +
                        "GROUP BY ?sparqlEndpoint ?subjectsTarget ?subjectsTargetNS ?objectsTarget ?objectsTargetNS\n" +
                        "HAVING (BOUND(?sparqlEndpoint) && BOUND(?subjectsTargetNS) && BOUND(?objectsTargetNS))\n"
                    // @formatter:on
            );
            alignmentsQuery.setIncludeInferred(false);
            alignmentsQuery.setBinding("targetNS", Values.literal(referenceDataset.getUriSpace()));

            List<Alignment> rv = new ArrayList<>();

            for (BindingSet bs : QueryResults.asList(alignmentsQuery.evaluate())) {
                IRI sparqlEndpoint = (IRI)bs.getValue("sparqlEndpoint");

                String subjectsTargetNS = bs.getValue("subjectsTargetNS").stringValue();
                List<IRI> subjectsTarget = Optional.ofNullable((IRI)bs.getValue("subjectsTarget")).map(v -> Lists.newArrayList(v)).orElseGet(ArrayList::new);

                if (subjectsTarget.isEmpty()) {
                    if (Objects.equals(subjectsTargetNS, sourceDatasetDescription.getUriSpace())) {
                        subjectsTarget.add(sourceDatasetDescription.getId());
                    } else if (Objects.equals(subjectsTargetNS, targetDatasetDescription.getUriSpace())) {
                        subjectsTarget.add(targetDatasetDescription.getId());
                    } else {
                        subjectsTarget = obtainDatasetMetadataFromNS(metadataConn, subjectsTargetNS).values().stream().map(Dataset::getId).collect(toList());
                    }
                }
                String objectsTargetNS = bs.getValue("objectsTargetNS").stringValue();
                List<IRI> objectsTarget = Optional.ofNullable((IRI)bs.getValue("objectsTarget")).map(v -> Lists.newArrayList(v)).orElseGet(ArrayList::new);
                if (Objects.equals(objectsTargetNS, sourceDatasetDescription.getUriSpace())) {
                    objectsTarget.add(sourceDatasetDescription.getId());
                } else if (Objects.equals(objectsTargetNS, targetDatasetDescription.getUriSpace())) {
                    objectsTarget.add(targetDatasetDescription.getId());
                }
                if (objectsTarget.isEmpty()) {
                    objectsTarget = obtainDatasetMetadataFromNS(metadataConn, objectsTargetNS).values().stream().map(Dataset::getId).collect(toList());
                }

                for (IRI st : subjectsTarget) {
                    for (IRI ot : objectsTarget) {
                        rv.add(new Alignment(uuidIRIgen.evaluate(SimpleValueFactory.getInstance()), null, Collections.emptySet(), new DataService(sparqlEndpoint.stringValue()), st, ot));
                    }
                }
            }

            return rv;
        };

        // alignments of the left dataset
        List<Alignment> leftAlignments = alignmentsRetriever.apply(sourceDatasetDescription);

        // alignments of the right dataset
        List<Alignment> rightAlignments = alignmentsRetriever.apply(targetDatasetDescription);

        List<AlignmentChain> alignmentChains = new ArrayList<>();

        for (Alignment firstAlignment : leftAlignments) {
            for (Alignment secondAlignment : rightAlignments) {
                IRI leftPivot = ObjectUtils.notEqual(sourceDataset, firstAlignment.getSubjectsTarget()) ? firstAlignment.getSubjectsTarget() : firstAlignment.getObjectsTarget();
                IRI rightPivot = ObjectUtils.notEqual(targetDataset, secondAlignment.getSubjectsTarget()) ? secondAlignment.getSubjectsTarget() : secondAlignment.getObjectsTarget();

                Map<IRI, Dataset> pivotDataset2object = obtainDatasetMetadata(metadataConn, Stream.of(leftPivot, rightPivot).distinct().toArray(s -> new IRI[s]));

                Dataset leftPivotObj = pivotDataset2object.get(leftPivot);
                Dataset rightPivotObj = pivotDataset2object.get(rightPivot);

                if (ObjectUtils.anyNull(leftPivotObj, rightPivotObj) || ObjectUtils.notEqual(leftPivotObj.getId(), rightPivotObj.getId())) continue; // skip non matching alignments

                double score = 1.0;

                supportDatasets.add(firstAlignment);
                supportDatasets.add(secondAlignment);
                supportDatasets.add(leftPivotObj);
                supportDatasets.add(rightPivotObj);

                alignmentChains.add(new AlignmentChain(score, Lists.newArrayList(firstAlignment.getId(), secondAlignment.getId())));

            }
        }

        return new AlignmentScenario(sourceDatasetDescription, targetDatasetDescription, supportDatasets,
                pairings, alignmentChains);
    }

    private static Map<IRI, Dataset> obtainDatasetMetadataFromNS(RepositoryConnection metadataConn, String uriSpace) {
        TupleQuery inputDatasetSparqlEndpointQuery = metadataConn.prepareTupleQuery(
                // @formatter:off
                "PREFIX void: <http://rdfs.org/ns/void#>\n" +
                        "PREFIX dcterms: <http://purl.org/dc/terms/>\n" +
                        "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
                        "PREFIX dcat: <http://www.w3.org/ns/dcat#>\n" +
                        "SELECT ?dataset ?uriSpace (MIN(?sparqlEndpointT) AS ?sparqlEndpoint) (MIN(?conformsToT) AS ?conformsTo) (GROUP_CONCAT(CONCAT(REPLACE(STR(?titleT),\",|\\\\\\\\\",\"\\\\\\\\$0\"),\",\",LANG(?titleT));separator=\",\") as ?title) {\n" +
                        "    ?dataset ^void:subset*/void:sparqlEndpoint ?sparqlEndpointT ;\n" +
                        "        void:uriSpace ?uriSpace . \n" +
                        "    FILTER EXISTS {\n" +
                        "        {\n" +
                        "            ?dataset ^foaf:primaryTopic/^dcat:record [] \n" +
                        "            FILTER NOT EXISTS {\n" +
                        "              ?dataset dcat:distribution []\n" +
                        "            " +
                        "}\n" +
                        "        } UNION {\n" +
                        "            ?dataset ^dcat:distribution/^foaf:primaryTopic/^dcat:record [] \n" +
                        "     " +
                        "   }\n" +
                        "    }\n" +
                        "  OPTIONAL {\n" +
                        "    ?dataset dcterms:conformsTo ?conformsToT .\n" +
                        "    FILTER(isIRI(?conformsToT))\n" +
                        "  }\n" +
                        "  OPTIONAL {\n" +
                        "    ?dataset dcterms:title ?titleT .\n" +
                        "    FILTER(isLiteral(?titleT))\n" +
                        "  }\n" +
                        "}\n" +
                        "GROUP BY ?dataset ?uriSpace \n" +
                        "HAVING BOUND(?sparqlEndpoint)\n" +
                        "VALUES(?uriSpace){(\"" + RenderUtils.escape(uriSpace) + "\")}"
                // @formatter:on
        );
        inputDatasetSparqlEndpointQuery.setIncludeInferred(false);
        Map<IRI, Dataset> dataset2object;
        try (TupleQueryResult queryResult = inputDatasetSparqlEndpointQuery.evaluate();
             Stream<BindingSet> stream = QueryResults.stream(queryResult)) {
            dataset2object = stream
                    .collect(Collectors.toMap(bs -> (IRI) bs.getValue("dataset"),
                            bs -> new Dataset((IRI) bs.getValue("dataset"), VOID.DATASET,
                                    bs.getValue("uriSpace").stringValue(),
                                    parseEncodedPlainLiteralSet(Optional.ofNullable(bs.getValue("title"))
                                            .map(Value::stringValue).orElse("")),
                                    new DataService(bs.getValue("sparqlEndpoint").stringValue()),
                                    (IRI) bs.getValue("conformsTo"))));
        }
        return dataset2object;

    }

    private static Map<IRI, Dataset> obtainDatasetMetadata(RepositoryConnection metadataConn, IRI... dataset) {
        TupleQuery inputDatasetSparqlEndpointQuery = metadataConn.prepareTupleQuery(
                // @formatter:off
                "PREFIX void: <http://rdfs.org/ns/void#>\n" +
                        "PREFIX dcterms: <http://purl.org/dc/terms/>\n" +
                        "SELECT ?dataset ?uriSpace (MIN(?sparqlEndpointT) AS ?sparqlEndpoint) (MIN(?conformsToT) AS ?conformsTo) (GROUP_CONCAT(CONCAT(REPLACE(STR(?titleT),\",|\\\\\\\\\",\"\\\\\\\\$0\"),\",\",LANG(?titleT));separator=\",\") as ?title) {\n" +
                        "    ?dataset ^void:subset*/void:sparqlEndpoint ?sparqlEndpointT ;\n" +
                        "        void:uriSpace ?uriSpace . \n" +
                        "  OPTIONAL {\n" +
                        "    ?dataset dcterms:conformsTo ?conformsToT .\n" +
                        "    FILTER(isIRI(?conformsToT))\n" +
                        "  }\n" +
                        "  OPTIONAL {\n" +
                        "    ?dataset dcterms:title ?titleT .\n" +
                        "    FILTER(isLiteral(?titleT))\n" +
                        "  }\n" +
                        "}\n" +
                        "GROUP BY ?dataset ?uriSpace \n" +
                        "HAVING BOUND(?sparqlEndpoint)\n" +
                        "VALUES(?dataset){" + Arrays.stream(dataset).map(d -> "(" + RenderUtils.toSPARQL(d) + ")").collect(Collectors.joining()) + "}"
                // @formatter:on
        );
        inputDatasetSparqlEndpointQuery.setIncludeInferred(false);

        Map<IRI, Dataset> dataset2object;
        try (TupleQueryResult queryResult = inputDatasetSparqlEndpointQuery.evaluate();
             Stream<BindingSet> stream = QueryResults.stream(queryResult)) {
            dataset2object = stream
                    .collect(Collectors.toMap(bs -> (IRI) bs.getValue("dataset"),
                            bs -> new Dataset((IRI) bs.getValue("dataset"), VOID.DATASET,
                                    bs.getValue("uriSpace").stringValue(),
                                    parseEncodedPlainLiteralSet(Optional.ofNullable(bs.getValue("title"))
                                            .map(Value::stringValue).orElse("")),
                                    new DataService(bs.getValue("sparqlEndpoint").stringValue()),
                                    (IRI) bs.getValue("conformsTo"))));
        }
        return dataset2object;
    }

    private static List<Literal> parseEncodedPlainLiteralList(String value) {
        List<String> rawList = Utilities.splitCsvString(value);
        List<Literal> processedList = new ArrayList<>(rawList.size() / 2);
        Iterator<String> it = rawList.iterator();
        SimpleValueFactory vf = SimpleValueFactory.getInstance();

        while (it.hasNext()) {
            String label = it.next();
            String langTag = it.next();

            if (langTag.isEmpty()) {
                processedList.add(vf.createLiteral(label));
            } else {
                processedList.add(vf.createLiteral(label, langTag));
            }
        }

        return processedList;
    }

    private static Set<Literal> parseEncodedPlainLiteralSet(String value) {
        return new HashSet<>(parseEncodedPlainLiteralList(value));
    }

    private LexicalizationSet bindingSet2LexicalizationSet(BindingSet bs) {
        IRI id = (IRI) bs.getValue("lexicalizationSet");

        LexicalizationSetStatistics stats = new LexicalizationSetStatistics();
        stats.setReferenceDataset((IRI) bs.getValue("referenceDataset"));
        if (bs.hasBinding("lexicalizationModel")) {
            stats.setLexicalizationModel((IRI) bs.getValue("lexicalizationModel"));
        }
        stats.setLanguageTag(bs.getValue("languageTag").stringValue());
        if (bs.hasBinding("lexicalEntries")) {
            stats.setLexicalEntries(Literals.getIntegerValue(bs.getValue("lexicalEntries"), BigInteger.ZERO));
        }
        if (bs.hasBinding("references")) {
            stats.setReferences(Literals.getIntegerValue(bs.getValue("references"), BigInteger.ZERO));
        }
        if (bs.hasBinding("lexicalizations")) {
            stats.setLexicalizations(
                    Literals.getIntegerValue(bs.getValue("lexicalizations"), BigInteger.ZERO));
        }
        if (bs.hasBinding("percentage")) {
            stats.setPercentage(Literals.getDecimalValue(bs.getValue("percentage"), BigDecimal.ZERO));
        }
        if (bs.hasBinding("avgNumOfLexicalizations")) {
            stats.setAvgNumOfLexicalizations(
                    Literals.getDecimalValue(bs.getValue("avgNumOfLexicalizations"), BigDecimal.ZERO));
        }
        if (bs.hasBinding("lexiconDataset")) {
            stats.setLexiconDataset((IRI) bs.getValue("lexiconDataset"));
        }
        String sparqlEndpoint;
        if (bs.hasBinding("sparqlEndpoint")) {
            sparqlEndpoint = bs.getValue("sparqlEndpoint").stringValue();
        } else {
            sparqlEndpoint = null;
        }

        return new LexicalizationSet(id, null, Collections.emptySet(),
                sparqlEndpoint != null ? new DataService(sparqlEndpoint) : null, stats);
    }

    @Override
    public SingleResourceMatchingProblem profileSingleResourceMatchingProblem(IRI resource,
                                                                              List<ResourceLexicalizationSet> resourceLexicalizationSets, IRI targetDataset,
                                                                              Model targetDatasetProfile) throws ProfilingException {
        Map<String, List<ResourceLexicalizationSet>> resourceLexicalizationSetsByLanguage = resourceLexicalizationSets
                .stream().collect(groupingBy(ResourceLexicalizationSet::getLanguageTag));
        Map<String, ResourceLexicalizationSet> reducedResourceLexicalizationSets = reduceResourceLexicalizationSets(
                resourceLexicalizationSetsByLanguage);

        if (reducedResourceLexicalizationSets.isEmpty()) {
            throw new ProfilingException("The source resource has no lexicalization");
        }

        List<LexicalizationSetStatistics> targetDatasetLexicalizationSets = extractLexicalizationSets(
                targetDataset, targetDatasetProfile);
        Map<String, List<LexicalizationSetStatistics>> targetDatasetLexicalizationSetsByLanguage = targetDatasetLexicalizationSets
                .stream().collect(groupingBy(LexicalizationSetStatistics::getLanguageTag));
        Map<String, LexicalizationSetStatistics> reducedTargetLexicalizationSets = reduceLexicalizationSets(
                targetDatasetLexicalizationSetsByLanguage);

        if (reducedTargetLexicalizationSets.isEmpty()) {
            throw new ProfilingException("The target dataset has no lexicalization set");
        }

        List<Pair<ResourceLexicalizationSet, LexicalizationSetStatistics>> pairedLexicalizationSet = reducedResourceLexicalizationSets
                .entrySet().stream()
                .filter(entry -> reducedTargetLexicalizationSets.containsKey(entry.getKey()))
                .map(entry -> ImmutablePair.of(entry.getValue(),
                        reducedTargetLexicalizationSets.get(entry.getKey())))
                .sorted(Comparator
                        .comparing(
                                (Pair<ResourceLexicalizationSet, LexicalizationSetStatistics> pair) -> pair
                                        .getRight().getPercentage(),
                                Comparator.nullsFirst(Comparator.naturalOrder()))
                        .reversed())
                .collect(toList());
        return new SingleResourceMatchingProblem(resource, targetDataset, pairedLexicalizationSet);
    }

    private List<Pair<LexicalizationSetStatistics, LexicalizationSetStatistics>> computeSortedSharedLexicalizationSets(
            Map<String, LexicalizationSetStatistics> reducedLexicalizationSets1,
            Map<String, LexicalizationSetStatistics> reducedLexicalizationSets2) {

        List<Pair<LexicalizationSetStatistics, LexicalizationSetStatistics>> lexicalizationSetPairs = new ArrayList<>();
        for (Entry<String, LexicalizationSetStatistics> entry1 : reducedLexicalizationSets1.entrySet()) {
            String language = entry1.getKey();
            LexicalizationSetStatistics lexicalizationSet1 = entry1.getValue();
            LexicalizationSetStatistics lexicalizationSet2 = reducedLexicalizationSets2.get(language);
            if (lexicalizationSet2 == null)
                continue;
            lexicalizationSetPairs.add(ImmutablePair.of(lexicalizationSet1, lexicalizationSet2));
        }

        lexicalizationSetPairs.sort(Comparator
                .comparingDouble((Pair<LexicalizationSetStatistics, LexicalizationSetStatistics> pair) -> {
                    LexicalizationSetStatistics stats1 = pair.getLeft();
                    LexicalizationSetStatistics stats2 = pair.getRight();

                    BigDecimal percentage1 = stats1.getPercentage();
                    if (percentage1 == null)
                        percentage1 = BigDecimal.ONE;

                    BigDecimal percentage2 = stats2.getPercentage();
                    if (percentage2 == null)
                        percentage2 = BigDecimal.ONE;

                    return percentage1.multiply(percentage2).doubleValue();
                }).reversed());

        return lexicalizationSetPairs;
    }

    private Map<String, LexicalizationSetStatistics> reduceLexicalizationSets(
            Map<String, List<LexicalizationSetStatistics>> lexicalizationSetsByLanguage) {
        Map<String, LexicalizationSetStatistics> reducedLexicalizationSets = new HashMap<>();

        for (Map.Entry<String, List<LexicalizationSetStatistics>> entry : lexicalizationSetsByLanguage
                .entrySet()) {

            String language = entry.getKey();
            List<LexicalizationSetStatistics> sameLanguageLexicalizationSets = entry.getValue();

            LexicalizationSetStatistics bestLexicalizationSet = sameLanguageLexicalizationSets.stream()
                    .sorted(Comparator
                            .comparing(LexicalizationSetStatistics::getPercentage,
                                    Comparator.nullsFirst(Comparator.naturalOrder()))
                            .thenComparing(LexicalizationSetStatistics::getLexicalizationModel,
                                    LexicalizationModelComparator.instance)
                            .reversed())
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Unable to retrieve a lexicalization set"));

            reducedLexicalizationSets.put(language, bestLexicalizationSet);
        }

        return reducedLexicalizationSets;
    }

    private Map<String, ResourceLexicalizationSet> reduceResourceLexicalizationSets(
            Map<String, List<ResourceLexicalizationSet>> lexicalizationSetsByLanguage) {
        Map<String, ResourceLexicalizationSet> reducedLexicalizationSets = new HashMap<>();

        for (Map.Entry<String, List<ResourceLexicalizationSet>> entry : lexicalizationSetsByLanguage
                .entrySet()) {

            String language = entry.getKey();
            List<ResourceLexicalizationSet> sameLanguageLexicalizationSets = entry.getValue();

            ResourceLexicalizationSet bestLexicalizationSet = sameLanguageLexicalizationSets.stream()
                    .sorted(Comparator
                            .comparing((ResourceLexicalizationSet x) -> x.getLabels().size(),
                                    Comparator.naturalOrder())
                            .thenComparing(ResourceLexicalizationSet::getLexicalizationModel,
                                    LexicalizationModelComparator.instance)
                            .reversed())
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Unable to retrieve a lexicalization set"));

            reducedLexicalizationSets.put(language, bestLexicalizationSet);
        }

        return reducedLexicalizationSets;
    }

    private ParticipantComponent buildParticipantFromBindings(BindingSet bindings, String role) {
        if (!bindings.hasBinding(role))
            return null;

        IRI componentName = (IRI) bindings.getValue(role);
        String bundleSymbolicName = ((Literal) bindings.getValue(role + "BundleSymbolicName")).getLabel();
        String bundleVersion = ((Literal) bindings.getValue(role + "BundleVersion")).getLabel();
        String javaClassName = ((Literal) bindings.getValue(role + "JavaClassName")).getLabel();
        IRI model = (IRI) bindings.getValue(role + "SupportedModel");
        String hostingOBR = null;

        if (bindings.hasBinding(role + "HostingOBR")) {
            hostingOBR = bindings.getValue(role + "HostingOBR").stringValue();
        }
        Component component;
        if (role.equals("mediator")) {
            component = new MediatorComponentImpl(componentName,
                    new ManifestationImpl(
                            javaClassName, new PluginCoordinatesImpl(bundleSymbolicName, bundleVersion)
                    ),
                    model);

        } else if (role.startsWith("conceptualAccessQueryProvider")) {
            component = new ConceptualAccessQueryProviderComponentImpl(componentName,
                    new ManifestationImpl(
                            javaClassName, new PluginCoordinatesImpl(bundleSymbolicName, bundleVersion)
                    ),
                    model);
        } else if (role.startsWith("linguisticAccessQueryProvider")) {
            component = new LinguisticAccessQueryProviderComponentImpl(componentName,
                    new ManifestationImpl(
                            javaClassName, new PluginCoordinatesImpl(bundleSymbolicName, bundleVersion)
                    ),
                    model);
        } else if (role.startsWith("linguisticAccess")) {
            component = new LinguisticAccessComponentImpl(componentName,
                    new ManifestationImpl(
                            javaClassName, new PluginCoordinatesImpl(bundleSymbolicName, bundleVersion)
                    ),
                    model);

        } else if (role.startsWith("conceptualAccess")) {
            component = new ConceptualAccessComponentImpl(componentName,
                    new ManifestationImpl(
                            javaClassName, new PluginCoordinatesImpl(bundleSymbolicName, bundleVersion)
                    ),
                    model);
        } else
            throw new RuntimeException("Unknown component type");

        return new ParticipantingComponentImpl(role, component);
    }

    // @Override
    // public RDFModel loadDataset(Class<? extends RDFModel> modelClass, String baseuri,
    // String persistenceDirectory) throws ModelCreationException, ModelAccessException, IOException,
    // ModelUpdateException, UnsupportedRDFFormatException, ClassNotFoundException {
    // OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
    // .createModelFactory(getModelFactory());
    // return (RDFModel) fact.loadModel(modelClass, baseuri, persistenceDirectory);
    // }

    // @Override
    // public LIMEModel loadLIMEModel(String baseuri, String persistenceDirectory) throws
    // ModelCreationException,
    // ModelAccessException, IOException, ModelUpdateException, UnsupportedRDFFormatException {
    // LIMEModelFactory limeFact = LIMEModelFactory.createModelFactory(getModelFactory());
    // return limeFact.loadLIMEModel(baseuri, persistenceDirectory);
    // }

    // @Override
    // public void profileDataset(RDFModel model, LIMEModel limeModel)
    // throws ModelCreationException, ProfilerException, ModelUpdateException, FileNotFoundException,
    // IOException, ModelAccessException, UnsupportedRDFFormatException {
    // LIMEProfiler profiler = new LIMEProfiler();
    // profiler.profile(model, limeModel);
    // }

    // @Override
    // public MediationProblem profileProblem(LIMEModel profile1, LIMEModel profile2) throws Exception {
    // Set<String> languages1 = new HashSet<String>();
    // Iterators.addAll(languages1, profile1.listLanguages());
    //
    // Set<String> languages2 = new HashSet<String>();
    // Iterators.addAll(languages2, profile1.listLanguages());
    //
    // Set<String> intersection = new HashSet<String>(languages1);
    // intersection.retainAll(languages2);
    //
    // System.out.println("Languages1 : " + languages1);
    // System.out.println("Languages2 : " + languages2);
    // System.out.println("Intersection : " + intersection);
    //
    // if (languages1.isEmpty()) {
    // UnknownProblem problem = new UnknownProblemImpl();
    // problem.setFirstProfile(profile1);
    // problem.setSecondProfile(profile2);
    // problem.setMessage("First input dataset has no natural language characterization");
    // return problem;
    // }
    //
    // if (languages2.isEmpty()) {
    // UnknownProblem problem = new UnknownProblemImpl();
    // problem.setFirstProfile(profile1);
    // problem.setSecondProfile(profile2);
    // problem.setMessage("Second input dataset has no natural language characterization");
    // return problem;
    // }
    //
    // int commonLanguagesCount = intersection.size();
    // if (commonLanguagesCount == 0) {
    // CrossLingualProblem problem = new CrossLingualProblemImpl();
    // problem.setFirstProfile(profile1);
    // problem.setSecondProfile(profile2);
    //
    // return problem;
    // } else if (commonLanguagesCount == 1) {
    // MonolingualProblem problem = new MonolingualProblemImpl();
    // problem.setFirstProfile(profile1);
    // problem.setSecondProfile(profile2);
    // problem.setLanguage(intersection.iterator().next());
    // return problem;
    // } else {
    // MultilingualProblem problem = new MultilingualProblemImpl();
    // problem.setFirstProfile(profile1);
    // problem.setSecondProfile(profile2);
    // problem.setLanguages(intersection);
    // return problem;
    // }
    // }

    // @Override
    // public List<Strategy> computeStrategies(MediationProblem mediationProblem)
    // throws IOException, RDF4JException, ClassNotFoundException {
    // Repository firstProfile = mediationProblem.getFirstProfile();
    //
    // String mediationScenarioBindings;
    //
    // if (mediationProblem instanceof MonolingualProblem) {
    // mediationScenarioBindings = SparqlUtilities.tableToBindings(
    // new String[] { "requiredMediationScenario", "mediationScenarioMismatch" },
    // new Object[] { MAPLE.MONOLINGUALMEDIATION, 0, MAPLE.MULTILINGUALMEDIATION, 1 });
    // } else if (mediationProblem instanceof MultilingualProblem) {
    // mediationScenarioBindings = SparqlUtilities.tableToBindings(
    // new String[] { "requiredMediationScenario", "mediationScenarioMismatch" },
    // new Object[] { MAPLE.MULTILINGUALMEDIATION, 0, MAPLE.MONOLINGUALMEDIATION, 1 });
    // } else if (mediationProblem instanceof CrossLingualProblem) {
    // mediationScenarioBindings = SparqlUtilities.tableToBindings(
    // new String[] { "requiredMediationScenario", "mediationScenarioMismatch" },
    // new Object[] { MAPLE.CROSSLINGUALMEDIATION, 0 });
    // } else {
    // mediationScenarioBindings = "";
    // }
    //
    // String datasetType1Fragment = SparqlUtilities.tableToBindings(new String[] { "datasetType1" },
    // RDFIterators.getCollectionFromIterator(firstProfile.listDatasetTypes()).toArray());
    //
    // String linguisticModel1Fragment = SparqlUtilities.tableToBindings(new String[] { "linguisticModel1" },
    // RDFIterators.getCollectionFromIterator(firstProfile.listLinguisticModels()).toArray());
    //
    // LIMEModel secondProfile = mediationProblem.getSecondProfile();
    //
    // String datasetType2Fragment = SparqlUtilities.tableToBindings(new String[] { "datasetType2" },
    // RDFIterators.getCollectionFromIterator(secondProfile.listDatasetTypes()).toArray());
    //
    // String linguisticModel2Fragment = SparqlUtilities.tableToBindings(new String[] { "linguisticModel2" },
    // RDFIterators.getCollectionFromIterator(secondProfile.listLinguisticModels()).toArray());
    //
    // String remoteSparqlEndpointsFragment = SparqlUtilities.tableToBindings(new String[] { "endpoint" },
    // componentRepository.getRemoteSparqlEndpoints());
    //
    // String queryTemplate = Utilities.readResoruceToString(this, "computeStrategies.sparql");
    // String querySpec = queryTemplate.replace("#remoteSparqlEndpoints#", remoteSparqlEndpointsFragment)
    // .replace("#datasetType1#", datasetType1Fragment)
    // .replace("#linguisticModel1#", linguisticModel1Fragment)
    // .replace("#datasetType2#", datasetType2Fragment)
    // .replace("#linguisticModel2#", linguisticModel2Fragment)
    // .replace("#mediationScenarioBindings#", mediationScenarioBindings);
    //
    // logger.debug(querySpec);
    //
    // TupleQuery query = componentRepository.createTupleQuery(querySpec);
    // TupleBindingsIterator resultIter = query.evaluate(false);
    //
    // List<Strategy> strategies = new LinkedList<Strategy>();
    //
    // while (resultIter.streamOpen()) {
    // TupleBindings bindings = resultIter.getNext();
    //
    // logger.debug("BINDINGS START");
    // logger.debug("@@@" + bindings);
    // logger.debug("BINDINGS END");
    //
    // Map<String, Participant> participants = new HashMap<String, Participant>();
    // Set<Class<? extends RDFModel>> requiredModelClasses = new HashSet<Class<? extends RDFModel>>();
    //
    // for (String role : Arrays.asList(Strategy.LINGUISTIC_ACCESS1_ROLE,
    // Strategy.LINGUISTIC_ACCESS2_ROLE, Strategy.CONCEPTUAL_ACCESS1_ROLE,
    // Strategy.CONCEPTUAL_ACCESS2_ROLE, Strategy.MEDIATOR_ROLE)) {
    // ParticipantComponent participant = buildParticipantFromBindings(bindings, role);
    //
    // Component component = participant.getComponent();
    //
    // if (component instanceof ModelAwareComponent) {
    // requiredModelClasses.add(((ModelAwareComponent) component).getModelClass());
    // }
    //
    // if (participant != null) {
    // participants.put(role, participant);
    // }
    // }
    //
    // requiredModelClasses.add(RDFModel.class);
    //
    // Set<Class<? extends RDFModel>> reducedModelClasses = Utilities
    // .reduceModelClasses(requiredModelClasses);
    //
    // if (reducedModelClasses.size() != 1) {
    // System.err.println("Cannot reduce model classes: " + reducedModelClasses + "; Skip strategy");
    // continue;
    // }
    //
    // List<Map<String, Participant>> participatingLinguisticResources =
    // resolveDependenciesOnLinguisticResources(
    // bindings, mediationProblem);
    //
    // if (participatingLinguisticResources != null) { // only if there are participating resources
    //
    // for (Map<String, Participant> aParticipantAssignment : participatingLinguisticResources) {
    // Map<String, Participant> augmentedParticipants = new HashMap<String, Participant>(
    // participants);
    // augmentedParticipants.putAll(aParticipantAssignment);
    //
    // strategies.add(new StrategyImpl(mediationProblem, augmentedParticipants,
    // reducedModelClasses.iterator().next()));
    // }
    //
    // } else {
    // strategies.add(new StrategyImpl(mediationProblem, participants,
    // reducedModelClasses.iterator().next()));
    // }
    //
    // }
    // resultIter.close();
    //
    // return strategies;
    // }

    // private List<Map<String, Participant>> resolveDependenciesOnLinguisticResources(TupleBindings bindings,
    // MediationProblem mediationProblem) throws IOException, UnsupportedQueryLanguageException,
    // ModelAccessException, MalformedQueryException, QueryEvaluationException {
    // ARTURIResource mediatorUriResource = bindings.getBoundValue(Strategy.MEDIATOR_ROLE).asURIResource();
    //
    // String remoteSparqlEndpointsFragment = SparqlUtilities.tableToBindings(new String[] { "endpoint" },
    // componentRepository.getRemoteSparqlEndpoints());
    //
    // String queryTemplate = Utilities.readResoruceToString(this,
    // "retrieveMediatorLinguisticDependencies.sparql");
    // String querySpec = queryTemplate.replace("#remoteSparqlEndpoints#", remoteSparqlEndpointsFragment)
    // .replace("#mediator#", RDFNodeSerializer.toNT(mediatorUriResource));
    //
    // logger.debug(querySpec);
    //
    // TupleQuery query = componentRepository.getConnection().createTupleQuery(querySpec);
    // Collection<TupleBindings> bindingsForMediator = RDFIterators
    // .getCollectionFromIterator(query.evaluate(false));
    //
    // if (bindingsForMediator == null) {
    // return null; // the mediator has no dependencies
    // }
    //
    // bindingsForMediator = filterByEndpoint(bindingsForMediator);
    //
    // Map<String, StringBuilder> role2pattern = new HashMap<String, StringBuilder>();
    // StringBuilder valuesFragment = new StringBuilder();
    //
    // Set<String> variables = new HashSet<String>();
    //
    // for (TupleBindings aBindingForDep : bindingsForMediator) {
    // String role = aBindingForDep.getBoundValue("lrReqRole").asLiteral().getLabel();
    //
    // variables.add("?" + role);
    //
    // StringBuilder sb = role2pattern.get(role);
    // if (sb == null) {
    // sb = new StringBuilder();
    // role2pattern.put(role, sb);
    // sb.append("?" + role + " " + RDFNodeSerializer.toNT(VoID.Res.SPARQLENDPOINT) + " " + "?"
    // + role + "_sparqlEndpoint .\n");
    // variables.add("?" + role + "_sparqlEndpoint");
    // }
    //
    // ARTURIResource pred = aBindingForDep.getBoundValue("pred").asURIResource();
    // ARTNode obj = aBindingForDep.getBoundValue("obj");
    //
    // if (obj.isURIResource() && obj.asURIResource().getNamespace()
    // .equals("http://art.uniroma2.it/ontologies/maple/placeholder#")) {
    // ARTURIResource objUri = obj.asURIResource();
    // sb.append("?" + role + " " + RDFNodeSerializer.toNT(pred) + " " + "?" + objUri.getLocalName()
    // + " .\n");
    // variables.add("?" + objUri.getLocalName());
    // } else {
    // sb.append("?" + role + " " + RDFNodeSerializer.toNT(pred) + " " + RDFNodeSerializer.toNT(obj)
    // + " .\n");
    // }
    // }
    //
    // if (mediationProblem instanceof MonolingualProblem) {
    // MonolingualProblem monolingualProblem = (MonolingualProblem) mediationProblem;
    // valuesFragment
    // .append(String.format("values(?language){(%s)}", RDFNodeSerializer.toNT(
    // VocabUtilities.nodeFactory.createLiteral(monolingualProblem.getLanguage()))))
    // .append("\n");
    // } else if (mediationProblem instanceof CrossLingualProblem) {
    // CrossLingualProblem crossLingualProblem = (CrossLingualProblem) mediationProblem;
    // valuesFragment
    // .append(String
    // .format("values(?firstLanguage){(%s)}",
    // RDFNodeSerializer.toNT(VocabUtilities.nodeFactory
    // .createLiteral(crossLingualProblem.getFirstLanguage()))))
    // .append("\n");
    // valuesFragment
    // .append(String
    // .format("values(?secondLanguage){(%s)}",
    // RDFNodeSerializer.toNT(VocabUtilities.nodeFactory
    // .createLiteral(crossLingualProblem.getSecondLanguage()))))
    // .append("\n");
    //
    // } else if (mediationProblem instanceof MultilingualProblem) {
    // MultilingualProblem multiLingualProblem = (MultilingualProblem) mediationProblem;
    // valuesFragment.append("values(?language){");
    //
    // for (String lang : multiLingualProblem.getLanguages()) {
    // valuesFragment.append(String.format("(%s) ",
    // RDFNodeSerializer.toNT(VocabUtilities.nodeFactory.createLiteral(lang))));
    // }
    // valuesFragment.append("}\n");
    // }
    //
    // queryTemplate = Utilities.readResoruceToString(this, "retriveSuitableLinguisticResources.sparql");
    // querySpec = queryTemplate.replace("#variables#", Joiner.on(" ").join(variables))
    // .replace("#values#", valuesFragment.toString())
    // .replace("#remoteSparqlEndpoints#", remoteSparqlEndpointsFragment)
    // .replace("#patterns#", Joiner.on("\n").join(role2pattern.values()));
    //
    // logger.debug(querySpec);
    //
    // query = componentRepository.createTupleQuery(querySpec);
    // TupleBindingsIterator bindings4ResourceIt = query.evaluate(false);
    //
    // List<Map<String, Participant>> alternativeResources = new ArrayList<Map<String, Participant>>();
    //
    // try {
    // while (bindings4ResourceIt.streamOpen()) {
    // TupleBindings alternative = bindings4ResourceIt.getNext();
    //
    // Map<String, ARTNode> variableValues = new HashMap<String, ARTNode>();
    //
    // for (String variableName : variables) {
    // String cleanName = variableName.substring(1);
    // ARTNode varValue = alternative.getBoundValue(cleanName);
    // variableValues.put(cleanName, varValue);
    // }
    //
    // Map<String, Participant> anAlternativeStrategy = new HashMap<String, Participant>();
    //
    // for (String role : role2pattern.keySet()) {
    // ARTURIResource sparqlEndpoint = alternative.getBoundValue(role + "_sparqlEndpoint")
    // .asURIResource();
    // anAlternativeStrategy.put(role,
    // new ParticipantLinguisticResourceImpl(role, sparqlEndpoint, variableValues));
    // }
    //
    // alternativeResources.add(anAlternativeStrategy);
    // }
    // } finally {
    // bindings4ResourceIt.close();
    // }
    //
    // return alternativeResources;
    // }

    // private Collection<TupleBindings> filterByEndpoint(Collection<TupleBindings> collectionOfBindings) {
    // Collection<TupleBindings> filteredCollection = new ArrayList<TupleBindings>();
    //
    // ARTResource lastEndpoint = null;
    // boolean atLeastOne = false;
    //
    // for (TupleBindings tb : collectionOfBindings) {
    // if (atLeastOne) {
    // if (tb.hasBinding("endpoint")) {
    // if (!Objects.equal(tb.getBoundValue("endpoint"), lastEndpoint)) {
    // break;
    // }
    // } else {
    // if (lastEndpoint != null) {
    // break;
    // }
    // }
    // }
    //
    // if (tb.hasBinding("endpoint")) {
    // lastEndpoint = tb.getBoundValue("endpoint").asResource();
    // }
    //
    // filteredCollection.add(tb);
    //
    // atLeastOne = true;
    // }
    //
    // return filteredCollection;
    //
    // }

    private static class LexicalizationModelComparator implements Comparator<IRI> {

        public static final LexicalizationModelComparator instance = new LexicalizationModelComparator();

        private List<IRI> models = Arrays.asList(
                SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2000/01/rdf-schema"),
                SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core"),
                SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2008/05/skos-xl"),
                SimpleValueFactory.getInstance().createIRI("http://www.w3.org/ns/lemon/ontolex"));

        @Override
        public int compare(IRI o1, IRI o2) {
            int index1 = models.indexOf(o1);
            int index2 = models.indexOf(o2);

            if (index1 == -1 && index2 == -1) {
                return o1.stringValue().compareTo(o2.stringValue());
            } else {
                return Integer.compare(index1, index2);
            }
        }

    }

    // @Override
    // public void deployStrategy(Strategy strategy) {
    // Collection<Participant> participants = strategy.getParticipants().values();
    //
    // Set<BundleCoordinates> requiredCoordinates = new HashSet<BundleCoordinates>();
    //
    // for (Participant p : participants) {
    // if (p instanceof ParticipantComponent) {
    // ParticipantComponent pc = (ParticipantComponent) p;
    // requiredCoordinates.add(pc.getComponent().getManifestation().getBundleCoordinates());
    // }
    // }
    //
    // componentRepository.deployBundles(requiredCoordinates);
    // }

    // @Override
    // public StrategyInstance instantiateStrategy(Strategy strategy, Repository dataset1, Repository
    // dataset2)
    // throws ClassNotFoundException, InstantiationException, IllegalAccessException,
    // NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException,
    // RDF4JException {
    // Map<String, Participant> participants = strategy.getParticipants();
    //
    // Collection<BundleCoordinates> coordinates = new LinkedList<BundleCoordinates>();
    // for (Participant participant : participants.values()) {
    // if (participant instanceof ParticipantComponent) {
    // ParticipantComponent participantComponent = (ParticipantComponent) participant;
    // coordinates
    // .add(participantComponent.getComponent().getManifestation().getBundleCoordinates());
    // }
    // }
    //
    // Map<BundleCoordinates, Bundle> coordinates2bundleMap = componentRepository
    // .retrieveBundles(coordinates);
    //
    // Map<String, Object> role2objMap = new HashMap<String, Object>();
    //
    // Map<String, Map<String, Value>> lrVariables = new HashMap<String, Map<String, Value>>();
    //
    // for (Entry<String, Participant> entry : strategy.getParticipants().entrySet()) {
    // String role = entry.getKey();
    // Participant participant = entry.getValue();
    // if (participant instanceof ParticipantComponent) {
    // ParticipantComponent participantComponent = (ParticipantComponent) participant;
    // Component component = participantComponent.getComponent();
    //
    // Manifestation manifestation = component.getManifestation();
    // Bundle bundle = coordinates2bundleMap.get(manifestation.getBundleCoordinates());
    // System.out.println("Bundle: " + bundle + "\nClass name: " + manifestation.getJavaClassName());
    // System.out.flush();
    // Class<?> clazz = bundle.loadClass(manifestation.getJavaClassName());
    // Object instance = clazz.newInstance();
    //
    // if (role.equals(Strategy.LINGUISTIC_ACCESS1_ROLE)
    // || role.equals(Strategy.CONCEPTUAL_ACCESS1_ROLE)) {
    // Class<? extends RDFModel> requiredModelClass = ((ModelAwareComponent) component)
    // .getModelClass();
    // Method m = instance.getClass().getMethod("setModel", requiredModelClass);
    // m.invoke(instance, Utilities.convertModelTo(dataset1, requiredModelClass));
    // }
    //
    // if (role.equals(Strategy.LINGUISTIC_ACCESS2_ROLE)
    // || role.equals(Strategy.CONCEPTUAL_ACCESS2_ROLE)) {
    // Class<? extends RDFModel> requiredModelClass = ((ModelAwareComponent) component)
    // .getModelClass();
    // Method m = instance.getClass().getMethod("setModel", requiredModelClass);
    // m.invoke(instance, Utilities.convertModelTo(dataset2, requiredModelClass));
    // }
    //
    // role2objMap.put(role, instance);
    // } else if (participant instanceof ParticipantLinguisticResource) {
    // ParticipantLinguisticResource participantLinguisticResource = (ParticipantLinguisticResource)
    // participant;
    //
    // TripleQueryModelHTTPConnection endpointConnection = getModelFactory()
    // .loadTripleQueryHTTPConnection(
    // participantLinguisticResource.getSPARQLEndpoint().getURI());
    // role2objMap.put(role, endpointConnection);
    // lrVariables.put(role, participantLinguisticResource.getVariable2Value());
    // }
    // }
    //
    // Object mediatorObj = role2objMap.get(Strategy.MEDIATOR_ROLE);
    //
    // if (mediatorObj instanceof LinguisticAccessAware) {
    // Object linguisticAccess1 = role2objMap.get(Strategy.LINGUISTIC_ACCESS1_ROLE);
    // Object linguisticAccess2 = role2objMap.get(Strategy.LINGUISTIC_ACCESS2_ROLE);
    // ((LinguisticAccessAware) mediatorObj).setLinguisticAccess1(linguisticAccess1);
    // ((LinguisticAccessAware) mediatorObj).setLinguisticAccess2(linguisticAccess2);
    // }
    //
    // if (mediatorObj instanceof ConceptualAccessAware) {
    // Object conceptualAccess1 = role2objMap.get(Strategy.CONCEPTUAL_ACCESS1_ROLE);
    // Object conceptualAccess2 = role2objMap.get(Strategy.CONCEPTUAL_ACCESS2_ROLE);
    //
    // ((ConceptualAccessAware) mediatorObj).setConceptualAccess1(conceptualAccess1);
    // ((ConceptualAccessAware) mediatorObj).setConceptualAccess2(conceptualAccess2);
    // }
    //
    // if (mediatorObj instanceof MediationProblemAware) {
    // ((MediationProblemAware) mediatorObj).setMediationProblem(strategy.getMediationProblem());
    // }
    //
    // if (mediatorObj instanceof LinguisticResourceAware) {
    // for (String lr : lrVariables.keySet()) {
    // ((LinguisticResourceAware) mediatorObj).setLinguisticResource(lr, lrVariables.get(lr),
    // (TripleQueryModelHTTPConnection) role2objMap.get(lr));
    // }
    // }
    //
    // return new StrategyInstanceImpl(strategy, dataset1, dataset2, role2objMap);
    // }

}
