package it.uniroma2.art.maple.orchestration.model;

import java.util.Collection;

/**
 * A collaboration consisting of a number of participants.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Collaboration {
	String MAIN_ROLE = "main";
	String CONCEPTUAL_ACCESS_ROLE = "conceptualAccess";
	String CONCEPTUAL_ACCESS_QUERY_PROVIDER_ROLE = "conceptualAccessQueryProvider";
	String LINGUISTIC_ACCESS_ROLE = "linguisticAccess";
	String LINGUISTIC_ACCESS_QUERY_PROVIDER_ROLE = "linguisticAccessQueryProvider";

	Collection<Participant> getParticipants();
}
