package it.uniroma2.art.maple.orchestration.model;

import java.util.Map;

/**
 * An instance of a {@link Collaboration}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface CollaborationInstance {
	
	Collaboration getCollaboration();
	Map<String, Object> getRoleObjectMapping();

}
