package it.uniroma2.art.maple.orchestration.model;

import org.eclipse.rdf4j.model.IRI;

/**
 * A MAPLE component. A component is identified trough a URI (see {@link #getName()}, and is provided by a
 * dynamically pluggable artifact (see {@link #getManifestation()}. Components are intended to be instantiated
 * by {@link ParticipantComponent}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Component {
	/**
	 * Returns this component name.
	 * 
	 * @return
	 */
	IRI getName();

	/**
	 * Returns this component manifestation.
	 * 
	 * @return
	 */
	Manifestation getManifestation();
}
