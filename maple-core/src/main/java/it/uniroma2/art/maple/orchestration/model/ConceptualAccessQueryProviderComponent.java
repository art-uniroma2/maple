package it.uniroma2.art.maple.orchestration.model;

/**
 * A query provider for conceptual access over a dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface ConceptualAccessQueryProviderComponent extends QueryProviderComponent, ModelAwareComponent {
}
