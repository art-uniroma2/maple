package it.uniroma2.art.maple.orchestration.model;

/**
 * A linguistic access over a dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface LinguisticAccessComponent extends Component, ModelAwareComponent {
}
