package it.uniroma2.art.maple.orchestration.model;

import java.util.Optional;

/**
 * A manifestation of a MAPLE component.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Manifestation {
	/**
	 * Returns the coordinates of the plugin hosting this component.
	 */
	PluginCoordinates getPluginCoordinates();

	/**
	 * Returns the Java class name (whithin the hosting bundle classpath) that realizes this component.
	 * 
	 * @return
	 */
	String getJavaClassName();
}
