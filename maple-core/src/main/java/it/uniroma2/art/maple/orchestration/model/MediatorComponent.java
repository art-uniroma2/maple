package it.uniroma2.art.maple.orchestration.model;

/**
 * A component for the mediation of datasets.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface MediatorComponent extends Component, ModelAwareComponent {
}
