package it.uniroma2.art.maple.orchestration.model;

import org.eclipse.rdf4j.model.IRI;

/**
 * A component relying on the underlying dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface ModelAwareComponent {
	/**
	 * Returns the model required by this component.
	 * 
	 * @return
	 */
	IRI getModel();
}
