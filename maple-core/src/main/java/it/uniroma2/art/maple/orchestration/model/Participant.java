package it.uniroma2.art.maple.orchestration.model;

/**
 * A participant in a mediation strategy.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Participant {
	/**
	 * Returns role of this participant.
	 * 
	 * @return
	 */
	String getRole();
}
