package it.uniroma2.art.maple.orchestration.model;

/**
 * A component participating in a mediation strategy.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface ParticipantComponent extends Participant {

	/**
	 * Returns the component instantiated by this participant.
	 * 
	 * @return
	 */
	Component getComponent();
}
