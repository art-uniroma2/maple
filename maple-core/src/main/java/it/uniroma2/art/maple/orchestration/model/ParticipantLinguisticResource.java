package it.uniroma2.art.maple.orchestration.model;

import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;

/**
 * A linguistic resource participating in a mediation strategy.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface ParticipantLinguisticResource extends Participant {

	IRI getSPARQLEndpoint();

	Map<String, Value> getVariable2Value();

}
