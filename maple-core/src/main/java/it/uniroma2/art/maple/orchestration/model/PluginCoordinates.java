package it.uniroma2.art.maple.orchestration.model;

/**
 * Coordinates of Plugin.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface PluginCoordinates {
	/**
	 * Returns this plugin identifier.
	 * 
	 * @return
	 */
	String getPluginId();

	/**
	 * Returns this plugin version.
	 * 
	 * @return
	 */
	String getVersion();

//	/**
//	 * Returns the URL of the OBR repository hosting the bundle, or <code>null</code> if it is assumed to be
//	 * locally available
//	 *
//	 * @return
//	 */
//	String getHostingOBR();
}
