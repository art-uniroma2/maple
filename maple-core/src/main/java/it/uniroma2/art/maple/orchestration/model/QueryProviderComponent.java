package it.uniroma2.art.maple.orchestration.model;

/**
 * A component for abstracting the query providers over a dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface QueryProviderComponent extends Component, ModelAwareComponent {
}
