package it.uniroma2.art.maple.orchestration.model;

import java.util.Map;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.maple.scenario.AlignmentScenario;

/**
 * A resolution strategy for a mediation problem.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface Strategy {
	/**
	 * This role represents the linguistic access over the first dataset.
	 */
	String LINGUISTIC_ACCESS1_ROLE = "linguisticAccess1";

	/**
	 * This role represents the linguistic access over the second dataset.
	 */
	String LINGUISTIC_ACCESS2_ROLE = "linguisticAccess2";

	/**
	 * This role represents the conceptual access over the first dataset.
	 */
	String CONCEPTUAL_ACCESS1_ROLE = "conceptualAccess1";

	/**
	 * This role represents the conceptual access over the second dataset.
	 */
	String CONCEPTUAL_ACCESS2_ROLE = "conceptualAccess2";

	/**
	 * This role represents the mediator between the input datasets.
	 */
	String MEDIATOR_ROLE = "mediator";

	/**
	 * Returns the mediation problem this strategy refers to.
	 * 
	 * @return
	 */
	AlignmentScenario getMediationProblem();

	/**
	 * Returns the participant having the specified role.
	 * 
	 * @param role
	 * @return
	 */
	Participant getParticipant(String role);

	/**
	 * Returns all the participants involved in the strategy.
	 * 
	 * @return
	 */
	Map<String, Participant> getParticipants();

	/**
	 * Returns the <code>model</code> required for loading the input datasets.
	 * 
	 * @return
	 */
	IRI getRequiredModel();
}
