package it.uniroma2.art.maple.orchestration.model;

import java.util.Map;

import org.eclipse.rdf4j.repository.Repository;

import it.uniroma2.art.maple.components.alignment.Alignment;

/**
 * An instance of a strategy.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface StrategyInstance {
	/**
	 * Executes the strategy instance.
	 */
	Alignment execute();

	/**
	 * Returns the underlying strategy.
	 * 
	 * @return
	 */
	Strategy getStrategy();

	/**
	 * Returns the first dataset.
	 * 
	 * @return
	 */
	Repository getDataset1();

	/**
	 * Returns the second dataset.
	 * 
	 * @return
	 */
	Repository getDataset2();

	/**
	 * Returns the map from roles to objects realizing them.
	 * 
	 * @return
	 */
	Map<String, Object> getRole2objMap();
}
