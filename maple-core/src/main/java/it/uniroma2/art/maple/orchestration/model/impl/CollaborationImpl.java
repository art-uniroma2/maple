package it.uniroma2.art.maple.orchestration.model.impl;

import java.util.Collection;
import java.util.Collections;

import it.uniroma2.art.maple.orchestration.model.Collaboration;
import it.uniroma2.art.maple.orchestration.model.Participant;

/**
 * An implementation of {@link Collaboration}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class CollaborationImpl implements Collaboration {

	private final Collection<Participant> participants;

	public CollaborationImpl(Collection<Participant> participants) {
		this.participants = Collections.unmodifiableCollection(participants);
	}

	@Override
	public Collection<Participant> getParticipants() {
		return participants;
	}

}
