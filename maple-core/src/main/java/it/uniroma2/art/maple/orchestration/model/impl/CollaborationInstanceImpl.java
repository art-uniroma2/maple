package it.uniroma2.art.maple.orchestration.model.impl;

import java.util.Map;

import it.uniroma2.art.maple.orchestration.model.Collaboration;
import it.uniroma2.art.maple.orchestration.model.CollaborationInstance;

/**
 * An implementation of {@link CollaborationInstance}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class CollaborationInstanceImpl implements CollaborationInstance {

	private Collaboration collaboration;
	private Map<String, Object> roleObjectMapping;

	public CollaborationInstanceImpl(Collaboration collaboration, Map<String, Object> roleObjectMapping) {
		this.collaboration = collaboration;
		this.roleObjectMapping = roleObjectMapping;
	}

	@Override
	public Collaboration getCollaboration() {
		return collaboration;
	}

	@Override
	public Map<String, Object> getRoleObjectMapping() {
		return roleObjectMapping;
	}

}
