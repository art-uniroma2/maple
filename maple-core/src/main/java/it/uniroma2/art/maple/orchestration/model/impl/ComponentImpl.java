package it.uniroma2.art.maple.orchestration.model.impl;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.maple.orchestration.model.Component;
import it.uniroma2.art.maple.orchestration.model.Manifestation;

/**
 * An implementation of {@link Component}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class ComponentImpl implements Component {

	private IRI name;
	private Manifestation manifestation;

	/**
	 * Constructs a component with the given name and manifestation.
	 * 
	 * @param name
	 * @param manifestation
	 */
	public ComponentImpl(IRI name, Manifestation manifestation) {
		this.name = name;
		this.manifestation = manifestation;
	}

	@Override
	public IRI getName() {
		return name;
	}

	public Manifestation getManifestation() {
		return manifestation;
	}
	
}
