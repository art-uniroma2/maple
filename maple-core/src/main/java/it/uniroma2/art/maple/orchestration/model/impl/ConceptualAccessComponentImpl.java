package it.uniroma2.art.maple.orchestration.model.impl;

import org.eclipse.rdf4j.model.IRI;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.ConceptualAccessComponent;
import it.uniroma2.art.maple.orchestration.model.Manifestation;

/**
 * An implementation of {@link ConceptualAccessComponent}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class ConceptualAccessComponentImpl extends ComponentImpl implements ConceptualAccessComponent {

	private IRI model;

	/**
	 * Constructs a conceptual access component with the given name and manifestation.
	 * 
	 * @param name
	 * @param manifestation
	 * @param modelClass
	 */
	public ConceptualAccessComponentImpl(IRI name, Manifestation manifestation, IRI model) {
		super(name, manifestation);
		this.model = model;
	}

	@Override
	public IRI getModel() {
		return model;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(ConceptualAccessComponent.class).add("name", getName())
				.add("model", getModel()).add("manifestation", getManifestation()).toString();
	}
}
