package it.uniroma2.art.maple.orchestration.model.impl;

import org.eclipse.rdf4j.model.IRI;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.ConceptualAccessComponent;
import it.uniroma2.art.maple.orchestration.model.ConceptualAccessQueryProviderComponent;
import it.uniroma2.art.maple.orchestration.model.Manifestation;

/**
 * An implementation of {@link ConceptualAccessQueryProviderComponent}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class ConceptualAccessQueryProviderComponentImpl extends ComponentImpl
		implements ConceptualAccessQueryProviderComponent {

	private IRI model;

	/**
	 * Constructs a query provider for conceptual access with the given name and manifestation.
	 * 
	 * @param name
	 * @param manifestation
	 * @param modelClass
	 */
	public ConceptualAccessQueryProviderComponentImpl(IRI name, Manifestation manifestation, IRI model) {
		super(name, manifestation);
		this.model = model;
	}

	@Override
	public IRI getModel() {
		return model;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(ConceptualAccessComponent.class).add("name", getName())
				.add("model", getModel()).add("manifestation", getManifestation()).toString();
	}
}
