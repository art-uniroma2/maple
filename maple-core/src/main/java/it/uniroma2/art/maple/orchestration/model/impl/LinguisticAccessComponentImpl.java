package it.uniroma2.art.maple.orchestration.model.impl;

import org.eclipse.rdf4j.model.IRI;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.LinguisticAccessComponent;
import it.uniroma2.art.maple.orchestration.model.Manifestation;

/**
 * An implementation of {@link LinguisticAccessComponent}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class LinguisticAccessComponentImpl extends ComponentImpl implements LinguisticAccessComponent {

	private IRI model;

	public LinguisticAccessComponentImpl(IRI name, Manifestation manifestation, IRI model) {
		super(name, manifestation);
		this.model = model;
	}

	@Override
	public IRI getModel() {
		return model;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(LinguisticAccessComponent.class).add("name", getName())
				.add("model", getModel()).add("manifestation", getManifestation()).toString();
	}
}
