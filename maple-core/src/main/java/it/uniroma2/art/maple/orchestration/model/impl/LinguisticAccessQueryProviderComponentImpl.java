package it.uniroma2.art.maple.orchestration.model.impl;

import org.eclipse.rdf4j.model.IRI;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.LinguisticAccessQueryProviderComponent;
import it.uniroma2.art.maple.orchestration.model.Manifestation;

/**
 * An implementation of {@link LinguisticAccessQueryProviderComponent}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class LinguisticAccessQueryProviderComponentImpl extends ComponentImpl
		implements LinguisticAccessQueryProviderComponent {

	private IRI model;

	/**
	 * Constructs a query provider for linguistic access with the given name and manifestation.
	 * 
	 * @param name
	 * @param manifestation
	 * @param modelClass
	 */
	public LinguisticAccessQueryProviderComponentImpl(IRI name, Manifestation manifestation, IRI model) {
		super(name, manifestation);
		this.model = model;
	}

	@Override
	public IRI getModel() {
		return model;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(LinguisticAccessQueryProviderComponent.class).add("name", getName())
				.add("model", getModel()).add("manifestation", getManifestation()).toString();
	}
}
