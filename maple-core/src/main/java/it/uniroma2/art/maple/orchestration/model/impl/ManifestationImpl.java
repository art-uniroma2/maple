package it.uniroma2.art.maple.orchestration.model.impl;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.PluginCoordinates;
import it.uniroma2.art.maple.orchestration.model.Manifestation;

/**
 * An implementation of {@link Manifestation}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class ManifestationImpl implements Manifestation {

	private PluginCoordinates pluginCoordinates;
	private String javaClassName;

	/**
	 * Constructs a manifestation object for the given plugin coordinates and Java class name.
	 *
	 * @param javaClassName
	 * @param pluginCoordinates
	 */
	public ManifestationImpl(String javaClassName, PluginCoordinates pluginCoordinates) {
		this.javaClassName = javaClassName;
		this.pluginCoordinates = pluginCoordinates;
	}

	@Override
	public PluginCoordinates getPluginCoordinates() {
		return pluginCoordinates;
	}

	@Override
	public String getJavaClassName() {
		return javaClassName;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(Manifestation.class).add("javaClassName", getJavaClassName())
				.add("pluginCoordinates", getPluginCoordinates()).toString();
	}
}
