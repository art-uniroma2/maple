package it.uniroma2.art.maple.orchestration.model.impl;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.maple.orchestration.model.Manifestation;
import it.uniroma2.art.maple.orchestration.model.MediatorComponent;

public class MediatorComponentImpl extends ComponentImpl implements MediatorComponent {
	private IRI model;

	public MediatorComponentImpl(IRI name, Manifestation manifestation, IRI model) {
		super(name, manifestation);
		this.model = model;
	}

	@Override
	public IRI getModel() {
		return model;
	}
}
