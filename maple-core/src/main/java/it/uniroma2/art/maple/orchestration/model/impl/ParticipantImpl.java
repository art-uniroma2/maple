package it.uniroma2.art.maple.orchestration.model.impl;

import it.uniroma2.art.maple.orchestration.model.Participant;

public class ParticipantImpl implements Participant {

	private String role;
	
	public ParticipantImpl(String role) {
		this.role = role;
	}
	
	@Override
	public String getRole() {
		return role;
	}

}
