package it.uniroma2.art.maple.orchestration.model.impl;

import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.maple.orchestration.model.ParticipantLinguisticResource;

public class ParticipantLinguisticResourceImpl extends ParticipantImpl
		implements ParticipantLinguisticResource {

	private IRI sparqlEndpoint;
	private Map<String, Value> variable2value;

	public ParticipantLinguisticResourceImpl(String role, IRI sparqlEndpoint,
			Map<String, Value> variable2value) {
		super(role);
		this.sparqlEndpoint = sparqlEndpoint;
		this.variable2value = variable2value;
	}

	@Override
	public IRI getSPARQLEndpoint() {
		return sparqlEndpoint;
	}

	@Override
	public Map<String, Value> getVariable2Value() {
		return variable2value;
	}

}
