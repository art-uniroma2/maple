package it.uniroma2.art.maple.orchestration.model.impl;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.Component;
import it.uniroma2.art.maple.orchestration.model.ParticipantComponent;

/**
 * An implementation of {@link ParticipantComponent}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class ParticipantingComponentImpl extends ParticipantImpl implements ParticipantComponent {

	private Component component;

	/**
	 * Constructs a participant for the given role, using the given component.
	 * 
	 * @param role
	 * @param component
	 */
	public ParticipantingComponentImpl(String role, Component component) {
		super(role);
		this.component = component;
	}

	@Override
	public Component getComponent() {
		return component;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(ParticipantComponent.class).add("role", getRole())
				.add("component", getComponent()).toString();
	}
}
