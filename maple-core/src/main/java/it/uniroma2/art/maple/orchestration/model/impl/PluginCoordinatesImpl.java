package it.uniroma2.art.maple.orchestration.model.impl;

import com.google.common.base.MoreObjects;

import it.uniroma2.art.maple.orchestration.model.PluginCoordinates;

import java.util.Objects;

/**
 * An implementation of {@link PluginCoordinates}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class PluginCoordinatesImpl implements PluginCoordinates {
	private String pluginId;
	private String version;
	public PluginCoordinatesImpl(String pluginId, String version) {
		this.pluginId = pluginId;
		this.version = version;
	}

	@Override
	public String getPluginId() {
		return pluginId;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		return Objects.hash(pluginId, version);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;

		if (getClass() != obj.getClass())
			return false;

		PluginCoordinatesImpl otherCoord = (PluginCoordinatesImpl) obj;
		return pluginId.equals(otherCoord.pluginId) && version.equals(otherCoord.version);
	}

//	@Override
//	public String getHostingOBR() {
//		return hostingOBR;
//	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(PluginCoordinates.class).add("pluginId", pluginId)
				.add("version", version).toString();
	}
}
