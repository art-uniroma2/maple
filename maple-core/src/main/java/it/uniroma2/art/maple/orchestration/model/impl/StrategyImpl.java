package it.uniroma2.art.maple.orchestration.model.impl;

import java.util.Map;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.maple.orchestration.model.Participant;
import it.uniroma2.art.maple.orchestration.model.Strategy;
import it.uniroma2.art.maple.scenario.AlignmentScenario;

/**
 * An implementation of {@link Strategy}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class StrategyImpl implements Strategy {

	private AlignmentScenario mediationProblem;
	private Map<String, Participant> participants;
	private IRI requiredModel;

	/**
	 * Constructs a strategy for the given participants.
	 * 
	 * @param mediationProblem
	 * @param participants
	 * @param requiredModel
	 */
	public StrategyImpl(AlignmentScenario mediationProblem, Map<String, Participant> participants,
			IRI requiredModel) {
		this.mediationProblem = mediationProblem;
		this.participants = participants;
		this.requiredModel = requiredModel;
	}

	@Override
	public String toString() {
		return participants.toString();
	}

	@Override
	public AlignmentScenario getMediationProblem() {
		return mediationProblem;
	}

	@Override
	public Map<String, Participant> getParticipants() {
		return participants;
	}

	@Override
	public Participant getParticipant(String role) {
		return participants.get(role);
	}

	@Override
	public IRI getRequiredModel() {
		return requiredModel;
	}

}
