package it.uniroma2.art.maple.orchestration.model.impl;

import java.net.URI;
import java.util.Map;

import org.eclipse.rdf4j.repository.Repository;

import it.uniroma2.art.maple.components.alignment.Alignment;
import it.uniroma2.art.maple.components.mediators.Mediator;
import it.uniroma2.art.maple.orchestration.model.Strategy;
import it.uniroma2.art.maple.orchestration.model.StrategyInstance;

/**
 * An implementation of {@link StrategyInstance}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class StrategyInstanceImpl implements StrategyInstance {

	private Strategy strategy;
	private Repository dataset1;
	private String dataset1BaseURI;
	private Repository dataset2;
	private String dataset2BaseURI;
	private Map<String, Object> role2objMap;

	/**
	 * Constructs an instance of the given strategy, grounding it over the provided datasets.
	 * 
	 * @param strategy
	 * @param dataset1
	 * @param dataset2
	 * @param role2ObjMap
	 */
	public StrategyInstanceImpl(Strategy strategy, Repository dataset1, String dataset1BaseURI,
			Repository dataset2, String dataset2BaseURI, Map<String, Object> role2objMap) {
		this.strategy = strategy;
		this.dataset1 = dataset1;
		this.dataset1BaseURI = dataset1BaseURI;
		this.dataset2 = dataset2;
		this.dataset2BaseURI = dataset2BaseURI;
		this.role2objMap = role2objMap;
	}

	@Override
	public Strategy getStrategy() {
		return strategy;
	}

	@Override
	public Repository getDataset1() {
		return dataset1;
	}

	@Override
	public Repository getDataset2() {
		return dataset2;
	}

	@Override
	public Map<String, Object> getRole2objMap() {
		return role2objMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.maple.orchestration.model.StrategyInstance#execute()
	 */
	@Override
	public Alignment execute() {
		Mediator mediatorObj = (Mediator) role2objMap.get(Strategy.MEDIATOR_ROLE);
		return ((Mediator) mediatorObj).align(URI.create(dataset1BaseURI), URI.create(dataset2BaseURI));
	}
}
