package it.uniroma2.art.maple.provisioning;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterators;
import com.google.common.io.Files;
import it.uniroma2.art.maple.components.access.FlatAccess;
import it.uniroma2.art.maple.components.access.HierarchicalAccess;
import it.uniroma2.art.maple.components.access.LabelAccess;
import it.uniroma2.art.maple.components.access.TerminologyAccess;
import it.uniroma2.art.maple.orchestration.MediationFramework;
import it.uniroma2.art.maple.orchestration.model.PluginCoordinates;
import org.apache.commons.lang3.NotImplementedException;
import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.pf4j.PluginManager;
import org.pf4j.PluginState;
import org.pf4j.PluginStateEvent;
import org.pf4j.PluginStateListener;
import org.pf4j.PluginWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ComponentRepository implements PluginStateListener {
	private static final Logger logger = LoggerFactory.getLogger(ComponentRepository.class);

	private SailRepository repo;
	private PluginManager pluginManager;
	private URL[] remoteSparqlEndpoints;
	private URL[] dataDumps;

	public PluginManager getPluginManager() {
		return pluginManager;
	}

	public void setPluginManager(PluginManager pluginManager) {
		this.pluginManager = pluginManager;
	}

	public void setRemoteSparqlEndpoints(URL[] endpoints) {
		this.remoteSparqlEndpoints = Iterators
				.toArray(Iterators.filter(Iterators.forArray(endpoints), Predicates.notNull()), URL.class);
		logger.debug("Remote SPARQL Endpoints: {}", Arrays.toString(this.remoteSparqlEndpoints));
	}

	public URL[] getRemoteSparqlEndpoints() {
		return remoteSparqlEndpoints;
	}

	public void setDataDumps(URL[] dataDumps) {
		this.dataDumps = Iterators
				.toArray(Iterators.filter(Iterators.forArray(dataDumps), Predicates.notNull()), URL.class);
		;
		logger.debug("Data dumps: {}", Arrays.toString(this.dataDumps));
	}

	public ComponentRepository(PluginManager pluginManager) {
		this.pluginManager = pluginManager;
	}

	public synchronized void initialize() throws RDF4JException, IOException {
		repo = new SailRepository(new MemoryStore());
		repo.init();

		try (RepositoryConnection conn = repo.getConnection()) {
			for (URL dumpUrl : dataDumps) {
				conn.add(dumpUrl, "", RDFFormat.TURTLE,
						conn.getValueFactory().createIRI(dumpUrl.toExternalForm()));
			}
		}

		List<PluginWrapper> startedPlugins = pluginManager.getStartedPlugins();
		for (PluginWrapper plugin : startedPlugins) {
			try {
				register(plugin);
			} catch (Exception e) {
				logger.error("An exception occurred when registering the plugin {}", plugin.getPluginId(), e);
			}
		}

		pluginManager.addPluginStateListener(this);
	}

	public synchronized void dispose() throws RDF4JException {
		try {
			if (repo != null) {
				Repositories.consume(repo, RepositoryConnection::clear);
				repo.shutDown();
			}
		} finally {
			if (pluginManager != null) {
				pluginManager.removePluginStateListener(this);
			}
		}
	}

	public synchronized void register(PluginWrapper aPlugin) throws Exception {
		logger.info("Registering plugin: {}", aPlugin.getPluginId());

		Enumeration<URL> entries = aPlugin.getPluginClassLoader().getResources("/META-INF/it.uniroma2.art.maple");

		while (entries.hasMoreElements()) {
			URL entryURL = entries.nextElement();
			logger.debug("Adding data located at: {}", entryURL);
			try (RepositoryConnection conn = repo.getConnection()) {
				conn.add(entryURL, "", RDFFormat.TURTLE,
						conn.getValueFactory().createIRI(entryURL.toExternalForm()));
			}
		}

	}

	public synchronized void unregister(PluginWrapper aPlugin) throws Exception {
		logger.info("Unregistering plugin: {}", aPlugin.getPluginId());
		Enumeration<URL> entries = aPlugin.getPluginClassLoader().getResources("/META-INF/it.uniroma2.art.maple");


		while (entries.hasMoreElements()) {
			URL entryURL = entries.nextElement();
			try (RepositoryConnection conn = repo.getConnection()) {
				conn.clear(conn.getValueFactory().createIRI(entryURL.toExternalForm()));
			}
		}
	}

	public Class<?> discoverFlatAccess(Collection<IRI> datasetTypes)
			throws RDF4JException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"prefix maple: <http://art.uniroma2.it/ontologies/maple/core#> prefix lime: <http://art.uniroma2.it/ontologies/lime#> select distinct ?flatAccess ?flatAccessClass ?symbolicName ?version {?flatAccess a maple:FlatAccess ; maple:manifestation [maple:javaClass ?flatAccessClass; maple:plugin [maple:pluginId ?symbolicName ; maple:version ?version]] .");

		for (IRI lm : datasetTypes) {
			sb.append("{?flatAccess maple:supportedDatasetType " + NTriplesUtil.toNTriplesString(lm)
					+ ".} union");
		}

		sb.append("{filter(false)}");

		sb.append("}");

		try (RepositoryConnection conn = repo.getConnection()) {
			TupleQuery query = conn.prepareTupleQuery(sb.toString());
			query.setIncludeInferred(true);
			BindingSet bs = QueryResults.singleResult(query.evaluate());

			if (bs != null) {
				String fullyQualifiedClassName = bs.getValue("flatAccessClass").stringValue();
				String symbolicName = bs.getValue("symbolicName").stringValue();
				String version = bs.getValue("version").stringValue();

				return loadParticipantClass(fullyQualifiedClassName, symbolicName, version);
			} else {
				return null;
			}
		}
	}

	private Class<?> loadParticipantClass(String fullyQualifiedClassName, String pluginId, String version) throws ClassNotFoundException {
		Optional<PluginWrapper> aPlugin = discoverPlugin(pluginId, version);
		if (aPlugin.isPresent()) {
			return aPlugin.get().getPluginClassLoader().loadClass(fullyQualifiedClassName);
		} else {
			return MediationFramework.class.getClassLoader().loadClass(fullyQualifiedClassName);
		}
	}

	protected Class<?> loadParticipantClass(String fullyQualifiedClassName, @Nullable PluginCoordinates coordinates) throws ClassNotFoundException {
		return loadParticipantClass(fullyQualifiedClassName, coordinates.getPluginId(), coordinates.getVersion());
	}

	public Object instantiateParticipant(String fullyQualifiedClassName, @Nullable PluginCoordinates coordinates) throws ClassNotFoundException {
		Class<?> clazz = loadParticipantClass(fullyQualifiedClassName, coordinates.getPluginId(), coordinates.getVersion());
		return pluginManager.getExtensionFactory().create(clazz);
	}


	public Class<?> discoverHierarchicalAccess(Collection<IRI> datasetTypes)
			throws RDF4JException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"prefix maple: <http://art.uniroma2.it/ontologies/maple/core#> prefix lime: <http://art.uniroma2.it/ontologies/lime#> select distinct ?hierarchicalAccess ?hierarchicalAccessClass ?symbolicName ?version {?hierarchicalAccess a maple:HierarchicalAccess ; maple:manifestation [maple:javaClass ?hierarchicalAccessClass; maple:plugin [maple:pluginId ?symbolicName ; maple:version ?version]] .");

		for (IRI lm : datasetTypes) {
			sb.append("{?hierarchicalAccess maple:supportedDatasetType " + NTriplesUtil.toNTriplesString(lm)
					+ ".} union");
		}

		sb.append("{filter(false)}");

		sb.append("}");

		try (RepositoryConnection conn = repo.getConnection()) {
			TupleQuery query = conn.prepareTupleQuery(sb.toString());
			query.setIncludeInferred(true);
			BindingSet bs = QueryResults.singleResult(query.evaluate());

			if (bs != null) {
				String fullyQualifiedClassName = bs.getValue("hierarchicalAccessClass").stringValue();
				String symbolicName = bs.getValue("symbolicName").stringValue();
				String version = bs.getValue("version").stringValue();

				return loadParticipantClass(fullyQualifiedClassName, symbolicName, version);
			} else {
				return null;
			}
		}
	}

	public Class<?> discoverLabelAccess(Collection<IRI> linguisticModels)
			throws RDF4JException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"prefix maple: <http://art.uniroma2.it/ontologies/maple/core#> prefix lime: <http://art.uniroma2.it/ontologies/lime#> select distinct ?labelAccess ?labelAccessClass ?symbolicName ?version {?labelAccess a maple:LabelAccess ; maple:manifestation [maple:javaClass ?labelAccessClass; maple:plugin [maple:pluginId ?symbolicName ; maple:version ?version]] .");

		for (IRI lm : linguisticModels) {
			sb.append("{?labelAccess maple:supportedLinguisticModel " + NTriplesUtil.toNTriplesString(lm)
					+ ".} union");
		}

		sb.append("{filter(false)}");

		sb.append("}");

		try (RepositoryConnection conn = repo.getConnection()) {
			TupleQuery query = conn.prepareTupleQuery(sb.toString());
			query.setIncludeInferred(true);
			BindingSet bs = QueryResults.singleResult(query.evaluate());

			if (bs != null) {
				String fullyQualifiedClassName = bs.getValue("labelAccessClass").stringValue();
				String symbolicName = bs.getValue("symbolicName").stringValue();
				String version = bs.getValue("version").stringValue();

				return loadParticipantClass(fullyQualifiedClassName, symbolicName, version);
			} else {
				return null;
			}
		}
	}

	public Class<?> discoverTerminologyAccess(Collection<IRI> linguisticModels)
			throws RDF4JException, ClassNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"prefix maple: <http://art.uniroma2.it/ontologies/maple/core#> prefix lime: <http://art.uniroma2.it/ontologies/lime#> select distinct ?terminologyAccess ?terminologyAccessClass ?symbolicName ?version {?terminologyAccess a maple:TerminologyAccess ; maple:manifestation [maple:javaClass ?terminologyAccessClass; maple:plugin [maple:pluginId ?symbolicName ; maple:version ?version]] .");

		for (IRI lm : linguisticModels) {
			sb.append("{?terminologyAccess maple:supportedLinguisticModel "
					+ NTriplesUtil.toNTriplesString(lm) + ".} union");

		}

		sb.append("{filter(false)}");

		sb.append("}");

		try (RepositoryConnection conn = repo.getConnection()) {
			TupleQuery query = conn.prepareTupleQuery(sb.toString());
			query.setIncludeInferred(true);
			BindingSet bs = QueryResults.singleResult(query.evaluate());

			if (bs != null) {
				String fullyQualifiedClassName = bs.getValue("terminologyAccessClass").stringValue();
				String symbolicName = bs.getValue("symbolicName").stringValue();
				String version = bs.getValue("version").stringValue();

				return loadParticipantClass(fullyQualifiedClassName, symbolicName, version);
			} else {
				return null;
			}
		}
	}

	// public Class<?> discoverMediator(MatchingProblem mediationProblem,
	// Map<Class<?>, ImmutablePair<?, ?>> availableInterfaces)
	// throws RDF4JException, ClassNotFoundException {
	// StringBuilder sb = new StringBuilder();
	// sb.append(
	// "prefix maple: <http://art.uniroma2.it/ontologies/maple/core#> prefix lime:
	// <http://art.uniroma2.it/ontologies/lime#>");
	// sb.append(
	// "select distinct * {?mediator a maple:Mediator ; maple:supportedMediationScenario ?mediationScenario
	// .");
	//
	// sb.append(getMediationScenarioFilter(mediationProblem));
	// sb.append(getAccessInterfaceFilter(availableInterfaces.keySet()));
	//
	// sb.append(
	// "?mediator maple:manifestation [maple:javaClass ?mediatorClass ; maple:plugin [maple:pluginId
	// ?symbolicName ; maple:version ?version]].");
	//
	// sb.append("}");
	// sb.append("order by asc (abs(?mediationScenarioMismatch))");
	// sb.append("limit 10");
	//
	// System.out.println(sb.toString());
	//
	// try (RepositoryConnection conn = repo.getConnection()) {
	// TupleQuery query = conn.prepareTupleQuery(sb.toString());
	// query.setIncludeInferred(true);
	// BindingSet bs = QueryResults.singleResult(query.evaluate());
	//
	// if (bs != null) {
	// String fullyQualifiedClassName = bs.getValue("mediatorClass").stringValue();
	// String symbolicName = bs.getValue("symbolicName").stringValue();
	// String version = bs.getValue("version").stringValue();
	//
	// Bundle aBundle = discoverBundle(symbolicName, version);
	//
	// return aBundle.loadClass(fullyQualifiedClassName);
	// } else {
	// throw new RuntimeException("No suitable mediator found");
	// }
	// }
	// }

	// private String getMediationScenarioFilter(MatchingProblem mediationProblem) {
	// if (mediationProblem instanceof MonolingualProblem) {
	// return "{?mediator maple:supportedMediationScenario maple:monolingualMediation. bind(0 as
	// ?mediationScenarioMismatch)} union {?mediator maple:supportedMediationScenario
	// maple:multilingualMediation. bind(1 as ?mediationScenarioMismatch)}";
	// } else if (mediationProblem instanceof MultilingualProblem) {
	// return "{?mediator maple:supportedMediationScenario maple:monolingualMediation. bind(-1 as
	// ?mediationScenarioMismatch)} union {?mediator maple:supportedMediationScenario
	// maple:multilingualMediation. bind(0 as ?mediationScenarioMismatch)}";
	// } else if (mediationProblem instanceof CrossLingualProblem) {
	// return "filter(?mediationScenario = maple:crossLanguageMediation)";
	// }
	//
	// return "{filter(false)}";
	// }

	private String getAccessInterfaceFilter(Set<Class<?>> availableInterfaces) {
		StringBuilder sb = new StringBuilder();
		Collection<String> interfaceQNames = Collections2.transform(availableInterfaces,
				new Clazz2QNameFunction());

		if (availableInterfaces.contains(LabelAccess.class)) {
			sb.append("optional {?mediator maple:linguisticAccess maple:LabelAccess}");
		}

		if (availableInterfaces.contains(TerminologyAccess.class)) {
			sb.append("optional {?mediator maple:linguisticAccess maple:TerminologyAccess}");
		}

		if (availableInterfaces.contains(FlatAccess.class)) {
			sb.append("optional {?mediator maple:conceptualAccess maple:FlatAccess}");
		}

		if (availableInterfaces.contains(HierarchicalAccess.class)) {
			sb.append("optional {?mediator maple:conceptualAccess maple:HierarchicalAccess}");
		}

		sb.append("filter not exists {?mediator maple:conceptualAccess ?ca . filter (");

		for (String availQName : interfaceQNames) {
			sb.append("?ca != ");
			sb.append(availQName);
			sb.append(" && ");
		}
		sb.append("true) }");

		sb.append("filter not exists {?mediator maple:linguisticAccess ?la . filter (");

		for (String availQName : interfaceQNames) {
			sb.append("?la != ");
			sb.append(availQName);
			sb.append(" && ");
		}
		sb.append("true) }");

		return sb.toString();
	}

	@Override
	public void pluginStateChanged(PluginStateEvent event) {
		PluginWrapper plugin = event.getPlugin();
		if (event.getPluginState() == PluginState.STARTED) { // a plugin gets activated
			try {
				register(plugin);
			} catch (Exception e) {
				logger.error("An exception occurred when registering the plugin {}", plugin.getPluginId(), e);
			}
		} else if (event.getOldState() == PluginState.STARTED) { // a plugin once active change its state
			try {
				unregister(plugin);
			} catch (Exception e) {
				logger.error("An exception occurred when unregistering the plugin {}", plugin.getPluginId(), e);
			}
		}
	}

	private static class Clazz2QNameFunction implements Function<Class<?>, String> {

		@Override
		public String apply(Class<?> from) {
			return "maple:" + from.getSimpleName();
		}

	}

	private Optional<PluginWrapper> discoverPlugin(String symbolicName, String version) {
		if (symbolicName.equals("it.uniroma2.art.maple.maple-core")) return Optional.empty();

		throw new NotImplementedException("Discoverey of non-local bundle is not currently supported");
	}

	public RepositoryConnection getConnection() {
		return repo.getConnection();
	}
}
