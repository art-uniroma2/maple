package it.uniroma2.art.maple.scenario;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects.ToStringHelper;
import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.profiler.LexicalizationSetStatistics;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

public class Alignment extends Dataset {

    private final IRI subjectsTarget;
    private final IRI objectsTarget;

    @JsonCreator
    public Alignment(@JsonProperty("@id") IRI id, @JsonProperty("uriSpace") String uriSpace,
                     @JsonProperty("title") Set<Literal> title, @JsonProperty("sparqlEndpoint") DataService sparqlEndpoint,
                     @JsonProperty("subjectsTarget") IRI subjectsTarget,
                     @JsonProperty("objectsTarget") IRI objectsTarget) {
        super(id, SimpleValueFactory.getInstance().createIRI("http://semanticturkey.uniroma2.it/ns/mdr#Alignment"), uriSpace, title, sparqlEndpoint, null);
        this.subjectsTarget = subjectsTarget;
        this.objectsTarget = objectsTarget;

    }

    public IRI getSubjectsTarget() {
        return subjectsTarget;
    }

    public IRI getObjectsTarget() {
        return objectsTarget;
    }

    @Override
    protected ToStringHelper toStringHelper() {
        return super.toStringHelper().add("subjectsTarget", subjectsTarget).add("objectsTarget", objectsTarget);
    }

}
