package it.uniroma2.art.maple.scenario;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import org.eclipse.rdf4j.model.IRI;

import java.util.List;

/**
 * Describes a chain of alignments for the two datasets (being matched).
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class AlignmentChain {
    private final double score;
    private final List<IRI> chain;

    @JsonCreator
    public AlignmentChain(@JsonProperty("score") double score,
                          @JsonProperty("chain") List<IRI> chain) {
        this.score = score;
        this.chain = chain;
    }

    public double getScore() {
        return score;
    }

    public List<IRI> getChain() {
        return chain;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("score", score).add("chain", chain).toString();
    }
}
