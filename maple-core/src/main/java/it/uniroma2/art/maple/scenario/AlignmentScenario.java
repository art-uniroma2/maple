package it.uniroma2.art.maple.scenario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Iterators;

import it.uniroma2.art.maple.scenario.AlignmentScenario.RefinableTaskReportDeserializer;
import it.uniroma2.art.maple.scenario.AlignmentScenario.RefinableTaskReportSerializer;
import it.uniroma2.art.maple.utilities.RDF4JJacksonModule;

/**
 * A report describing a given matching scenarios. It may include multiple options the user shall choose from
 * to compile a final scenario definition.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
@JsonSerialize(using = RefinableTaskReportSerializer.class)
@JsonDeserialize(using = RefinableTaskReportDeserializer.class)
public class AlignmentScenario {

    private final Dataset leftDataset;
    private final Dataset rightDataset;

    private final Collection<Dataset> supportDatasets;

    private final List<RefinablePairing> pairings;
    private final List<AlignmentChain> alignmentChains;

    @JsonCreator
    public AlignmentScenario(@JsonProperty("leftDataset") Dataset sourceDataset,
                             @JsonProperty("rightDataset") Dataset targetDataset,
                             @JsonProperty("supportDatasets") Collection<Dataset> supportDatasets,
                             @JsonProperty("pairings") List<RefinablePairing> pairings,
                             @JsonProperty("alignmentChains") List<AlignmentChain> alignmentChains) {
        this.leftDataset = sourceDataset;
        this.rightDataset = targetDataset;
        this.supportDatasets = supportDatasets;
        this.pairings = pairings;
        this.alignmentChains = alignmentChains;
    }

    public Dataset getLeftDataset() {
        return leftDataset;
    }

    public Dataset getRightDataset() {
        return rightDataset;
    }

    public Collection<Dataset> getSupportDatasets() {
        return supportDatasets;
    }

    public List<RefinablePairing> getPairings() {
        return pairings;
    }

    public List<AlignmentChain> getAlignmentChains() {
        return alignmentChains;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("leftDataset", leftDataset)
                .add("rightDataset", rightDataset).add("supportDatasets", supportDatasets)
                .add("pairings", pairings).add("alignmentChains", alignmentChains).toString();
    }

    public static class RefinableTaskReportSerializer extends StdSerializer<AlignmentScenario> {

        private static final long serialVersionUID = 9196563638788638731L;

        protected RefinableTaskReportSerializer() {
            super(AlignmentScenario.class);
        }

        @Override
        public void serialize(AlignmentScenario value, JsonGenerator gen, SerializerProvider provider)
                throws IOException {
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new RDF4JJacksonModule());
            om.registerModule(new Jdk8Module());
            ObjectCodec oldCodec = gen.getCodec();
            gen.setCodec(om);
            try {
                gen.writeStartObject();
                gen.writeObjectField("leftDataset", value.leftDataset);
                gen.writeObjectField("rightDataset", value.rightDataset);
                gen.writeObjectField("supportDatasets", value.supportDatasets);
                gen.writeObjectField("pairings", value.pairings);
                gen.writeObjectField("alignmentChains", value.alignmentChains);
                gen.writeEndObject();
            } finally {
                gen.setCodec(oldCodec);
            }
        }

    }

    public static class RefinableTaskReportDeserializer extends StdDeserializer<AlignmentScenario> {

        private static final long serialVersionUID = 6941852049397051347L;

        protected RefinableTaskReportDeserializer() {
            super(AlignmentScenario.class);
        }

        @Override
        public AlignmentScenario deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new RDF4JJacksonModule());
            om.registerModule(new Jdk8Module());
            ObjectCodec oldCodec = p.getCodec();
            p.setCodec(om);
            try {
                Dataset leftDataset = null;
                Dataset rightDataset = null;
                List<Dataset> supportDatasets = new ArrayList<>();
                List<RefinablePairing> pairings = new ArrayList<>();
                List<AlignmentChain> alignmentChains = new ArrayList<>();

                JsonToken token = p.currentToken();

                if (!token.isStructStart())
                    throw new IllegalArgumentException("Expected start of struct");

                while (!token.isStructEnd()) {
                    String fieldName = p.nextFieldName();
                    if (fieldName != null) {
                        token = p.nextToken();

                        if (fieldName.equals("leftDataset")) {
                            leftDataset = p.readValueAs(Dataset.class);
                        } else if (fieldName.equals("rightDataset")) {
                            rightDataset = p.readValueAs(Dataset.class);
                        } else if (fieldName.equals("supportDatasets")) {
                            if (token != JsonToken.START_ARRAY) {
                                throw new IOException("Expected start array");
                            }
                            if (!supportDatasets.isEmpty()) {
                                supportDatasets.clear();
                            }

                            token = p.nextToken();
                            if (token != JsonToken.END_ARRAY) {
                                Iterators.addAll(supportDatasets, p.readValuesAs(Dataset.class));
                            }
                        } else if (fieldName.equals("pairings")) {
                            if (token != JsonToken.START_ARRAY) {
                                throw new IOException("Expected start array");
                            }
                            if (!pairings.isEmpty()) {
                                pairings.clear();
                            }
                            token = p.nextToken();
                            if (token != JsonToken.END_ARRAY) {
                                Iterators.addAll(pairings, p.readValuesAs(RefinablePairing.class));
                            }
                        } else if (fieldName.equals("alignmentChains")) {
                            if (token != JsonToken.START_ARRAY) {
                                throw new IOException("Expected start array");
                            }
                            if (!alignmentChains.isEmpty()) {
                                alignmentChains.clear();
                            }
                            token = p.nextToken();
                            if (token != JsonToken.END_ARRAY) {
                                Iterators.addAll(alignmentChains, p.readValuesAs(AlignmentChain.class));
                            }
                        }
                    } else {
                        break;
                    }
                }

                return new AlignmentScenario(leftDataset, rightDataset, supportDatasets, pairings, alignmentChains);
            } finally {
                p.setCodec(oldCodec);
            }
        }

    }

}
