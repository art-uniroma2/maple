package it.uniroma2.art.maple.scenario;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.uniroma2.art.lime.profiler.ConceptualizationSetStatistics;

/**
 * Describes a conceptualization set
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class ConceptualizationSet extends Dataset {

	private ConceptualizationSetStatistics stats;

	@JsonCreator
	public ConceptualizationSet(@JsonProperty("@id") IRI id, @JsonProperty("@type") IRI type,
			@JsonProperty("uriSpace") String uriSpace, @JsonProperty("title") Set<Literal> title,
			@JsonProperty("sparqlEndpoint") DataService sparqlEndpoint,
			@JsonProperty("lexiconDataset") Resource lexiconDataset,
			@JsonProperty("conceptualDataset") Resource conceptualDataset,
			@JsonProperty("conceptualizations") BigInteger conceptualizations,
			@JsonProperty("concepts") BigInteger concepts,
			@JsonProperty("lexicalEntries") BigInteger lexicalEntries,
			@JsonProperty("avgSynonymy") BigDecimal avgSynonymy,
			@JsonProperty("avgAmbiguity") BigDecimal avgAmbiguity) {
		this(id, type, uriSpace, title, sparqlEndpoint, ConceptualizationSetStatistics.of(lexiconDataset,
				conceptualDataset, conceptualizations, concepts, lexicalEntries, avgSynonymy, avgAmbiguity));
	}

	public ConceptualizationSet(IRI id, IRI type, String uriSpace, Set<Literal> title, DataService sparqlEndpoint,
			ConceptualizationSetStatistics stats) {
		super(id, type, uriSpace, title, sparqlEndpoint, null);
		this.stats = stats;
	}

	public IRI getLexiconDataset() {
		return (IRI) stats.getLexiconDataset();
	}

	public IRI getConceptualDataset() {
		return (IRI) stats.getConceptualDataset();
	}

	public long getConceptualizations() {
		return stats.getConceptualizations().longValueExact();
	}

	public long getLexicalEntries() {
		return stats.getLexicalEntries().longValueExact();
	}

	public long getConcepts() {
		return stats.getConcepts().longValueExact();
	}

	public double getAvgAmbiguity() {
		return stats.getAvgAmbiguity().doubleValue();
	}

	public double getAvgSynonymy() {
		return stats.getAvgSynonymy().doubleValue();
	}

	@Override
	protected ToStringHelper toStringHelper() {
		return super.toStringHelper().add("lexiconDataset", getLexiconDataset())
				.add("conceptualDataset", getConceptualDataset())
				.add("conceptualizations", getConceptualizations()).add("lexicalEntries", getLexicalEntries())
				.add("concepts", getConcepts()).add("avgAmbiguity", getAvgAmbiguity())
				.add("avgSynonymy", getAvgSynonymy());
	}
}
