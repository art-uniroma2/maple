package it.uniroma2.art.maple.scenario;

import java.util.Optional;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Describes a data service providing access to a {@link Dataset}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class DataService {
	private String endpointURL;
	private Optional<String> username;
	private Optional<String> password;

	public DataService(String endpointURL) {
		this(endpointURL, null, null);
	}

	@JsonCreator
	public DataService(@JsonProperty("endpointURL") String endpointURL,
			@JsonProperty("username") String username, @JsonProperty("passowrd") String password) {
		this.endpointURL = endpointURL;
		this.username = Optional.ofNullable(username);
		this.password = Optional.ofNullable(password);
	}

	public String getEndpointURL() {
		return endpointURL;
	}

	@Nullable
	public Optional<String> getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = Optional.ofNullable(username);
	}

	@Nullable
	public Optional<String> getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = Optional.ofNullable(password);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(DataService.class).add("endpointURL", endpointURL)
				.add("username", username)
				.add("password", Optional.ofNullable(password).map(v -> "****").orElse(null)).toString();
	}

}
