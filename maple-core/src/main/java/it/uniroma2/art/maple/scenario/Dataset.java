package it.uniroma2.art.maple.scenario;

import java.util.Optional;
import java.util.Set;

import javax.annotation.Nullable;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.uniroma2.art.maple.orchestration.impl.ConceptSet;

/**
 * Describes a dataset that is involved in a matching scenario.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "@type", visible = true)
@JsonSubTypes({ @JsonSubTypes.Type(name = "http://rdfs.org/ns/void#Dataset", value = VoidDataset.class),
		@JsonSubTypes.Type(name = "http://www.w3.org/ns/lemon/ontolex#ConceptSet", value = ConceptSet.class),
		@JsonSubTypes.Type(name = "http://www.w3.org/ns/lemon/lime#Lexicon", value = Lexicon.class),
		@JsonSubTypes.Type(name = "http://www.w3.org/ns/lemon/lime#LexicalizationSet", value = LexicalizationSet.class),
		@JsonSubTypes.Type(name = "http://www.w3.org/ns/lemon/lime#ConceptualizationSet", value = ConceptualizationSet.class),
		@JsonSubTypes.Type(name = "http://semanticturkey.uniroma2.it/ns/mdr#Alignment", value = Alignment.class)})
public class Dataset {
	private final IRI id;
	private final IRI type;
	private final String uriSpace;
	private final Optional<DataService> sparqlEndpoint;
	private final Optional<IRI> conformsTo;
	private final Set<Literal> title;

	/**
	 * A dataset description
	 * 
	 * @param id
	 * @param type
	 * @param uriSpace
	 * @param sparqlEndpoint
	 */
	@JsonCreator
	public Dataset(@JsonProperty("@id") IRI id, @JsonProperty("@type") IRI type,
			@JsonProperty("uriSpace") String uriSpace, @JsonProperty("title") Set<Literal> title,
			@JsonProperty("sparqlEndpoint") @Nullable DataService sparqlEndpoint,
			@JsonProperty("conformsTo") @Nullable IRI conformsTo) {
		this.id = id;
		this.type = type;
		this.uriSpace = uriSpace;
		this.sparqlEndpoint = Optional.ofNullable(sparqlEndpoint);
		this.conformsTo = Optional.ofNullable(conformsTo);
		this.title = title;
	}

	@JsonProperty("@id")
	public IRI getId() {
		return id;
	}

	@JsonProperty("@type")
	public IRI getType() {
		return type;
	}

	public String getUriSpace() {
		return uriSpace;
	}

	public Optional<DataService> getSparqlEndpoint() {
		return sparqlEndpoint;
	}

	@JsonInclude(Include.NON_ABSENT)
	public Optional<IRI> getConformsTo() {
		return conformsTo;
	}
	
	public Set<Literal> getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return toStringHelper().toString();
	}

	protected ToStringHelper toStringHelper() {
		return MoreObjects.toStringHelper(this).add("id", id).add("type", type).add("uriSpace", uriSpace)
				.add("sparqlEndpoint", sparqlEndpoint).add("conformsTo", conformsTo).add("title", title);
	}
}
