package it.uniroma2.art.maple.scenario;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import javax.annotation.Nullable;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.profiler.LexicalizationSetStatistics;

public class LexicalizationSet extends Dataset {

	private final LexicalizationSetStatistics stats;

	@JsonCreator
	public LexicalizationSet(@JsonProperty("@id") IRI id, @JsonProperty("uriSpace") String uriSpace,
			@JsonProperty("title") Set<Literal> title, @JsonProperty("sparqlEndpoint") DataService sparqlEndpoint,
			@JsonProperty("referenceDataset") Resource referenceDataset,
			@JsonProperty("lexiconDataset") Resource lexiconDataset,
			@JsonProperty("lexicalizationModel") IRI lexicalizationModel,
			@JsonProperty("lexicalizations") BigInteger lexicalizations,
			@JsonProperty("references") BigInteger references,
			@JsonProperty("lexicalEntries") BigInteger lexicalEntries,
			@JsonProperty("avgNumOfLexicalizations") BigDecimal avgNumOfLexicalizations,
			@JsonProperty("percentage") BigDecimal percentage,
			@JsonProperty("languageTag") String languageTag, @JsonProperty("languageLexvo") IRI languageLexvo,
			@JsonProperty("languageLOC") IRI languageLOC) {
		this(id, uriSpace, title, sparqlEndpoint,
				LexicalizationSetStatistics.of(referenceDataset, lexiconDataset, lexicalizationModel,
						lexicalizations, references, lexicalEntries, avgNumOfLexicalizations, percentage,
						languageTag, languageLexvo, languageLOC));
	}

	public LexicalizationSet(IRI id, String uriSpace, Set<Literal> title, DataService sparqlEndpoint,
			LexicalizationSetStatistics stats) {
		super(id, LIME.LEXICALIZATION_SET, uriSpace, title, sparqlEndpoint, null);
		this.stats = stats;
	}

	public String getLanguageTag() {
		return stats.getLanguageTag();
	}

	public IRI getLexicalizationModel() {
		return stats.getLexicalizationModel();
	}

	public double getPercentage() {
		return stats.getPercentage().doubleValue();
	}

	public double getAvgNumOfLexicalizations() {
		return stats.getAvgNumOfLexicalizations().doubleValue();
	}

	public long getReferences() {
		return stats.getReferences().longValueExact();
	}

	public Long getLexicalEntries() {
		return stats.getLexicalEntries() != null ? stats.getLexicalEntries().longValueExact() : null;
	}

	public long getLexicalizations() {
		return stats.getLexicalizations().longValueExact();
	}

	public IRI getReferenceDataset() {
		return (IRI) stats.getReferenceDataset();
	}

	public @Nullable IRI getLexiconDataset() {
		return (IRI) stats.getLexiconDataset().orElse(null);
	}

	@Override
	protected ToStringHelper toStringHelper() {
		return super.toStringHelper().add("languageTag", getLanguageTag())
				.add("lexicalizationModel", getLexicalizationModel()).add("percentage", getPercentage())
				.add("avgNumOfLexicalizations", getAvgNumOfLexicalizations())
				.add("references", getReferences()).add("lexicalEntries", getLexicalEntries())
				.add("lexicalizations", getLexicalizations()).add("referenceDataset", getReferenceDataset())
				.add("lexiconDataset", getLexiconDataset());
	}

}
