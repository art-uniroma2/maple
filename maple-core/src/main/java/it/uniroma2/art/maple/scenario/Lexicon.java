package it.uniroma2.art.maple.scenario;

import java.math.BigInteger;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects.ToStringHelper;

import it.uniroma2.art.lime.profiler.LexiconStats;

/**
 * Describes a lexicon
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class Lexicon extends Dataset {

	private LexiconStats stats;

	@JsonCreator
	public Lexicon(@JsonProperty("@id") IRI id, @JsonProperty("@type") IRI type,
			@JsonProperty("uriSpace") String uriSpace, @JsonProperty("title") Set<Literal> title,
			@JsonProperty("sparqlEndpoint") DataService sparqlEndpoint,
			@JsonProperty("languageTag") String languageTag, @JsonProperty("languageLexvo") IRI languageLexvo,
			@JsonProperty("languageLOC") IRI languageLOC,
			@JsonProperty("linguisticCatalog") IRI linguisticCatalog,
			@JsonProperty("lexicalEntries") BigInteger lexicalEntries) {
		this(id, type, uriSpace, title, sparqlEndpoint,
				LexiconStats.of(languageTag, languageLexvo, languageLOC, linguisticCatalog, lexicalEntries));
	}

	public Lexicon(IRI id, IRI type, String uriSpace, Set<Literal> title, DataService sparqlEndpoint,
			LexiconStats stats) {
		super(id, type, uriSpace, title, sparqlEndpoint, null);
		this.stats = stats;
	}

	public long getLexicalEntries() {
		return stats.getLexicalEntries().longValueExact();
	}

	public String getLanguageTag() {
		return stats.getLanguageTag();
	}

	@Override
	protected ToStringHelper toStringHelper() {
		return super.toStringHelper().add("languageTag", getLanguageTag()).add("lexicalEntries",
				getLexicalEntries());
	}

}
