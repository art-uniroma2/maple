package it.uniroma2.art.maple.scenario;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Describes a pairing of lexicalization sets from two datasets (being matched).
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class Pairing {
	private final double score;
	private final PairingHand source;
	private final PairingHand target;
	private final Synonymizer synonymizer;

	@JsonCreator
	public Pairing(@JsonProperty("score") double score, @JsonProperty("source") PairingHand source,
			@JsonProperty("target") PairingHand target,
			@JsonProperty("synonymizers") @Nullable Synonymizer synonymizer) {
		this.score = score;
		this.source = source;
		this.target = target;
		this.synonymizer = synonymizer;
	}

	public double getScore() {
		return score;
	}

	public PairingHand getSource() {
		return source;
	}

	public PairingHand getTarget() {
		return target;
	}

	public @Nullable Synonymizer getSynonymizer() {
		return synonymizer;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("score", score).add("source", source)
				.add("target", target).add("synonymizers", synonymizer).toString();
	}
}
