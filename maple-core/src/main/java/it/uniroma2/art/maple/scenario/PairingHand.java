package it.uniroma2.art.maple.scenario;

import org.eclipse.rdf4j.model.IRI;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Describes a hand of a {@link RefinablePairing}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class PairingHand {
	private final IRI lexicalizationSet;

	@JsonCreator
	public PairingHand(@JsonProperty("lexicalizationSet") IRI lexicalizationSet) {
		this.lexicalizationSet = lexicalizationSet;
	}

	public IRI getLexicalizationSet() {
		return lexicalizationSet;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("lexicalizationSet", lexicalizationSet).toString();
	}
}
