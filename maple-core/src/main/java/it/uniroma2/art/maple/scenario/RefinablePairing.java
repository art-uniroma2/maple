package it.uniroma2.art.maple.scenario;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Describes a refinable pairing of lexicalization sets from two datasets (being matched).
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class RefinablePairing {
	private final double score;
	private final double bestCombinedScore;
	private final PairingHand source;
	private final PairingHand target;
	private final List<Synonymizer> synonymizers;

	@JsonCreator
	public RefinablePairing(@JsonProperty("score") double score, @JsonProperty("bestCombinedScore") double bestCombinedScore,
			@JsonProperty("source") PairingHand source, @JsonProperty("target") PairingHand target,
			@JsonProperty("synonymizers") List<Synonymizer> synonymizers) {
		this.score = score;
		this.bestCombinedScore = bestCombinedScore;
		this.source = source;
		this.target = target;
		this.synonymizers = synonymizers;
	}

	public double getScore() {
		return score;
	}

	public double getBestCombinedScore() {
		return bestCombinedScore;
	}

	public PairingHand getSource() {
		return source;
	}

	public PairingHand getTarget() {
		return target;
	}

	public List<Synonymizer> getSynonymizers() {
		return synonymizers;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("score", score).add("bestCombinedScore", bestCombinedScore)
				.add("source", source).add("target", target).add("synonymizers", synonymizers).toString();
	}
}
