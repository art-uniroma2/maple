package it.uniroma2.art.maple.scenario;

import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import com.google.common.base.MoreObjects;

/**
 * A single resource lexicalization set.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class ResourceLexicalizationSet {
	private String languageTag;
	private IRI lexicalizationModel;
	private List<Literal> labels;

	/**
	 * @param languageTag
	 * @param lexicalizationModel
	 * @param labels
	 */
	public ResourceLexicalizationSet(String languageTag, IRI lexicalizationModel, List<Literal> labels) {
		this.languageTag = languageTag;
		this.lexicalizationModel = lexicalizationModel;
		this.labels = labels;
	}

	public String getLanguageTag() {
		return languageTag;
	}

	public IRI getLexicalizationModel() {
		return lexicalizationModel;
	}

	public List<Literal> getLabels() {
		return labels;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("languageTag", languageTag)
				.add("lexicalizationModel", lexicalizationModel).add("labels", labels).toString();
	}
}
