package it.uniroma2.art.maple.scenario;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Iterators;
import it.uniroma2.art.maple.scenario.ScenarioDefinition.TaskReportDeserializer;
import it.uniroma2.art.maple.scenario.ScenarioDefinition.TaskReportSerializer;
import it.uniroma2.art.maple.utilities.RDF4JJacksonModule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A report describing a specific scenario definition. Differently from a {@link AlignmentScenario}, this kind
 * of report should not offer any degree of freedom in refining the matching strategy.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@JsonSerialize(using = TaskReportSerializer.class)
@JsonDeserialize(using = TaskReportDeserializer.class)
public class ScenarioDefinition {

	private final Dataset leftDataset;
	private final Dataset rightDataset;

	private final Collection<Dataset> supportDatasets;

	private final List<Pairing> pairings;
	private final List<AlignmentChain> alignmentChains;

	@JsonCreator
	public ScenarioDefinition(@JsonProperty("leftDataset") Dataset sourceDataset,
			@JsonProperty("rightDataset") Dataset targetDataset,
			@JsonProperty("supportDatasets") Collection<Dataset> supportDatasets,
			@JsonProperty("pairings") List<Pairing> pairings,
		    @JsonProperty("alignmentChains") List<AlignmentChain> alignmentChains) {
		this.leftDataset = sourceDataset;
		this.rightDataset = targetDataset;
		this.supportDatasets = supportDatasets;
		this.pairings = pairings;
		this.alignmentChains = alignmentChains;
	}

	public Dataset getLeftDataset() {
		return leftDataset;
	}

	public Dataset getRightDataset() {
		return rightDataset;
	}

	public Collection<Dataset> getSupportDatasets() {
		return supportDatasets;
	}

	public List<Pairing> getPairings() {
		return pairings;
	}

	public List<AlignmentChain> getAlignmentChains() {
		return alignmentChains;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("leftDataset", leftDataset)
				.add("rightDataset", rightDataset).add("supportDatasets", supportDatasets)
				.add("pairings", pairings).add("alignmentChains", alignmentChains).toString();
	}

	public static class TaskReportSerializer extends StdSerializer<ScenarioDefinition> {

		private static final long serialVersionUID = 9196563638788638731L;

		protected TaskReportSerializer() {
			super(ScenarioDefinition.class);
		}

		@Override
		public void serialize(ScenarioDefinition value, JsonGenerator gen, SerializerProvider provider)
				throws IOException {
			ObjectMapper om = new ObjectMapper();
			om.registerModule(new RDF4JJacksonModule());
			om.registerModule(new Jdk8Module());
			ObjectCodec oldCodec = gen.getCodec();
			gen.setCodec(om);
			try {
				gen.writeStartObject();
				gen.writeObjectField("leftDataset", value.leftDataset);
				gen.writeObjectField("rightDataset", value.rightDataset);
				gen.writeObjectField("supportDatasets", value.supportDatasets);
				gen.writeObjectField("pairings", value.pairings);
				gen.writeObjectField("alignmentChains", value.alignmentChains);
				gen.writeEndObject();
			} finally {
				gen.setCodec(oldCodec);
			}
		}

	}

	public static class TaskReportDeserializer extends StdDeserializer<ScenarioDefinition> {

		private static final long serialVersionUID = 6941852049397051347L;

		protected TaskReportDeserializer() {
			super(ScenarioDefinition.class);
		}

		@Override
		public ScenarioDefinition deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			ObjectMapper om = new ObjectMapper();
			om.registerModule(new RDF4JJacksonModule());
			om.registerModule(new Jdk8Module());
			ObjectCodec oldCodec = p.getCodec();
			p.setCodec(om);
			try {
				Dataset leftDataset = null;
				Dataset rightDataset = null;
				List<Dataset> supportDatasets = new ArrayList<>();
				List<Pairing> pairings = new ArrayList<>();
				List<AlignmentChain> alignmentChains = new ArrayList<>();

				JsonToken token = p.currentToken();

				if (!token.isStructStart())
					throw new IllegalArgumentException("Expected start of struct");

				while (!token.isStructEnd()) {
					String fieldName = p.nextFieldName();
					if (fieldName != null) {
						token = p.nextToken();

						if (fieldName.equals("leftDataset")) {
							leftDataset = p.readValueAs(Dataset.class);
						} else if (fieldName.equals("rightDataset")) {
							rightDataset = p.readValueAs(Dataset.class);
						} else if (fieldName.equals("supportDatasets")) {
							if (token != JsonToken.START_ARRAY) {
								throw new IOException("Expected start array");
							}
							if (!supportDatasets.isEmpty()) {
								supportDatasets.clear();
							}
							supportDatasets.addAll(om.readValue(p, new TypeReference<List<Dataset>>(){}));
						} else if (fieldName.equals("pairings")) {
							if (token != JsonToken.START_ARRAY) {
								throw new IOException("Expected start array");
							}
							if (!pairings.isEmpty()) {
								pairings.clear();
							}
							pairings.addAll(om.readValue(p, new TypeReference<List<Pairing>>(){}));
						}else if (fieldName.equals("alignmentChains")) {
							if (token != JsonToken.START_ARRAY) {
								throw new IOException("Expected start array");
							}
							if (!alignmentChains.isEmpty()) {
								alignmentChains.clear();
							}
							alignmentChains.addAll(om.readValue(p, new TypeReference<List<AlignmentChain>>(){}));
						}
					} else {
						break;
					}
				}

				return new ScenarioDefinition(leftDataset, rightDataset, supportDatasets, pairings, alignmentChains);
			} finally {
				p.setCodec(oldCodec);
			}
		}

	}
}
