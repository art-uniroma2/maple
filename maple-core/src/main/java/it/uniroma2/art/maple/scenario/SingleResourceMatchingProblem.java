package it.uniroma2.art.maple.scenario;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.lime.profiler.LexicalizationSetStatistics;

/**
 * The problem of matching an individual resource against a target dataset.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class SingleResourceMatchingProblem {

	private IRI sourceResource;
	private IRI targetDataset;
	private List<Pair<ResourceLexicalizationSet, LexicalizationSetStatistics>> pairedLexicalizationSets;

	public SingleResourceMatchingProblem(IRI sourceResource, IRI targetDataset,
			List<Pair<ResourceLexicalizationSet, LexicalizationSetStatistics>> pairedLexicalizationSets) {
		this.sourceResource = sourceResource;
		this.targetDataset = targetDataset;
		this.pairedLexicalizationSets = pairedLexicalizationSets;
	}

	public IRI getSourceResource() {
		return sourceResource;
	}

	public IRI getTargetDataset() {
		return targetDataset;
	}

	public List<Pair<ResourceLexicalizationSet, LexicalizationSetStatistics>> getPairedLexicalizationSets() {
		return pairedLexicalizationSets;
	}
}
