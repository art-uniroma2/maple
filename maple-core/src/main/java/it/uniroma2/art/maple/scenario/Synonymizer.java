package it.uniroma2.art.maple.scenario;

import org.eclipse.rdf4j.model.IRI;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Describes a language resource used to obtain synonyms.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class Synonymizer {
	private final IRI lexicon;
	private final IRI conceptualizationSet;
	private final double score;

	@JsonCreator
	public Synonymizer(@JsonProperty("score") double score, @JsonProperty("lexicon") IRI lexicon,
			@JsonProperty("conceptualizationSet") IRI conceptualizationSet) {
		this.score = score;
		this.lexicon = lexicon;
		this.conceptualizationSet = conceptualizationSet;
	}

	public double getScore() {
		return score;
	}

	public IRI getLexicon() {
		return lexicon;
	}

	public IRI getConceptualizationSet() {
		return conceptualizationSet;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("score", score).add("lexicon", lexicon)
				.add("conceptualizationSet", conceptualizationSet).toString();
	}
}
