package it.uniroma2.art.maple.scenario;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes a void:Dataset that is involved in a matching scenario.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class VoidDataset extends Dataset {

	@JsonCreator
	public VoidDataset(@JsonProperty("@id") IRI id, @JsonProperty("@type") IRI type,
			@JsonProperty("uriSpace") String uriSpace, @JsonProperty("title") Set<Literal> title,
			@JsonProperty("sparqlEndpoint") DataService sparqlEndpoint,
			@JsonProperty("conformsTo") IRI conformsTo) {
		super(id, type, uriSpace, title, sparqlEndpoint, conformsTo);
	}

}
