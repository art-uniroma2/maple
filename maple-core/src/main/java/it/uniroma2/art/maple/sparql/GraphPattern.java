package it.uniroma2.art.maple.sparql;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.Query;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;

/**
 * A SPARQL graph pattern with additional metadata easing its composition into another query.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class GraphPattern<T> {
	private final Collection<String> inputVariables;
	private final String outputVariable;
	private final Set<Namespace> namespaces;
	private final String graphPattern;

	public GraphPattern(Collection<String> inputVariables, String outputVariable, Set<Namespace> namespaces,
			String graphPattern) {
		this.inputVariables = Collections.unmodifiableCollection(inputVariables);
		this.outputVariable = outputVariable;
		this.graphPattern = graphPattern;
		this.namespaces = Collections.unmodifiableSet(namespaces);
	}

	public String getGraphPattern() {
		return graphPattern;
	}

	public Collection<String> getInputVariables() {
		return inputVariables;
	}

	public String getOutputVariable() {
		return outputVariable;
	}

	public Set<Namespace> getNamespaces() {
		return namespaces;
	}
	
	public BooleanQuery prepareBooleanQuery(RepositoryConnection conn) {
		return (BooleanQuery) prepareQuery(conn);
	}

	public TupleQuery prepareTupleQuery(RepositoryConnection conn) {
		return (TupleQuery) prepareQuery(conn);
	}

	public Query prepareQuery(RepositoryConnection conn) {
		StringBuilder sb = new StringBuilder();
		for (Namespace ns : namespaces) {
			sb.append("prefix ").append(ns.getPrefix()).append(": <").append(ns.getName()).append(">\n");
		}
		if (outputVariable != null) {
			sb.append("select distinct ?");
			sb.append(outputVariable);
		} else {
			sb.append("ask");
		}
		sb.append(" {\n");
		sb.append(graphPattern);
		sb.append("}\n");

		Query query = conn.prepareQuery(sb.toString());
		query.setIncludeInferred(true);
		return query;
	}
}
