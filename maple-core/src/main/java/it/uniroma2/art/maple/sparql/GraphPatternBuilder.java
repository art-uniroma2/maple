package it.uniroma2.art.maple.sparql;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.rdf4j.model.Namespace;

/**
 * A utility class to build an immutable intance of {@link GraphPattern}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class GraphPatternBuilder {
	private Set<String> inputVariables;
	private String outputVariable;
	private String graphPattern;
	private Set<Namespace> namespaces;

	public GraphPatternBuilder() {
		inputVariables = new HashSet<>();
		outputVariable = null;
		graphPattern = null;
		namespaces = new HashSet<>();
	}

	public GraphPatternBuilder inputVariable(String variableName) {
		this.inputVariables.add(variableName);
		return this;
	}

	public GraphPatternBuilder outputVariable(String variableName) {
		this.outputVariable = variableName;
		return this;
	}

	public GraphPatternBuilder graphPattern(String graphPattern) {
		this.graphPattern = graphPattern;
		return this;
	}

	public GraphPatternBuilder ns(Namespace ns) {
		this.namespaces.add(ns);
		return this;
	}

	public <T> GraphPattern<T> build() {
		if (graphPattern == null) {
			throw new IllegalStateException("SPARQL graph pattern not set");
		}

		return new GraphPattern<T>(inputVariables, outputVariable, namespaces, graphPattern);
	}

}
