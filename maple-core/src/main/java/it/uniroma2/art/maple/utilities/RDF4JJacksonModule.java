package it.uniroma2.art.maple.utilities;

import java.io.IOException;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class RDF4JJacksonModule extends SimpleModule {

	private static final long serialVersionUID = 6123117372382189771L;

	@Override
	public void setupModule(SetupContext context) {
		this.addSerializer(IRI.class, new StdSerializer<IRI>(IRI.class) {

			private static final long serialVersionUID = 6525027567601632185L;

			@Override
			public void serialize(IRI value, JsonGenerator gen, SerializerProvider provider)
					throws IOException {
				gen.writeString(value.toString());
			}

		});
		this.addDeserializer(IRI.class, new StdDeserializer<IRI>(IRI.class) {

			private static final long serialVersionUID = 2314796752221956593L;

			@Override
			public IRI deserialize(JsonParser p, DeserializationContext ctxt)
					throws IOException, JsonProcessingException {
				String s = p.getValueAsString();
				return s == null ? null : SimpleValueFactory.getInstance().createIRI(s);
			}

		});
		this.addSerializer(Resource.class, new StdSerializer<Resource>(Resource.class) {

			private static final long serialVersionUID = 8811315351834781075L;

			@Override
			public void serialize(Resource value, JsonGenerator gen, SerializerProvider provider)
					throws IOException {
				gen.writeString(value.toString());
			}

		});
		this.addDeserializer(Resource.class, new StdDeserializer<Resource>(Resource.class) {

			private static final long serialVersionUID = 1175403748039003051L;

			@Override
			public Resource deserialize(JsonParser p, DeserializationContext ctxt)
					throws IOException, JsonProcessingException {
				String s = p.getValueAsString();
				return s == null ? null
						: s.startsWith("_") ? SimpleValueFactory.getInstance().createBNode(s.substring(2))
								: SimpleValueFactory.getInstance().createIRI(s);
			}

		});
		this.addSerializer(Literal.class, new StdSerializer<Literal>(Literal.class) {

			private static final long serialVersionUID = 6525027567601632195L;

			@Override
			public void serialize(Literal value, JsonGenerator gen, SerializerProvider provider)
					throws IOException {
				if (XMLSchema.STRING.equals(value.getDatatype())) {
					gen.writeString(value.getLabel());
				} else {
					gen.writeStartObject();
					gen.writeStringField("@value", value.getLabel());
					if (RDF.LANGSTRING.equals(value.getDatatype())) {
						gen.writeStringField("@lang", value.getLanguage().orElse(""));
					} else {
						gen.writeStringField("@type", value.getLabel());
					}
					gen.writeEndObject();
				}
			}

		});
		this.addDeserializer(Literal.class, new StdDeserializer<Literal>(Literal.class) {

			private static final long serialVersionUID = 6525027567601632195L;

			@Override
			public Literal deserialize(JsonParser p, DeserializationContext ctxt)
					throws IOException, JsonProcessingException {
				SimpleValueFactory vf = SimpleValueFactory.getInstance();
				if (JsonToken.VALUE_STRING == p.currentToken()) {
					return vf.createLiteral(p.getValueAsString());
				} else {
					ReifiedLiteral reifiedLit = p.readValueAs(ReifiedLiteral.class);

					if (reifiedLit.lang != null) {
						return vf.createLiteral(reifiedLit.value, reifiedLit.lang);
					} else {
						return vf.createLiteral(reifiedLit.value, reifiedLit.type);
					}
				}
			}

		});

		super.setupModule(context);
	}

	private static class ReifiedLiteral {
		@JsonProperty(value = "@value", required = true)
		public String value;

		@JsonProperty("@type")
		public IRI type;

		@JsonProperty("@lang")
		public String lang;
	}

}
