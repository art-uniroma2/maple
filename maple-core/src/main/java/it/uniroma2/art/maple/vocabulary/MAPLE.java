package it.uniroma2.art.maple.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

/**
 * Vocabulary file for MAPLE
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class MAPLE {

	/** http://art.uniroma2.it/ontologies/maple/core **/
	public static final String URI = "http://art.uniroma2.it/ontologies/maple/core";

	/** http://art.uniroma2.it/ontologies/maple/core# **/
	public static final String NAMESPACE = "http://art.uniroma2.it/ontologies/maple/core#";

	/**
	 * Recommended prefix for the MAPLE namespace: "maple"
	 */
	public static final String PREFIX = "maple";

	/**
	 * An immutable {@link Namespace} constant that represents the MAPLE namespace.
	 */
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	// INDIVIDUALS

	/** maple:monolingualMediation **/
	public static final IRI MONOLINGUALMEDIATION;

	/** maple:multilingualMediation **/
	public static final IRI MULTILINGUALMEDIATION;

	/** maple:crosslingualMediation **/
	public static final IRI CROSSLINGUALMEDIATION;

	/** maple:FlatAccess **/
	public static final IRI FLAT_ACCESS;

	/** maple:HierarchicalAccess **/
	public static final IRI HIERARCHICAL_ACCESS;

	/** maple:LabelAccess **/
	public static final IRI LABEL_ACCESS;

	/** maple:TerminologyAccess **/
	public static final IRI TERMINOLOGY_ACCESS;

	/** maple:requiresFlatAccessQueryProvider **/
	public static final IRI REQUIRES_FLAT_ACCESS_QUERY_PROVIDER;

	/** maple:requiresHierarchicalAccessQueryProvider **/
	public static final IRI REQUIRES_HIERARCHICAL_ACCESS_QUERY_PROVIDER;

	/** maple:requiresLabelAccessQueryProvider **/
	public static final IRI REQUIRES_LABEL_ACCESS_QUERY_PROVIDER;

	/** maple:requiresTerminologyAccessQueryProvider **/
	public static final IRI REQUIRES_TERMINOLOGY_ACCESS_QUERY_PROVIDER;

	/** maple:FlatAccessQueryProvider **/
	public static final IRI FLAT_ACCESS_QUERY_PROVIDER;

	/** maple:HierarchicalAccessQueryProvider **/
	public static final IRI HIERARCHICAL_ACCESS_QUERY_PROVIDER;

	/** maple:LabelAccessQueryProvider **/
	public static final IRI LABEL_ACCESS_QUERY_PROVIDER;

	/** maple:TerminologyAccessQueryProvider **/
	public static final IRI TERMINOLOGY_ACCESS_QUERY_PROVIDER;

	static {
		ValueFactory factory = SimpleValueFactory.getInstance();
		MONOLINGUALMEDIATION = factory.createIRI(NAMESPACE, "monolingualMediation");
		MULTILINGUALMEDIATION = factory.createIRI(NAMESPACE, "multilingualMediation");
		CROSSLINGUALMEDIATION = factory.createIRI(NAMESPACE, "crosslingualMediation");

		FLAT_ACCESS = factory.createIRI(NAMESPACE, "FlatAccess");
		HIERARCHICAL_ACCESS = factory.createIRI(NAMESPACE, "HierarchicalAccess");
		LABEL_ACCESS = factory.createIRI(NAMESPACE, "LabelAccess");
		TERMINOLOGY_ACCESS = factory.createIRI(NAMESPACE, "TerminologyAccess");

		REQUIRES_FLAT_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE, "requiresFlatAccessQueryProvider");
		REQUIRES_HIERARCHICAL_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE,
				"requiresHierarchicalAccessQueryProvider");
		REQUIRES_LABEL_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE,
				"requiresLabelAccessQueryProvider");
		REQUIRES_TERMINOLOGY_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE,
				"requiresTerminologyAccessQueryProvider");

		FLAT_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE, "FlatAccessQueryProvider");
		HIERARCHICAL_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE, "HierarchicalAccessQueryProvider");
		LABEL_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE, "LabelAccessQueryProvider");
		TERMINOLOGY_ACCESS_QUERY_PROVIDER = factory.createIRI(NAMESPACE, "TerminologyAccessQueryProvider");

	}

}
