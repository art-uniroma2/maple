package it.uniroma2.art.maple.access.impl;

import java.io.File;
import java.io.IOException;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.inferencer.fc.DirectTypeHierarchyInferencer;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.inferencer.fc.SchemaCachingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.AfterClass;

import com.google.common.collect.Sets;
import com.google.common.io.Files;

/**
 * The base class of tests that need to load a model. Concrete subclasses must invoke {@link #loadModel(IRI,
 * String))} to load a model.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public abstract class AbstractModelBasedTest {
	protected static Repository repo;
	protected static RepositoryConnection conn;
	protected static ValueFactory vf;
	protected static String baseUri;

	static {
		vf = SimpleValueFactory.getInstance();
	}

	public static void loadModel(IRI model, String baseUri) throws RDF4JException, IOException {
		repo = new SailRepository(new DirectTypeHierarchyInferencer(new SchemaCachingRDFSInferencer(new MemoryStore())));
		repo.init();
		AbstractModelBasedTest.baseUri = baseUri;

		conn = repo.getConnection();
		conn.setNamespace("", createDefaultNamespaceFromBaseURI(baseUri));

		if (Sets.newHashSet("http://www.w3.org/2004/02/skos/core", "http://www.w3.org/2008/05/skos-xl")
				.contains(model.stringValue())) {
			conn.add(
					AbstractModelBasedTest.class
							.getResourceAsStream("/it/uniroma2/art/maple/vocabulary/skos.rdf"),
					"http://www.w3.org/2004/02/skos/core", RDFFormat.RDFXML,
					vf.createIRI("http://www.w3.org/2004/02/skos/core"));
		}

	}

	protected static String createDefaultNamespaceFromBaseURI(String baseUri) {
		if (baseUri.endsWith("/")) {
			return baseUri;
		} else {
			return baseUri + "#";
		}
	}

	protected static IRI qname(String qn) {
		String[] components = qn.split(":");

		if (components.length != 2) {
			throw new IllegalArgumentException("Not a valid qname: " + qn);
		}

		return iri(conn.getNamespace(components[0]) + components[1]);
	}

	protected static IRI iri(String iriSpec) {
		return vf.createIRI(iriSpec);
	}

	protected static Literal literal(String label, String lang) {
		return vf.createLiteral(label, lang);
	}

	protected static Literal literal(String label) {
		return vf.createLiteral(label);
	}

	@AfterClass
	public static void tearDown() throws RDF4JException {
		try {
			if (conn != null) {
				conn.close();
			}
		} finally {
			if (repo != null) {
				repo.shutDown();
			}
		}
	}
}
