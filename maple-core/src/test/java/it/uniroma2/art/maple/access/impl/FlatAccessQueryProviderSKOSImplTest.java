package it.uniroma2.art.maple.access.impl;

import java.util.List;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.containsInAnyOrder;

import it.uniroma2.art.maple.components.access.impl.FlatAccessQueryImpl;
import it.uniroma2.art.maple.components.access.impl.FlatAccessQueryProviderSKOSImpl;

/**
 * A test case for {@link FlatAccessQueryProviderSKOSImpl}. The underlying data model is
 * /maple-core/src/test/resources/it/uniroma2/art/maple/access/impl/exampleThesaurus.ttl
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class FlatAccessQueryProviderSKOSImplTest extends AbstractModelBasedTest {

	protected static FlatAccessQueryImpl flatAccess;
	protected static FlatAccessQueryProviderSKOSImpl queryProvider;

	@BeforeClass
	public static void setUp() throws Exception {
		loadModel(iri("http://www.w3.org/2004/02/skos/core"), "http://example.org");
		conn.add(FlatAccessQueryProviderOWLImplTest.class.getResource("exampleThesaurus.ttl"), null,
				RDFFormat.TURTLE);

		queryProvider = new FlatAccessQueryProviderSKOSImpl();
		flatAccess = new FlatAccessQueryImpl();
		flatAccess.setRepositoryConnection(conn);
		flatAccess.setConceptualAccessQueryProvider(queryProvider);
		flatAccess.initialize();
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected resources.
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void test() throws RDF4JException {
		List<IRI> actualCollection = QueryResults.asList(flatAccess.listNamedConcepts());

		assertThat(actualCollection,
				containsInAnyOrder(qname(":animalia"), qname(":mammalia"), qname(":reptilia"),
						qname(":canis"), qname(":felis"), qname(":crocodylus"), qname(":testudo")));
	}
}
