package it.uniroma2.art.maple.access.impl;

import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.is;

import java.util.List;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;

import it.uniroma2.art.maple.components.access.impl.HierarchicalAccessQueryImpl;
import it.uniroma2.art.maple.components.access.impl.HierarchicalAccessQueryProviderSKOSImpl;
import it.uniroma2.art.maple.test.filter.HasChildNamedConcept_Predicate;
import it.uniroma2.art.maple.test.filter.HasParentNamedConcept_Predicate;
import it.uniroma2.art.maple.test.matcher.MapleMatchers;

/**
 * A test case for {@link HierarchicalAccessQueryProviderSKOSImpl}. The underlying data model is
 * /maple-core/src/test/resources/it/uniroma2/art/maple/access/impl/exampleThesaurus.ttl
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class HierarchicalAccessQueryProviderSKOSImplTest extends AbstractModelBasedTest {

	protected static HierarchicalAccessQueryProviderSKOSImpl queryProvider;

	protected static HierarchicalAccessQueryImpl hierarchicalAccess;

	private static IRI ANIMALIA;

	private static IRI MAMMALIA;

	private static IRI REPTILIA;

	private static IRI CANIS;

	private static IRI CROCODYLUS;

	private static IRI TESTUDO;

	private static IRI FELIS;

	@BeforeClass
	public static void setUp() throws Exception {
		loadModel(iri("http://www.w3.org/2004/02/skos/core"), "http://example.org");
		conn.add(FlatAccessQueryProviderOWLImplTest.class.getResource("exampleThesaurus.ttl"), null,
				RDFFormat.TURTLE);
		queryProvider = new HierarchicalAccessQueryProviderSKOSImpl();

		hierarchicalAccess = new HierarchicalAccessQueryImpl();
		hierarchicalAccess.setConceptualAccessQueryProvider(queryProvider);
		hierarchicalAccess.setRepositoryConnection(conn);
		hierarchicalAccess.initialize();
		
		ANIMALIA = qname(":animalia");
		MAMMALIA = qname(":mammalia");
		REPTILIA = qname(":reptilia");
		CANIS = qname(":canis");
		FELIS = qname(":felis");
		CROCODYLUS = qname(":crocodylus");
		TESTUDO = qname(":testudo");
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected resources.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListNamedConcepts() throws RDF4JException {
		List<IRI> namedConcepts = QueryResults.asList(hierarchicalAccess.listNamedConcepts());

		assertThat(namedConcepts,
				containsInAnyOrder(ANIMALIA, MAMMALIA, REPTILIA, CANIS, FELIS, CROCODYLUS, TESTUDO));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected topmost resources.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testUppermostNamedConcepts() throws RDF4JException {
		List<IRI> namedConcepts = QueryResults.asList(hierarchicalAccess.listUppermostNamedConcepts());

		assertThat(namedConcepts, containsInAnyOrder(ANIMALIA));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected direct children of each
	 * resource in the underlying ontology. For each child, the test checks that the method
	 * {@link HierarchicalAccessQueryProviderSKOSImpl#hasParentNamedConcept(ARTURIResource, ARTURIResource)}
	 * returns <code>true</code>.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testChildNamedConcepts() throws RDF4JException {
		List<IRI> childNamedConcepts = QueryResults
				.asList(hierarchicalAccess.listChildNamedConcepts(ANIMALIA));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, ANIMALIA))));
		assertThat(childNamedConcepts, containsInAnyOrder(MAMMALIA, REPTILIA));

		childNamedConcepts = QueryResults.asList(hierarchicalAccess.listChildNamedConcepts(MAMMALIA));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, MAMMALIA))));
		assertThat(childNamedConcepts, containsInAnyOrder(FELIS, CANIS));

		childNamedConcepts = QueryResults.asList(hierarchicalAccess.listChildNamedConcepts(REPTILIA));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, REPTILIA))));
		assertThat(childNamedConcepts, containsInAnyOrder(CROCODYLUS, TESTUDO));

		childNamedConcepts = QueryResults.asList(hierarchicalAccess.listChildNamedConcepts(FELIS));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, FELIS))));
		assertThat(childNamedConcepts, is(empty()));

		childNamedConcepts = QueryResults.asList(hierarchicalAccess.listChildNamedConcepts(CANIS));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, CANIS))));
		assertThat(childNamedConcepts, is(empty()));

		childNamedConcepts = QueryResults.asList(hierarchicalAccess.listChildNamedConcepts(CROCODYLUS));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, CROCODYLUS))));
		assertThat(childNamedConcepts, is(empty()));

		childNamedConcepts = QueryResults.asList(hierarchicalAccess.listChildNamedConcepts(TESTUDO));

		assertThat(childNamedConcepts, everyItem(MapleMatchers
				.satifies(HasParentNamedConcept_Predicate.getPredicate(hierarchicalAccess, TESTUDO))));
		assertThat(childNamedConcepts, is(empty()));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected direct parents of each
	 * resource in the underlying ontology. For each parent, the test checks that the method
	 * {@link HierarchicalAccessQueryProviderSKOSImpl#hasChildNamedConcept(ARTURIResource, ARTURIResource)}
	 * returns <code>true</code>.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testParentNamedConcepts() throws RDF4JException {
		List<IRI> parentNamedConcepts = QueryResults
				.asList(hierarchicalAccess.listParentNamedConcepts(ANIMALIA));

		assertThat(parentNamedConcepts, everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, ANIMALIA))));
		assertThat(parentNamedConcepts, is(empty()));

		parentNamedConcepts = QueryResults.asList(hierarchicalAccess.listParentNamedConcepts(MAMMALIA));

		assertThat(parentNamedConcepts, CoreMatchers.everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, MAMMALIA))));
		assertThat(parentNamedConcepts, containsInAnyOrder(ANIMALIA));

		parentNamedConcepts = QueryResults.asList(hierarchicalAccess.listParentNamedConcepts(REPTILIA));

		assertThat(parentNamedConcepts, everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, REPTILIA))));
		assertThat(parentNamedConcepts, containsInAnyOrder(ANIMALIA));

		parentNamedConcepts = QueryResults.asList(hierarchicalAccess.listParentNamedConcepts(CANIS));

		assertThat(parentNamedConcepts, everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, CANIS))));
		assertThat(parentNamedConcepts, containsInAnyOrder(MAMMALIA));

		parentNamedConcepts = QueryResults.asList(hierarchicalAccess.listParentNamedConcepts(FELIS));

		assertThat(parentNamedConcepts, everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, FELIS))));
		assertThat(parentNamedConcepts, containsInAnyOrder(MAMMALIA));

		parentNamedConcepts = QueryResults.asList(hierarchicalAccess.listParentNamedConcepts(CROCODYLUS));

		assertThat(parentNamedConcepts, everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, CROCODYLUS))));
		assertThat(parentNamedConcepts, containsInAnyOrder(REPTILIA));

		parentNamedConcepts = QueryResults.asList(hierarchicalAccess.listParentNamedConcepts(TESTUDO));

		assertThat(parentNamedConcepts, everyItem(MapleMatchers
				.satifies(HasChildNamedConcept_Predicate.getPredicate(hierarchicalAccess, TESTUDO))));
		assertThat(parentNamedConcepts, containsInAnyOrder(REPTILIA));
	}

	/**
	 * Verifies cases in which
	 * {@link HierarchicalAccessQueryProviderSKOSImpl#hasChildNamedConcept(ARTURIResource, ARTURIResource)}
	 * and
	 * {@link HierarchicalAccessQueryProviderSKOSImpl#hasParentNamedConcept(ARTURIResource, ARTURIResource)}
	 * are expected to return false: i.e. reflexive invocations, non direct neighbor, ...
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testDirectNeighbor() throws RDF4JException {
		Assert.assertFalse(hierarchicalAccess.hasParentNamedConcept(ANIMALIA, ANIMALIA));
		Assert.assertFalse(hierarchicalAccess.hasParentNamedConcept(ANIMALIA, RDFS.RESOURCE));
		Assert.assertFalse(hierarchicalAccess.hasChildNamedConcept(ANIMALIA, ANIMALIA));
		Assert.assertFalse(hierarchicalAccess.hasChildNamedConcept(ANIMALIA, CANIS));
	}
}
