package it.uniroma2.art.maple.access.impl;

import java.util.List;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.containsInAnyOrder;

import it.uniroma2.art.maple.components.access.impl.LabelAccessQueryImpl;
import it.uniroma2.art.maple.components.access.impl.LabelAccessQueryProviderRDFSImpl;

/**
 * A test case for {@link LabelAccessQueryProviderRDFSImpl}. The underlying data model is
 * /maple-core/src/test/resources/it/uniroma2/art/maple/access/impl/exampleRdfsLabels.ttl
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class LabelAccessQueryProviderRDFSImplTest extends AbstractModelBasedTest {
	private static LabelAccessQueryImpl labelAccess;
	private static Literal prova;
	private static Literal esempio;
	private static Literal test;
	private static Literal example;
	private static Literal simpleLiteral;
	private static LabelAccessQueryProviderRDFSImpl queryProvider;

	@BeforeClass
	public static void setUp() throws Exception {
		loadModel(iri("http://www.w3.org/2000/01/rdf-schema"), "http://example.org");
		conn.add(FlatAccessQueryProviderOWLImplTest.class.getResource("exampleRdfsLabels.ttl"), null,
				RDFFormat.TURTLE);
		queryProvider = new LabelAccessQueryProviderRDFSImpl();
		labelAccess = new LabelAccessQueryImpl();
		labelAccess.setLinguisticAccessQueryProvider(queryProvider);
		labelAccess.setRepositoryConnection(conn);
		labelAccess.initialize();

		prova = literal("prova", "it");
		esempio = literal("esempio", "it");

		test = literal("test", "en");
		example = literal("example", "en");

		simpleLiteral = literal("simpleLiteral");
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected rdf:label(s).
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testAllLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> allLabels = QueryResults.asList(labelAccess.listLabels(resource));

		assertThat(allLabels, containsInAnyOrder(prova, esempio, test, example, simpleLiteral));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected rdf:label(s) in Italian.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testItLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> allLabels = QueryResults.asList(labelAccess.listLabels(resource, "it"));

		assertThat(allLabels, containsInAnyOrder(prova, esempio));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected rdf:label(s) in English.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testEnLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> allLabels = QueryResults.asList(labelAccess.listLabels(resource, "en"));

		assertThat(allLabels, containsInAnyOrder(test, example));
	}

}
