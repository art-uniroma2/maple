package it.uniroma2.art.maple.access.impl;

import java.util.List;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import it.uniroma2.art.maple.components.access.impl.TerminologyAccessQueryImpl;
import it.uniroma2.art.maple.components.access.impl.TerminologyAccessQueryProviderSKOSXLImpl;

/**
 * A test case for {@link TerminologyAccessSKOSXLImpl}. The access object is expected to ignore existing
 * rdfs:label(s) and skos:{pref,alt,hidden}Labels. The underlying data model is
 * /maple-core/src/test/resources/it/uniroma2/art/maple/access/impl/exampleSKOSXLLabels.ttl
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class TerminologyAccessQueryProviderSKOSXLImplTest extends AbstractModelBasedTest {
	private static Literal PREF_LABEL_ITA;
	private static Literal ALT_LABEL1_ITA;
	private static Literal ALT_LABEL2_ITA;
	private static Literal HIDDEN_LABEL1_ITA;
	private static Literal HIDDEN_LABEL2_ITA;
	private static Literal SIMPLE_LITERAL_ITA;

	private static Literal PREF_LABEL_ENG;
	private static Literal ALT_LABEL1_ENG;
	private static Literal ALT_LABEL2_ENG;
	private static Literal HIDDEN_LABEL1_ENG;
	private static Literal HIDDEN_LABEL2_ENG;
	private static Literal SIMPLE_LITERAL_ENG;

	private static TerminologyAccessQueryProviderSKOSXLImpl queryProvider;
	private static TerminologyAccessQueryImpl terminologyAccess;

	@BeforeClass
	public static void setUp() throws Exception {
		loadModel(iri("http://www.w3.org/2008/05/skos-xl"), "http://example.org");
		conn.add(FlatAccessQueryProviderOWLImplTest.class.getResource("exampleSKOSXLLabels.ttl"), null,
				RDFFormat.TURTLE);
		queryProvider = new TerminologyAccessQueryProviderSKOSXLImpl();
		terminologyAccess = new TerminologyAccessQueryImpl();
		terminologyAccess.setLinguisticAccessQueryProvider(queryProvider);
		terminologyAccess.setRepositoryConnection(conn);
		terminologyAccess.initialize();

		PREF_LABEL_ITA = literal("prefx", "it");
		ALT_LABEL1_ITA = literal("altx1", "it");
		ALT_LABEL2_ITA = literal("altx2", "it");
		HIDDEN_LABEL1_ITA = literal("hiddenx1", "it");
		HIDDEN_LABEL2_ITA = literal("hiddenx2", "it");
		SIMPLE_LITERAL_ITA = literal("simpleLiteral", "it");

		PREF_LABEL_ENG = literal("prefx", "en");
		ALT_LABEL1_ENG = literal("altx1", "en");
		ALT_LABEL2_ENG = literal("altx2", "en");
		HIDDEN_LABEL1_ENG = literal("hiddenx1", "en");
		HIDDEN_LABEL2_ENG = literal("hiddenx2", "en");
		SIMPLE_LITERAL_ENG = literal("simpleLiteral", "en");

	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected
	 * skosxl:{pref,alt,hidden}Labels(s).
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> allLabels = QueryResults.asList(terminologyAccess.listLabels(resource));

		assertThat(allLabels,
				containsInAnyOrder(PREF_LABEL_ITA, ALT_LABEL1_ITA, ALT_LABEL2_ITA, HIDDEN_LABEL1_ITA,
						HIDDEN_LABEL2_ITA, PREF_LABEL_ENG, ALT_LABEL1_ENG, ALT_LABEL2_ENG, HIDDEN_LABEL1_ENG,
						HIDDEN_LABEL2_ENG));
		assertThat(allLabels, not(hasItem(SIMPLE_LITERAL_ITA)));
		assertThat(allLabels, not(hasItem(SIMPLE_LITERAL_ENG)));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected
	 * skosxl:{pref,alt,hidden}Labels(s) in Italian.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListItalianLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> allItalianLabels = QueryResults.asList(terminologyAccess.listLabels(resource, "it"));

		assertThat(allItalianLabels, containsInAnyOrder(PREF_LABEL_ITA, ALT_LABEL1_ITA, ALT_LABEL2_ITA,
				HIDDEN_LABEL1_ITA, HIDDEN_LABEL2_ITA));
		assertThat(allItalianLabels, not(hasItem(SIMPLE_LITERAL_ITA)));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected
	 * skosxl:{pref,alt,hidden}Labels(s) in English.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListEnglishLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> allEnglishLabels = QueryResults.asList(terminologyAccess.listLabels(resource, "en"));

		assertThat(allEnglishLabels, containsInAnyOrder(PREF_LABEL_ENG, ALT_LABEL1_ENG, ALT_LABEL2_ENG,
				HIDDEN_LABEL1_ENG, HIDDEN_LABEL2_ENG));
		assertThat(allEnglishLabels, not(hasItem(SIMPLE_LITERAL_ENG)));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skosxl:prefLabels(s).
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListPrefLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> prefLabels = QueryResults.asList(terminologyAccess.listPrefLabels(resource));

		assertThat(prefLabels, containsInAnyOrder(PREF_LABEL_ITA, PREF_LABEL_ENG));
	}

	/**
	 * Tests that the access object returns expected skosxl:prefLabels(s) in Italian.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testGetItalianPrefLabel() throws RDF4JException {
		IRI resource = iri(baseUri);

		Literal italianPrefLabel = terminologyAccess.getPrefLabel(resource, "it");
		assertEquals(PREF_LABEL_ITA, italianPrefLabel);
	}

	/**
	 * Tests that the access object returns expected skosxl:prefLabels(s) in English.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testGetEnglishPrefLabel() throws RDF4JException {
		IRI resource = iri(baseUri);

		Literal englishPrefLabel = terminologyAccess.getPrefLabel(resource, "en");
		assertEquals(PREF_LABEL_ENG, englishPrefLabel);
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skoxls:altLabels(s).
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListAltLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> altLabels = QueryResults.asList(terminologyAccess.listAltLabels(resource));

		assertThat(altLabels,
				containsInAnyOrder(ALT_LABEL1_ITA, ALT_LABEL2_ITA, ALT_LABEL1_ENG, ALT_LABEL2_ENG));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skosxl:altLabels(s) in
	 * Italian.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListItalianAltLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> italianAltLabels = QueryResults.asList(terminologyAccess.listAltLabels(resource, "it"));

		assertThat(italianAltLabels, containsInAnyOrder(ALT_LABEL1_ITA, ALT_LABEL2_ITA));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skosxl:altLabels(s) in
	 * English.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListEnglishAltLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> englishAltLabels = QueryResults.asList(terminologyAccess.listAltLabels(resource, "en"));

		assertThat(englishAltLabels, containsInAnyOrder(ALT_LABEL1_ENG, ALT_LABEL2_ENG));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skosxl:hiddenLabels(s).
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListHiddenLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> hiddenLabels = QueryResults.asList(terminologyAccess.listHiddenLabels(resource));

		assertThat(hiddenLabels, containsInAnyOrder(HIDDEN_LABEL1_ITA, HIDDEN_LABEL2_ITA, HIDDEN_LABEL1_ENG,
				HIDDEN_LABEL2_ENG));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skosxl:hiddenLabels(s) in
	 * Italian.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListItalianHiddenLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> italianHiddenLabels = QueryResults
				.asList(terminologyAccess.listHiddenLabels(resource, "it"));

		assertThat(italianHiddenLabels, containsInAnyOrder(HIDDEN_LABEL1_ITA, HIDDEN_LABEL2_ITA));
	}

	/**
	 * Tests that the access object lists (in whatever order) exactly the expected skosxl:hiddenLabels(s) in
	 * English.
	 * 
	 * @throws RDF4JException
	 */
	@Test
	public void testListEnglishHiddenLabels() throws RDF4JException {
		IRI resource = iri(baseUri);

		List<Literal> englishHiddenLabels = QueryResults
				.asList(terminologyAccess.listHiddenLabels(resource, "en"));

		assertThat(englishHiddenLabels, containsInAnyOrder(HIDDEN_LABEL1_ENG, HIDDEN_LABEL2_ENG));
	}
}
