package it.uniroma2.art.maple.orchestration;

import java.net.URL;

import it.uniroma2.art.maple.components.access.LabelAccess;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestRule;

import it.uniroma2.art.maple.orchestration.impl.MediationFrameworkImpl;
import it.uniroma2.art.maple.provisioning.ComponentRepository;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;

import javax.print.attribute.standard.Media;

/**
 * A Junit {@link TestRule} that instantiates a {@link MediationFramework}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class MediationFrameworkRule extends ExternalResource {
	private ComponentRepository componentRepository;
	private MediationFrameworkImpl mediationFramework;
	private PluginManager pluginManager;


	public MediationFrameworkRule() {
	}

	@Override
	protected void before() throws Throwable {
		super.before();

		pluginManager = new DefaultPluginManager();
		// only class path extensions!

		componentRepository = new ComponentRepository(pluginManager);

		componentRepository.setDataDumps(new URL[]{MediationFramework.class.getResource("/META-INF/it.uniroma2.art.maple/access_implementations.ttl")});
		componentRepository.initialize();

		MediationFrameworkImpl mediationFrameworkImpl = new MediationFrameworkImpl();
		mediationFrameworkImpl.setComponentRepository(componentRepository);

		mediationFramework = mediationFrameworkImpl;

	}

	@Override
	protected void after() {
		if (componentRepository != null) {
			componentRepository.dispose();
		}
	}

	public MediationFramework getObject() {
		return mediationFramework;
	}
}
