package it.uniroma2.art.maple.orchestration;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import it.uniroma2.art.maple.scenario.Alignment;
import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.VOID;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import it.uniroma2.art.maple.components.access.TerminologyAccess;
import it.uniroma2.art.maple.orchestration.model.Collaboration;
import it.uniroma2.art.maple.orchestration.model.CollaborationInstance;
import it.uniroma2.art.maple.scenario.AlignmentScenario;

/**
 * A test suite for {@link MediationFramework}.
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class MediationFrameworkTest {


	@Rule
	public final MediationFrameworkRule mediationFrameworkRule = new MediationFrameworkRule();

	@Test
	public void computeFlatAccesses() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject().computeFlatAccesses(
				SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2002/07/owl"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeHierarchicalAccesses() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
				.computeHierarchicalAccesses(
						SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeLabelAccesses() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject().computeLabelAccesses(
				SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeTerminologyAccesses() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
				.computeTerminologyAccesses(
						SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2008/05/skos-xl"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeFlatAccessQueryProviders() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
				.computeFlatAccessQueryProviders(
						SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2002/07/owl"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeHierarchicalAccessQueryProviders() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
				.computeHierarchicalAccessQueryProviders(
						SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeLabelAccessQueryProviders() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
				.computeLabelAccessQueryProviders(
						SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2004/02/skos/core"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void computeTerminologyAccessQueryProviders() throws Exception {
		List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
				.computeTerminologyAccessQueryProviders(
						SimpleValueFactory.getInstance().createIRI("http://www.w3.org/2008/05/skos-xl"));

		candidateCollaborations.forEach(c -> System.out.println(c.getParticipants()));
	}

	@Test
	public void instantiateCollaboration() throws Exception {
		Repository repository = new SailRepository(new MemoryStore());
		repository.init();
		try {
			try (RepositoryConnection conn = repository.getConnection()) {
				ValueFactory vf = conn.getValueFactory();
				List<Collaboration> candidateCollaborations = mediationFrameworkRule.getObject()
						.computeTerminologyAccesses(SimpleValueFactory.getInstance()
								.createIRI("http://www.w3.org/2004/02/skos/core"));
				conn.add(vf.createIRI("http://example.org/Person"), SKOS.PREF_LABEL,
						vf.createLiteral("Person", "en"));
				conn.add(vf.createIRI("http://example.org/Person"), SKOS.PREF_LABEL,
						vf.createLiteral("Persona", "it"));

				Collaboration chosenCollaboration = candidateCollaborations.iterator().next();
				Map<String, RepositoryConnection> connections = new HashMap<>();
				connections.put("target", conn);
				CollaborationInstance collaborationInstance = mediationFrameworkRule.getObject()
						.instantiateCollaboration(chosenCollaboration, connections);

				TerminologyAccess terminologyAccess = (TerminologyAccess) collaborationInstance
						.getRoleObjectMapping().get(Collaboration.LINGUISTIC_ACCESS_ROLE);

				assertThat(terminologyAccess.getPrefLabel(vf.createIRI("http://example.org/Person"), "it"),
						equalTo(vf.createLiteral("Persona", "it")));

			}
		} finally {
			repository.shutDown();
		}
	}

	// @Ignore
	@Test
	public void testProfileMatching()
			throws ProfilingException, RDFParseException, RepositoryException, IOException {
		Repository metadataRep = new SailRepository(new MemoryStore());
		metadataRep.init();
		try (RepositoryConnection metadataConn = metadataRep.getConnection()) {
			ValueFactory vf = metadataConn.getValueFactory();
			metadataConn.add(this.getClass().getResource("scenario1-metadataregistry.ttl"), null,
					RDFFormat.TURTLE);
			// EuroVoc
			IRI sourceDataset;
			try (RepositoryResult<Statement> repositoryResult = metadataConn.getStatements(null, VOID.URI_SPACE,
					vf.createLiteral("http://eurovoc.europa.eu/"));
					Stream<Statement> stream = QueryResults.stream(repositoryResult)) {
				sourceDataset = stream
						.map(Statement::getSubject).map(IRI.class::cast).findFirst()
						.orElseThrow(() -> new RuntimeException("Could not find EuroVoc dataset IRI"));
			}

			// TESEO
			IRI targetDataset;
			try (RepositoryResult<Statement> repositoryResult = metadataConn.getStatements(null, VOID.URI_SPACE,
					vf.createLiteral("http://www.senato.it/teseo/tes/"));
				 Stream<Statement> stream = QueryResults.stream(repositoryResult)) {
				targetDataset = stream
						.map(Statement::getSubject).map(IRI.class::cast).findFirst()
						.orElseThrow(() -> new RuntimeException("Could not find TESEO dataset IRI"));
			}

			AlignmentScenario matchingProblem = mediationFrameworkRule.getObject().profileAlignmentScenario(metadataConn,
					sourceDataset, targetDataset);
			System.out.println(matchingProblem);
		} finally {
			metadataRep.shutDown();
		}
	}

	@Test
	public void testProfileMatchingNoLexicalizations()
			throws ProfilingException, RDFParseException, RepositoryException, IOException {
		Repository metadataRep = new SailRepository(new MemoryStore());
		metadataRep.init();
		try (RepositoryConnection metadataConn = metadataRep.getConnection()) {
			ValueFactory vf = metadataConn.getValueFactory();
			metadataConn.add(this.getClass().getResource("scenario2-metadataregistry.ttl"), null,
					RDFFormat.TURTLE);
			// EuroVoc
			IRI sourceDataset;
			try (RepositoryResult<Statement> repositoryResult = metadataConn.getStatements(null, VOID.URI_SPACE,
					vf.createLiteral("http://eurovoc.europa.eu/"));
				 Stream<Statement> stream = QueryResults.stream(repositoryResult)) {
				sourceDataset = stream
						.map(Statement::getSubject).map(IRI.class::cast).findFirst()
						.orElseThrow(() -> new RuntimeException("Could not find EuroVoc dataset IRI"));
			}
			// TESEO
			IRI targetDataset;
			try (RepositoryResult<Statement> repositoryResult = metadataConn.getStatements(null, VOID.URI_SPACE,
					vf.createLiteral("http://www.senato.it/teseo/tes/"));
				 Stream<Statement> stream = QueryResults.stream(repositoryResult)) {
				targetDataset = stream
						.map(Statement::getSubject).map(IRI.class::cast).findFirst()
						.orElseThrow(() -> new RuntimeException("Could not find TESEO dataset IRI"));
			}
			AlignmentScenario matchingProblem = mediationFrameworkRule.getObject().profileAlignmentScenario(metadataConn,
					sourceDataset, targetDataset);

			assertThat(matchingProblem.getPairings(), is(empty()));
		} finally {
			metadataRep.shutDown();
		}
	}

	@Test
	public void testAlignmentBootstrapping()
			throws ProfilingException, RDFParseException, RepositoryException, IOException {
		Repository metadataRep = new SailRepository(new MemoryStore());
		metadataRep.init();
		try (RepositoryConnection metadataConn = metadataRep.getConnection()) {
			ValueFactory vf = metadataConn.getValueFactory();
			metadataConn.add(this.getClass().getResource("alignmentbootstrapping-metadataregistry.ttl"), null,
					RDFFormat.TURTLE);
			// OntoA
			IRI sourceDataset = vf.createIRI("http://semanticturkey.uniroma2.it/metadataregistry/dataset_ontoA");

			// OntoC
			IRI targetDataset = vf.createIRI("http://semanticturkey.uniroma2.it/metadataregistry/dataset_ontoC");

			AlignmentScenario matchingProblem = mediationFrameworkRule.getObject().profileAlignmentScenario(metadataConn,
					sourceDataset, targetDataset);
			assertThat(matchingProblem.getAlignmentChains(), hasSize(1));
			List<IRI> chain = matchingProblem.getAlignmentChains().get(0).getChain();
			assertThat(chain, hasSize(2));
			Alignment firstAlign = (Alignment) matchingProblem.getSupportDatasets().stream().filter(ds -> chain.get(0).equals(ds.getId())).findFirst().orElse(null);
			Alignment secondAlign = (Alignment) matchingProblem.getSupportDatasets().stream().filter(ds -> chain.get(1).equals(ds.getId())).findFirst().orElse(null);
			assertThat(firstAlign, notNullValue());
			assertThat(firstAlign.getSubjectsTarget(), equalTo(sourceDataset));
			assertThat(firstAlign.getObjectsTarget(), equalTo(secondAlign.getSubjectsTarget()));
			assertThat(secondAlign.getObjectsTarget(), equalTo(targetDataset));


			assertThat(secondAlign, notNullValue());

		} finally {
			metadataRep.shutDown();
		}
	}


}
