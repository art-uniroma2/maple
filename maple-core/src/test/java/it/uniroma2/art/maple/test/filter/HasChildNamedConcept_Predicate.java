package it.uniroma2.art.maple.test.filter;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;

import com.google.common.base.Predicate;

import it.uniroma2.art.maple.components.access.HierarchicalAccess;

/**
 * A predicate accepting named resources that have a given child named concept according to an object
 * implementing {@link HierarchicalAccess}.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class HasChildNamedConcept_Predicate implements Predicate<IRI> {
	private HierarchicalAccess hierarchicalAccess;
	private IRI childConcept;

	/**
	 * Constructs a predicate for the specified <code>hierarchicalAccess</code> and <code>childConcept</code>.
	 * 
	 * @param hierarchicalAccess
	 * @param childConcept
	 */
	private HasChildNamedConcept_Predicate(HierarchicalAccess hierarchicalAccess, IRI childConcept) {
		this.hierarchicalAccess = hierarchicalAccess;
		this.childConcept = childConcept;
	}

	/**
	 * Static factory method that constructs a predicate for the specified <code>hierarchicalAccess</code> and
	 * <code>childConcept</code>.
	 * 
	 * @param hierarchicalAccess
	 * @param childConcept
	 * @return
	 */
	public static HasChildNamedConcept_Predicate getPredicate(HierarchicalAccess hierarchicalAccess,
			IRI childConcept) {
		return new HasChildNamedConcept_Predicate(hierarchicalAccess, childConcept);
	}

	/**
	 * Accepts named resources that have a given child named concept according to an object implementing
	 * {@link HierarchicalAccess}.
	 * 
	 * @param concept
	 * @return
	 */
	@Override
	public boolean apply(IRI concept) {
		try {
			return hierarchicalAccess.hasChildNamedConcept(concept, childConcept);
		} catch (RDF4JException e) {
			e.printStackTrace();
			return false;
		}
	}
}
