package it.uniroma2.art.maple.test.filter;

import org.eclipse.rdf4j.common.exception.RDF4JException;
import org.eclipse.rdf4j.model.IRI;

import com.google.common.base.Predicate;

import it.uniroma2.art.maple.components.access.HierarchicalAccess;

/**
 * A predicate accepting named resources that have a given parent named concept according to an object
 * implementing {@link HierarchicalAccess}.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class HasParentNamedConcept_Predicate implements Predicate<IRI> {
	private HierarchicalAccess hierarchicalAccess;
	private IRI parentConcept;

	/**
	 * Constructs a predicate for the specified <code>hierarchicalAccess</code> and <code>parentConcept</code>
	 * .
	 * 
	 * @param hierarchicalAccess
	 * @param parentConcept
	 */
	private HasParentNamedConcept_Predicate(HierarchicalAccess hierarchicalAccess, IRI parentConcept) {
		this.hierarchicalAccess = hierarchicalAccess;
		this.parentConcept = parentConcept;
	}

	/**
	 * Static factory method that constructs a predicate for the specified <code>hierarchicalAccess</code> and
	 * <code>parentConcept</code>.
	 * 
	 * @param hierarchicalAccess
	 * @param parentConcept
	 * @return
	 */
	public static HasParentNamedConcept_Predicate getPredicate(HierarchicalAccess hierarchicalAccess,
			IRI parentConcept) {
		return new HasParentNamedConcept_Predicate(hierarchicalAccess, parentConcept);
	}

	/**
	 * Accepts named resources that have a given parent named concept according to an object implementing
	 * {@link HierarchicalAccess}.
	 * 
	 * @param concept
	 * @return
	 */
	@Override
	public boolean apply(IRI concept) {
		try {
			return hierarchicalAccess.hasParentNamedConcept(concept, parentConcept);
		} catch (RDF4JException e) {
			e.printStackTrace();
			return false;
		}
	}
}
