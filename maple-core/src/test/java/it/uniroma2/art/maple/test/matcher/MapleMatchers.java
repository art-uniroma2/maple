package it.uniroma2.art.maple.test.matcher;

import com.google.common.base.Predicate;

/**
 * A group of static factory methods for matchers provided by MAPLE. By statically importing these methods, it
 * is possible to use them as a sort of Domain-Specific Language for the definition of complex matchers.
 * 
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class MapleMatchers {

	/**
	 * Creates a matcher that matches if the examined object satisfies the specified Guava predicate.
	 */
	public static <T> PredicateMatcher<T> satifies(Predicate<T> predicate) {
		return new PredicateMatcher<T>(predicate);
	}

}
