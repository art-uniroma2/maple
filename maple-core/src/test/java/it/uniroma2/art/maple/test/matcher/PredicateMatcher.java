package it.uniroma2.art.maple.test.matcher;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import com.google.common.base.Predicate;

/**
 * A matcher verifying that an item satisfies a Guava predicate.
 *
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * 
 * @param <T>
 */
public class PredicateMatcher<T> extends TypeSafeMatcher<T> {

	private Predicate<T> predicate;

	/**
	 * Constructs a matcher wrapping a Guava predicate.
	 * 
	 * @param predicate
	 */
	public PredicateMatcher(Predicate<T> predicate) {
		this.predicate = predicate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hamcrest.SelfDescribing#describeTo(org.hamcrest.Description)
	 */
	@Override
	public void describeTo(Description description) {
		description.appendText("the item should satisfy the predicate " + predicate.getClass().getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hamcrest.TypeSafeMatcher#matchesSafely(java.lang.Object)
	 */
	@Override
	protected boolean matchesSafely(T item) {
		return predicate.apply(item);
	}

}
